﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using PrismAdminAPI.Models;
using System;
using PrismAdminAPI.Manager;
using PrismAdminAPI.Repository;

namespace PrismAdminAPI
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            string deviceId = null;
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            IStudentAPIManager studentAPIManager = new StudentAPIManager();
            ISingleSignInUserManager singleSignInUserManager = new SingleSignInUserManager(new SingleSignInUserRepository());

            var currentUser = new Login() { PhoneNumber = context.UserName, Password= context.Password };
            var checkUserInfoExist = studentAPIManager.StudentLogin(currentUser);

            if (checkUserInfoExist != null)
            {
                //get uuid / deviceId 
                context.Request.Headers.TryGetValue("uuid", out string[] uuid);

                if(uuid[0] !=null && !string.IsNullOrEmpty(uuid[0]))
                    deviceId = uuid[0];

                bool isDuplicateLogin = false;

                if (!string.IsNullOrEmpty(deviceId))
                {
                    var singleUserLoginDetails = singleSignInUserManager.GetSingleSignInUserDetails(deviceId, checkUserInfoExist.LoginId, 0);
                     isDuplicateLogin = singleSignInUserManager.IsDuplicateLogin(singleUserLoginDetails, deviceId);
                }

                if (isDuplicateLogin == false)
                {
                    identity.AddClaim(new Claim("LoginId", Convert.ToString(currentUser.LoginId)));

                    string SponsorerName = checkUserInfoExist.SponsorerName != null ? checkUserInfoExist.SponsorerName : "";
                    string LandingImagePath = checkUserInfoExist.LandingImagePath != null ? checkUserInfoExist.LandingImagePath : "";
                    string ThumbnailImagePath = checkUserInfoExist.ThumbnailImagePath != null ? checkUserInfoExist.ThumbnailImagePath : "";

                    var props = new AuthenticationProperties(new Dictionary<string, string>
                                    {
                                        {
                                         "Name", $"{checkUserInfoExist.FirstName} {checkUserInfoExist.LastName}"
                                        },
                                        {
                                         "PhoneNumber", currentUser.PhoneNumber
                                        },
                                        {
                                         "LoginId", checkUserInfoExist.LoginId.ToString()
                                        },
                                        {
                                         "MediumId", checkUserInfoExist.MediumId.ToString()
                                        },
                                        {
                                         "MediumName", checkUserInfoExist.MediumName.ToString()
                                        },
                                        {
                                         "StandardId", checkUserInfoExist.StandardId.ToString()
                                        },
                                        {
                                         "SponsorerName",SponsorerName
                                        },
                                        {
                                         "SPLandingImage", LandingImagePath
                                        },
                                        {
                                         "SPThumbnailImage", ThumbnailImagePath
                                        }
                                    });
                    var ticket = new AuthenticationTicket(identity, props);
                    context.Validated(ticket);
                }
                else
                {
                    context.SetError("duplicate_login", "Duplicate Login");
                    return;
                }
            }
            else
            {
                context.SetError("invalid_grant", "Provided username and password is not matching, Please retry.");
                context.Rejected();
            }
        }


        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            //validate your client  
            //var currentClient = context.ClientId;  

            //if (Client does not match)  
            //{  
            //    context.SetError("invalid_clientId", "Refresh token is issued to a different clientId.");  
            //    return Task.FromResult<object>(null);  
            //}  

            // Change authentication ticket for refresh token requests  
            var newIdentity = new ClaimsIdentity(context.Ticket.Identity);
            //newIdentity.AddClaim(new Claim("Id", Convert.ToString(currentUser.LoginId)));
            //newIdentity.AddClaim(new Claim("PhoneNumber", Convert.ToString(currentUser.PhoneNumber)));
            //newIdentity.AddClaim(new Claim("FirstName", Convert.ToString(currentUser.FirstName)));
            //newIdentity.AddClaim(new Claim("LastName", Convert.ToString(currentUser.LastName)));

            var newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);
            context.Validated(newTicket);

            return Task.FromResult<object>(null);
        }
    }
}