﻿using System.Web;
using System.Web.Optimization;

namespace PrismAdminAPI
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            //CSS: Bootstrap Theme Bundling Start
            bundles.Add(new StyleBundle("~/BoostrapThemeCss").Include(
                   "~/ThemeContent/plugins/fontawesome-free/css/all.min.css",
                   "~/ThemeContent/plugins/overlayScrollbars/css/OverlayScrollbars.min.css",
                   "~/ThemeContent/dist/css/adminlte.min.css",
                   "~/ThemeContent/cdn/font.css"
                ));
            //CSS: Bootstrap Theme Bundling Start

            //JS: Bootstrap Theme Bundling Start
            bundles.Add(new ScriptBundle("~/BoostrapThemeJSMain").Include(
                    "~/ThemeContent/plugins/jquery/jquery.min.js",
                    "~/ThemeContent/plugins/bootstrap/js/bootstrap.bundle.min.js",
                    "~/ThemeContent/dist/js/adminlte.js",
                    "~/ThemeContent/dist/js/demo.js"
                ));
            bundles.Add(new ScriptBundle("~/BoostrapThemeJSSub1").Include(
                    "~/ThemeContent/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js",
                    "~/ThemeContent/plugins/jquery-mousewheel/jquery.mousewheel.js",
                    "~/ThemeContent/plugins/raphael/raphael.min.js",
                    "~/ThemeContent/plugins/jquery-mapael/jquery.mapael.min.js",
                    "~/ThemeContent/plugins/jquery-mapael/maps/usa_states.min.js",
                    "~/ThemeContent/plugins/chart.js/Chart.min.js",
                    "~/ThemeContent/dist/js/pages/dashboard2.js"
                ));

            //JS: Bootstrap Theme Bundling Start
        }
    }
}
