using System.Web.Mvc;
using Unity;
using Unity.Mvc5;
using PrismAdminAPI.Manager;
using PrismAdminAPI.Repository;
using System.Web.Http;
using PrismAdminAPI.BAL;
using PrismAdminAPI.Manager.McqAPIManager;
using PrismAdminAPI.Repository.StudentDetails;
using PrismAdminAPI.Manager.StudentDetails;
using PrismAdminAPI.Manager.SponsorerAdmin;

namespace PrismAdminAPI
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterType<IStandardRepository, StandardRepository>();
            container.RegisterType<IStandardManager, StandardManager>();

            container.RegisterType<IMediumRepository, MediumRepository>();
            container.RegisterType<IMediumManager, MediumManager>();

            container.RegisterType<ISectionRepository, SectionRepository>();
            container.RegisterType<ISectionManager, SectionManager>();

            container.RegisterType<ISubjectManager, SubjectManager>();
            container.RegisterType<ISubjectRepository, SubjectRepository>();

            container.RegisterType<IConfigurationRepository, ConfigurationRepository>();
            container.RegisterType<IConfigurationManager, ConfigurationManager>();

            container.RegisterType<IChapterRepository, ChapterRepository>();
            container.RegisterType<IChapterManager, ChapterManager>();

            container.RegisterType<IChapterContentRepository, ChapterContentRepository>();
            container.RegisterType<IChapterContentManager, ChapterContentManager>();

            container.RegisterType<ISubjectContentRepository, SubjectContentRepository>();
            container.RegisterType<ISubjectContentManager, SubjectContentManager>();

            container.RegisterType<IAdminManager, AdminManager>();
            container.RegisterType<IAdminRepository, AdminRepository>();

            container.RegisterType<ICouponRepository, CouponRepository>();
            container.RegisterType<ICouponManager, CouponManager>();

            container.RegisterType<ISponsorerRepository, SponsorerRepository>();
            container.RegisterType<ISponsorerManager, SponsorerManager>();

            container.RegisterType<ISponsorerAdminRepository, SponsorerAdminRepository>();
            container.RegisterType<ISponsorerAdminManager, SponsorerAdminManager>();

            container.RegisterType<IAdminRepository, AdminRepository>();
            container.RegisterType<IAdminManager, AdminManager>();

            container.RegisterType<IMcqRepository, McqRepository>();
            container.RegisterType<IMcqManager, McqManager>();

            container.RegisterType<IStudentDetailsRepository, StudentDetailsRepository>();
            container.RegisterType<IStudentDetailsManager, StudentDetailsManager>();

            // Student API Repository
            container.RegisterType<IStudentAPIManager, StudentAPIManager>();
            container.RegisterType<IStudentAPIRepository, StudentAPIRepository>();

            // SingleSignInUser Manager
            container.RegisterType<ISingleSignInUserManager, SingleSignInUserManager>();
            container.RegisterType<ISingleSignInUserRepository, SingleSignInUserRepository>();

            // Student API Repository
            container.RegisterType<IMcqAPIManager, McqAPIManager>();

            // Dependancy Injection for MVC
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            //Dependancy Injection For Web API 
            GlobalConfiguration.Configuration.DependencyResolver =
                    new UnityResolver(container);
        }
    }
}