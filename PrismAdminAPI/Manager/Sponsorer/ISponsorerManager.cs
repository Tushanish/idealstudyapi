﻿using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Manager
{
    public interface ISponsorerManager
    {
        bool AddSponsorer(Sponsorer sponsorer);
        bool UpdateSponsorer(Sponsorer sponsorer);
        IEnumerable<Sponsorer> GetAll(long? sponsorerId = null, bool? isActive = null);
        bool AcitvateDeactivateSponsorer(long SponsererId, bool IsActive);
        bool AssignSponsorerToCoupon(long? fromCouponId, long? toCouponId, long? sponsorerId);
    }
}