﻿using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrismAdminAPI.Manager.McqAPIManager
{
    public interface IMcqAPIManager
    {
        List<ApiQuestion> GetApiQuestions(int chapterId);
        List<ApiAnswer> GetApiAnswer(int chapterId);

        bool SubmitMcq( List<ApiAnswer> apiAnswer);
    }
}
