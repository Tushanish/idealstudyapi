﻿using PrismAdminAPI.Models;
using PrismAdminAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Manager.McqAPIManager
{
    public class McqAPIManager : IMcqAPIManager
    {
        private IMcqRepository _mcqRepository;
        public McqAPIManager()
        {
            _mcqRepository = new McqRepository();
        }

        public List<ApiAnswer> GetApiAnswer(int chapterId)
        {
            var answers = _mcqRepository.GetAnswers(null,chapterId);
            List<ApiAnswer> apiAnswers = new List<ApiAnswer>();
            answers.ToList().ForEach(x =>
            {
                apiAnswers.Add(new ApiAnswer
                {
                    AnswerId = x.AnswerNo,
                    AnswerValue = x.AnswerName,
                    QuestionId = x.QuestionNo,
                    IsCorrectAnswer = x.IsCorrect,
                    SelectedValue = false
                });
            });

            return apiAnswers;
        }

        public List<ApiQuestion> GetApiQuestions(int chapterId)
        {
            var questions = _mcqRepository.GetQuestions(null, chapterId, null, null, null);
            List<ApiQuestion> apiQuestions = new List<ApiQuestion>();
            questions.ToList().ForEach(x =>
            {
                apiQuestions.Add(new ApiQuestion
                {
                    QuestionId = x.QuestionNo,
                    QuestionValue = x.QuestionName
                });
            });

            Shuffle(apiQuestions);

            return apiQuestions;
        }

        public bool SubmitMcq(List<ApiAnswer> apiAnswer)
        {
            throw new NotImplementedException();
            //return _mcqApiRepository.SubmitMcq( apiAnswer);
        }

        public static void Shuffle<T>(IList<T> list)
        {
            Random rng = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}