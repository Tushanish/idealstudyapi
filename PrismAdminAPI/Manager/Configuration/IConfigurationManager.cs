﻿using PrismAdminAPI.Models;
using PrismAdminAPI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrismAdminAPI.Manager
{
    public interface IConfigurationManager
    {
        IEnumerable<StandardMedium> GetStandardMediamConfiguration(long? _standardMediumId = null);

        long InsertStandardMedium(StandardMedium _standardMedium);

        IEnumerable<StandardMedium> GetConfiguredStandardMediamConfiguration(long? _standardId = null);

        IEnumerable<SubjectViewModel> GetConfiguredSubjectList(long _standardId, long _mediumId);

        IEnumerable<MediumSections> GetMediamSectionConfiguration(long? _mediumSectionId = null);

        long InsertMediumSection(MediumSections _mediumSection);
    }
}
