﻿using PrismAdminAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PrismAdminAPI.Models;

namespace PrismAdminAPI.Manager
{
    public class AdminManager :IAdminManager
    {
        private readonly IAdminRepository _adminrepository = null;

        public AdminManager(IAdminRepository adminrepository)
        {
            _adminrepository = adminrepository;
        }

        public Admin ValidateAdmin(UserLogin _userLogin)
        {
            return _adminrepository.ValidateAdmin(_userLogin);
        }

        public string[] GetUserRoles(string _userName)
        {
            return _adminrepository.GetUserRoles(_userName);
        }

        public bool InsertAdmin(Admin admin)
        {
            return _adminrepository.InsertAdmin(admin);
        }

        public bool UpdateAdmin(Admin admin)
        {
            return _adminrepository.UpdateAdmin(admin);
        }

        public bool ActivateDeactivateAdmin(long _adminId, bool _isActive)
        {
            return _adminrepository.ActivateDeactivateAdmin(_adminId,_isActive);
        }

        public IEnumerable<Admin> GetAdminListByAdminId(long adminId)
        {
            return _adminrepository.GetAdminListByAdminId(adminId);
        }

        public IEnumerable<Role> GetRolesByRoleId(long roleId)
        {
            return _adminrepository.GetRolesByRoleId(roleId);
        }
    }
}