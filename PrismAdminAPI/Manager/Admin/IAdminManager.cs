﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrismAdminAPI.Models;

namespace PrismAdminAPI.Manager
{
    public interface IAdminManager
    {
        Admin ValidateAdmin(UserLogin _userLogin);

        string[] GetUserRoles(string _userName);

        bool InsertAdmin(Admin admin);

        bool UpdateAdmin(Admin admin);

        bool ActivateDeactivateAdmin(long _adminId, bool _isActive);

        IEnumerable<Admin> GetAdminListByAdminId(long adminId);

        IEnumerable<Role> GetRolesByRoleId(long roleId);
    }
}
