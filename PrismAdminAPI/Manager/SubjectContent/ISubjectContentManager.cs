﻿using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrismAdminAPI.Manager
{
    public interface ISubjectContentManager
    {
        long InsertSubjectContent(SubjectContent _subjectContent);

        List<SubjectContentViewModel> GetSubjectContent(long? _standardId = null, long? _mediumId = null, long? _subjectId = null,long? _sectionId = null);

    }
}
