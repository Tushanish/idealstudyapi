﻿using Dapper;
using PrismAdminAPI.Models;
using PrismAdminAPI.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Manager
{
    public class SubjectContentManager : ISubjectContentManager
    {
        ISubjectContentRepository subjectContentRepository = null;
        public SubjectContentManager()
        {
            subjectContentRepository = new SubjectContentRepository();
        }

        public long InsertSubjectContent(SubjectContent _chapterContent)
        {
            return subjectContentRepository.InsertSubjectContent(_chapterContent);
        }

        public List<SubjectContentViewModel> GetSubjectContent(long? _standardId = null, long? _mediumId = null, long? _subjectId = null, long? _sectionId = null)
        {
            return subjectContentRepository.GetSubjectContent(_standardId, _mediumId, _subjectId, _sectionId);
        }

    }
}