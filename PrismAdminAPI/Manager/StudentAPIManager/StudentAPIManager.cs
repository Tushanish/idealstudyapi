﻿using Dapper;
using PrismAdminAPI.Models;
using PrismAdminAPI.Repository;
using PrismAdminAPI.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Manager
{
    public class StudentAPIManager : IStudentAPIManager
    {
        private IStudentAPIRepository studentAPIRepository;
        public StudentAPIManager()
        {
            studentAPIRepository = new StudentAPIRepository();
        }

        public IEnumerable<Medium> GetMediumList()
        {
            return studentAPIRepository.GetMediumList();
        }

        public Student StudentRegistration(Login _login, bool parseDate)
        {
            return studentAPIRepository.StudentRegistration(_login , parseDate);
        }

        public Student StudentLogin(Login _login)
        {
            return studentAPIRepository.StudentLogin(_login);
        }

        public IEnumerable<SubjectViewModel> GetSubjectList(long loginId)
        {
            return studentAPIRepository.GetSubjectList(loginId);
        }

        public IEnumerable<Chapter> GetChapterList(long _standardId, long _subjectId, long _mediumId)
        {
            return studentAPIRepository.GetChapterList(_standardId, _subjectId, _mediumId);
        }

        public IEnumerable<SectionMediumViewModel> GetChapterSection(bool _isChapterSection, long _mediumId, bool _isActive, long _subjectId, long _chapterId)
        {
            return studentAPIRepository.GetChapterSection(_isChapterSection, _mediumId, _isActive, _subjectId, _chapterId);
        }

        public IEnumerable<SectionMediumViewModel> GetStandardSection(bool _isStandardSection, long _subjectId, long _mediumId, bool _isActive)
        {
            return studentAPIRepository.GetStandardSection(_isStandardSection, _subjectId, _mediumId, _isActive);
        }

        public List<ChapterContentViewModel> GetChapterContentForSection(long chapterId, long sectionId)
        {
            return studentAPIRepository.GetChapterContentForSection(chapterId, sectionId);
        }

        public SubjectContentViewModel GetSubjectContentForSection(long subjectId, long sectionId)
        {
            return studentAPIRepository.GetSubjectContentForSection(subjectId, sectionId);
        }

        public Coupon ValidateCoupon(string _couponCode)
        {
            return studentAPIRepository.ValidateCoupon(_couponCode);
        }

        public bool ValidateMobileNumber(string _phoneNumber)
        {
            return studentAPIRepository.ValidateMobileNumber(_phoneNumber);
        }

        public bool UpdateAppActiveTime(long loginId)
        {
            return studentAPIRepository.UpdateAppActiveTime(loginId);
        }
    }
}