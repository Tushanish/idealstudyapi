﻿using PrismAdminAPI.Models;
using PrismAdminAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Manager
{
    public class SingleSignInUserManager : ISingleSignInUserManager
    {
        private readonly ISingleSignInUserRepository _singleSignInUserRepository;

        public SingleSignInUserManager(ISingleSignInUserRepository singleSignInUserRepository)
        {
            _singleSignInUserRepository = singleSignInUserRepository;
        }

        public SingleSignInUser GetSingleSignInUserDetails (string uuid, long? loginId = null, long? _couponId = null)
        {
            return _singleSignInUserRepository.GetSingleSignInDetails(loginId, _couponId, uuid);
        }

        public bool IsDuplicateLogin(SingleSignInUser details, string uuid)
        {
            return _singleSignInUserRepository.IsDuplicateLogin(details, uuid);
        }

        public bool RemoveSingleSignInDetails(long? loginId,long? couponId, string uuid)
        {
            return _singleSignInUserRepository.RemoveSingleSignInUser(loginId,couponId, uuid);
        }
    }
}