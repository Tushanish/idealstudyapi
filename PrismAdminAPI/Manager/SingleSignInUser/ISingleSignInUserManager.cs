﻿using System.Collections.Generic;
using PrismAdminAPI.Models;

namespace PrismAdminAPI.Manager
{
    public interface ISingleSignInUserManager
    {
        SingleSignInUser GetSingleSignInUserDetails( string uuid, long? loginId, long? _couponId);

        bool RemoveSingleSignInDetails(long? loginId, long? couponId, string uuid);

        bool IsDuplicateLogin(SingleSignInUser details, string uuid);
    }
}