﻿using PrismAdminAPI.Models;
using PrismAdminAPI.Repository;
using System.Collections.Generic;


namespace PrismAdminAPI.Manager.SponsorerAdmin
{
    public class SponsorerAdminManager : ISponsorerAdminManager
    {
        private readonly ISponsorerAdminRepository _sponsorerAdminRepository = null;

        public SponsorerAdminManager(ISponsorerAdminRepository sponsorerAdminRepository)
        {
            _sponsorerAdminRepository = sponsorerAdminRepository;
        }

        public IEnumerable<SponsorerCoupon> GetCouponsForSponsorer(long sponsorerId)
        {
            return _sponsorerAdminRepository.GetCouponsForSponsorer(sponsorerId);
        }

        public IEnumerable<StudentDetail> GetStudentDetailsForSponsorer(bool unusedCoupons,long sponsorerId)
        {
            return _sponsorerAdminRepository.GetStudentDetailsForSponsorer(unusedCoupons,sponsorerId);
        }
    }
}