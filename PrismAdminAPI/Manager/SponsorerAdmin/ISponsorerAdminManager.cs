﻿using System.Collections.Generic;
using PrismAdminAPI.Models;

namespace PrismAdminAPI.Manager.SponsorerAdmin
{
    public interface ISponsorerAdminManager
    {
        IEnumerable<SponsorerCoupon> GetCouponsForSponsorer(long sponsorerId);

        IEnumerable<StudentDetail> GetStudentDetailsForSponsorer(bool usedCoupons,long sponsorerId);
    }
}