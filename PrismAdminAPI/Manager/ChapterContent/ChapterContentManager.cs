﻿using Dapper;
using PrismAdminAPI.Models;
using PrismAdminAPI.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Manager
{
    public class ChapterContentManager : IChapterContentManager
    {
        IChapterContentRepository chapterContentRepository = null;
        public ChapterContentManager()
        {
            chapterContentRepository = new ChapterContentRepository();
        }

        public long InsertChapterContent(ChapterContent _chapterContent)
        {
            return chapterContentRepository.InsertChapterContent(_chapterContent);
        }

        public List<ChapterContentViewModel> GetChapterContent(long? _standardId=null, long? _mediumId = null, long? _chapterId = null, long? _sectionId = null, long? _subjectId = null, bool? _isSingleRecord = true)
        {
            return chapterContentRepository.GetChapterContent(_standardId, _mediumId, _chapterId, _sectionId, _subjectId, _isSingleRecord);
        }

        public bool DeleteChapterContent(long _chapterContentId)
        {
            return chapterContentRepository.DeleteChapterContent(_chapterContentId);
        }

        public bool EnableDisableChapterSection(long _chapterId, long _sectionId, bool _isDisable)
        {
            return chapterContentRepository.EnableDisableChapterSection(_chapterId, _sectionId, _isDisable);
        }
    }
}