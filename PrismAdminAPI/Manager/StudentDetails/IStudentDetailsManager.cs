﻿using System.Collections.Generic;
using PrismAdminAPI.Models;

namespace PrismAdminAPI.Manager.StudentDetails
{
    public interface IStudentDetailsManager
    {
        IEnumerable<StudentDetail> GetStudentDetails(int loginId,bool usedCoupon, out long totalRecords, int start, int length);
        bool UpdatePassword(int loginId, string password, string mobileNumber,
            string firstName, string lastName, int mediumId);
        IEnumerable<StudentDetail> FilterStudentDetails(string searchValue, bool isusedCoupons, long fromCouponId, long toCouponId);
    }
}