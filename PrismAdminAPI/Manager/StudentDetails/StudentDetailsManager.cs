﻿using PrismAdminAPI.Models;
using PrismAdminAPI.Repository.StudentDetails;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Manager.StudentDetails
{
    public class StudentDetailsManager : IStudentDetailsManager
    {
        private readonly IStudentDetailsRepository _studentDetailsRepository = null;

        public StudentDetailsManager(IStudentDetailsRepository studentDetailsRepository)
        {
            _studentDetailsRepository = studentDetailsRepository;
        }

        public IEnumerable<StudentDetail> FilterStudentDetails(string searchValue, bool isusedCoupons,long fromCouponId,long toCouponId)
        {
            return _studentDetailsRepository.GetFilterStudentDetails(searchValue, isusedCoupons, fromCouponId, toCouponId);
        }

        public IEnumerable<StudentDetail> GetStudentDetails(int loginId, bool usedCoupon,out long totalRecords ,int start , int length)
        {
            return _studentDetailsRepository.GetStudentDetails(loginId,usedCoupon, out totalRecords, start,length);
        }

        public bool UpdatePassword(int loginId , string password , string mobileNumber, string firstName, string lastName, int mediumId)
        {
            return _studentDetailsRepository.UpdatePassword(loginId, password,mobileNumber,firstName,lastName,mediumId);
        }
    }
}