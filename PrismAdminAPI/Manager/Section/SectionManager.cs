﻿using PrismAdminAPI.Models;
using PrismAdminAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Manager
{
    public class SectionManager : ISectionManager
    {
        private ISectionRepository sectionRepository;
        public SectionManager()
        {
            sectionRepository = new SectionRepository();
        }

        public IEnumerable<Section> GetSectionList(long? _sectionId = null, bool? _isActive = null)
        {
            return sectionRepository.GetSectionList();
        }

        public long InsertSection(Section _section)
        {
            return sectionRepository.InsertSection(_section);
        }

        public int UpdateSection(Section _section)
        {
            return sectionRepository.UpdateSection(_section);
        }

        public int ActivateDeactivateSection(long _sectionId, bool _isActive)
        {
            return sectionRepository.ActivateDeactivateSection(_sectionId, _isActive);
        }

        public int ActivateDeactivateStandardSection(long _sectionId, bool _isStandardSection) {
            return sectionRepository.ActivateDeactivateStandardSection(_sectionId, _isStandardSection);
        }

        public int ActivateDeactivateChapterSection(long _sectionId, bool _isChapterSection) {
            return sectionRepository.ActivateDeactivateChapterSection(_sectionId, _isChapterSection);
        }

        public IEnumerable<Section> GetSectionsByChapterId(long chapterId, long mediumId)
        {
            return sectionRepository.GetSectionsByChapterId(chapterId, mediumId);
        }
    }
}