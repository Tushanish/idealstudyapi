﻿using PrismAdminAPI.Models;
using PrismAdminAPI.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Manager
{
    public class McqManager : IMcqManager
    {

        private readonly IMcqRepository _mcqRepository;

        public McqManager(IMcqRepository mcqRepository)
        {
            _mcqRepository = mcqRepository;
        }

        public bool AddMcq(LnkQuestionChapterViewModel lnkQuestionChapterViewModel, IEnumerable<Question> existingQuestions)
        {
            if (existingQuestions.Count() > 0)
            {
                existingQuestions.ToList().ForEach(question =>
                {
                    var existingQue = lnkQuestionChapterViewModel.Questions.FirstOrDefault(x => x.QuestionName.ToLower() == question.QuestionName.ToLower());

                    if (existingQue != null)
                    {
                        lnkQuestionChapterViewModel.Questions.Remove(existingQue);
                    }
                });
            }

            lnkQuestionChapterViewModel.Questions.ForEach(x =>
            {
                x.Answers.ForEach(ans =>
                {
                    ans.ActionType = "I";
                });
            });

            var questionModel = CreateLnkQuestionModel(lnkQuestionChapterViewModel);
            var questionTable = GetQuestionTable(questionModel.Questions);
            var answerTable = GetAnswerTable(questionModel.Questions.SelectMany(x => x.Answers).ToList());

            return _mcqRepository.InsertMcq(questionModel.SubjectId, questionModel.ChapterId, questionModel.MediumId, questionModel.SectionId, questionModel.StandardId, questionTable, answerTable);
        }

        public bool UpdateMcq(LnkQuestionChapterViewModel lnkQuestionChapterViewModel, IEnumerable<Question> existingQuestions)
        {
            var _allAnswers = _mcqRepository.GetAnswers(0, lnkQuestionChapterViewModel.ChapterId).ToList();

            lnkQuestionChapterViewModel.Questions.ForEach(x =>
            {
                var answers = _allAnswers.Where(z => z.QuestionNo == x.QuestionNo).ToList();

                answers.ForEach(a => a.ActionType = "D"); //Default Set to Delete

                int i = 1;
                x.Answers.ForEach(ans => ans.AnswerNo = i++);

                x.Answers.ForEach(ans =>
                {
                    var existingAnswer = answers.FirstOrDefault(q => q.AnswerNo == ans.AnswerNo);

                    if (existingAnswer != null) //UPDATE OR INSERT
                    {
                        ans.ActionType = "U";
                        existingAnswer.ActionType = "U";
                    }
                    else
                    {
                        ans.ActionType = "I";
                        if (existingAnswer != null)
                        {
                            existingAnswer.ActionType = "I";
                        }
                    }
                });

                if (x.Answers.Count < answers.Count) //DELETE EXISTING
                {
                    answers.Where(a => a.ActionType == "D").ToList().ForEach(answerToDelete =>
                    {
                        x.Answers.Add(answerToDelete);
                    });
                }
            });


            var questionModel = CreateLnkQuestionModel(lnkQuestionChapterViewModel);
            var questionTable = GetQuestionTable(questionModel.Questions);
            var answerTable = GetAnswerTable(questionModel.Questions.SelectMany(x => x.Answers).ToList());

            return _mcqRepository.UpdateMcq(questionModel.SubjectId, questionModel.ChapterId, questionModel.MediumId, questionModel.SectionId, questionModel.StandardId, questionTable, answerTable);
        }


        public LnkQuestionChapterViewModel GetFilteredMcq(long? standardId, long? mediumId, long? subjectId, long? chapterId, long? sectionId, bool IsCorrectFilter)
        {
            var Questions = _mcqRepository.GetQuestions(subjectId, chapterId, mediumId, sectionId, standardId);
            var Answers = _mcqRepository.GetAnswers(0, chapterId).ToList();

            if (IsCorrectFilter)
                Answers = Answers.Where(x => x.IsCorrect == true).ToList();

            var viewModel = new LnkQuestionChapterViewModel
            {
                Questions = new List<Question>()
            };

            Questions.ToList().ForEach(question =>
            {
                question.Answers = Answers.Where(x => x.QuestionNo == question.QuestionNo).ToList();
                viewModel.Questions.Add(question);
            });

            return viewModel;
        }

        private LnkQuestionChapterDetails CreateLnkQuestionModel(LnkQuestionChapterViewModel lnkQuestionChapterViewModel)
        {
            return new LnkQuestionChapterDetails
            {
                ChapterId = lnkQuestionChapterViewModel.ChapterId,
                MediumId = lnkQuestionChapterViewModel.MediumId,
                SectionId = lnkQuestionChapterViewModel.SectionId,
                SubjectId = lnkQuestionChapterViewModel.SubjectId,
                StandardId = lnkQuestionChapterViewModel.StandardId,
                Questions = lnkQuestionChapterViewModel.Questions
            };
        }

        private DataTable GetQuestionTable(List<Question> questions)
        {
            DataTable questionTable = new DataTable();
            questionTable.Columns.Add("QuestionId", typeof(long));
            questionTable.Columns.Add("QuestionNo", typeof(long));
            questionTable.Columns.Add("QuestionName", typeof(string));

            foreach (var question in questions)
            {
                questionTable.Rows.Add(question.QuestionId, question.QuestionNo, question.QuestionName);
            }
            return questionTable;
        }

        private DataTable GetAnswerTable(List<Answer> answers)
        {
            DataTable answerTable = new DataTable();
            answerTable.Columns.Add("AnswerId", typeof(long));
            answerTable.Columns.Add("AnswerNo", typeof(long));
            answerTable.Columns.Add("QuestionNo", typeof(long));
            answerTable.Columns.Add("IsCorrect", typeof(bool));
            answerTable.Columns.Add("AnswerName", typeof(string));
            answerTable.Columns.Add("ActionType", typeof(char));

            foreach (var answer in answers)
            {
                answerTable.Rows.Add(answer.AnswerId, answer.AnswerNo, answer.QuestionNo, answer.IsCorrect, answer.AnswerName, answer.ActionType);
            }
            return answerTable;
        }
    }
}