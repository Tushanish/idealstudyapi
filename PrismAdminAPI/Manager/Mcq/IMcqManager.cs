﻿using PrismAdminAPI.Models;
using System.Collections.Generic;
using System.Data;

namespace PrismAdminAPI.Manager
{
    public interface IMcqManager
    {
        bool AddMcq(LnkQuestionChapterViewModel lnkQuestionChapterViewModel, IEnumerable<Question> existingQuestionId);
        LnkQuestionChapterViewModel GetFilteredMcq(long? standardId, long? mediumId, long? subjectId, long? chapterId, long? sectionId, bool IsCorrectFilter);
        bool UpdateMcq(LnkQuestionChapterViewModel lnkQuestionChapterViewModel, IEnumerable<Question> existingQuestions);
    }
}