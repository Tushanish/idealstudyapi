﻿using PrismAdminAPI.Models;
using PrismAdminAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Manager
{
    public class CouponManager : ICouponManager
    {
        private readonly ICouponRepository _couponRepository;

        public CouponManager(ICouponRepository couponRepository)
        {
            _couponRepository = couponRepository;
        }

        public bool SaveBulkCoupons<T>(int batchSize, string tableName, List<T> _list)
        {
            return _couponRepository.SaveBulkCoupon(batchSize, tableName, _list);
        }

        public IEnumerable<Coupon> GetAllCoupons(long? couponId = null, bool? isActive = null)
        {
            return _couponRepository.GetAllCoupons(couponId, isActive);
        }

        public bool ActivateDeactivateCoupon(long? fromCouponId, long? toCouponId, bool isActive)
        {
            return _couponRepository.ActivateDeactivateCoupon(fromCouponId,toCouponId, isActive);
        }

        public IEnumerable<LnkCouponDetails> GetAllLnkCouponDetails(long? couponId = null, long? sponsorerId = null)
        {
            return _couponRepository.GetAllLnkCouponDetails(couponId, sponsorerId);
        }

        public IEnumerable<Coupon> GetCouponsForIds(List<string> couponIds)
        {
            return _couponRepository.GetCouponsForIds(couponIds);
        }

        public IEnumerable<CouponDetails> GetCouponDetails(string searchValue, bool usedCoupons, out long totalrecords, long fromCouponId = 0, long toCouponId = 0, long start = 0, long length = 0)
        {
            return _couponRepository.GetCouponDetails(searchValue, usedCoupons, out totalrecords, fromCouponId, toCouponId, start, length);
        }
    }
}