﻿using System.Collections.Generic;
using PrismAdminAPI.Models;

namespace PrismAdminAPI.Manager
{
    public interface ICouponManager
    {
        IEnumerable<Coupon> GetAllCoupons(long? couponId = null, bool? isActive = null);
        bool SaveBulkCoupons<T>(int batchSize, string tableName, List<T> _list);
        bool ActivateDeactivateCoupon(long? fromCouponId, long? toCouponId, bool IsActive);
        IEnumerable<LnkCouponDetails> GetAllLnkCouponDetails(long? couponId = null, long? sponsorerId = null);
        IEnumerable<Coupon> GetCouponsForIds(List<string> couponIds);

        IEnumerable<CouponDetails> GetCouponDetails(string searchValue, bool usedCoupons, out long totalrecords, long fromCouponId = 0, long toCouponId = 0,long start = 0,long length = 0);
    }
}