﻿using PrismAdminAPI.Models;
using PrismAdminAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Manager
{
    public class StandardManager : IStandardManager
    {
        private IStandardRepository _standardRepository = null;

        public StandardManager()
        {
            _standardRepository = new StandardRepository();
        }

        public IEnumerable<Standard> GetStandardList(long? standardId = null, bool? isActive = null)
        {
            return _standardRepository.GetStandardList(standardId, isActive);
        }

        public long InsertStandard(Standard _standard)
        {
            return _standardRepository.InsertStandard(_standard);
        }

        public int UpdateStandard(Standard _standard)
        {
            return _standardRepository.UpdateStandard(_standard);
        }

        public int ActivateDeactivateStandard(long _standardId, bool _isActive)
        {
            return _standardRepository.ActivateDeactivateStandard(_standardId, _isActive);
        }
    }
}