﻿function GetMediumsById() {
    var _standardId = $(".ddlStandard").val();
    var ddlMedium = $("#ddlMedium");
    ddlMedium.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading Mediums.....</option>');
    $.ajax({
        type: "GET",
        url: _urlMediumList,
        data: { standardId: _standardId },
        dataType: "json",
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            
            ddlMedium.empty().append('<option selected="selected" value="0">-- Select Medium --</option>');
            $.each(response, function () {

                ddlMedium.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        },
        error: function () {
            
        }
    });
    return false;
}

function GetSubjectList() {
    var _standardId = $(".ddlStandard").val();
    var _mediumId = $("#ddlMedium").val();
    var ddlSubject = $("#ddlSubject");

    $("#MediumId").val(_mediumId);

    ddlSubject.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading Subjects.....</option>');
    $.ajax({
        type: "GET",
        url: _urlSubjectList,
        data: { standardId: _standardId, mediumId: _mediumId, },
        dataType: "json",
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            

            ddlSubject.empty().append('<option selected="selected" value="0">-- Select Subject --</option>');
            $.each(response, function () {
                ddlSubject.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        },
        error: function () {

        }
    });
    return false;
}

function OnSubjectChange(eval) {
    var _subjectId = $(eval).val();
    $("#SubjectId").val(_subjectId);
}

