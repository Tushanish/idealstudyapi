﻿var _alertDanger = "alert-danger"
var _alertSuccess = "alert-success";
var isSubjectEdit = false;

function ActiveDeactive(eval) {
    var _coupon = {};
    _coupon.CouponId= $(eval).attr("data-serialid");
    _coupon.IsActive = $(eval).prop("checked");
    debugger;
    $.ajax({
        type: "POST",
        url: _urlActiveDeactivateCoupon,
        data: '{_coupon: ' + JSON.stringify(_coupon) + '}',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function () {
            SuccessMessage($("#divAlertMessage"), "Data updated successfully!");
        },
        error: function () {
            ErrorMessage($("#divAlertMessage"), "Error while updating the data!");
        }
    });
    return false;
}

$(document).ready(function () {
    $('input[type=radio][name=couponRadio]').change(function () {
        if (this.value == 'unused') {
            LoadData(false, 0, 0);
            //$.ajax({
            //    type: "GET",
            //    url: _urlGetUnUsedCoupons + '',
            //    data: {
            //        unused: false
            //    },
            //    dataType: 'JSON',
            //    success: function (response) {
            //        reloadDataTable(response, false)
            //    },
            //    error: function () {
            //        alert("Dynamic content load failed.");
            //    }
            //});
        }
        else if (this.value == 'used') {
            LoadData(true, 0, 0);
            //$.ajax({
            //    type: "GET",
            //    url: _urlGetUnUsedCoupons,
            //    data: {
            //        unused: true
            //    },
            //    dataType: 'JSON',
            //    success: function (response) {
            //        reloadDataTable(response, true)
            //    },
            //    error: function () {
            //        alert("Dynamic content load failed.");
            //    }
            //});
        }
    });

    $('#filterData').click(function () {
        FilterData();
    });

    $('#btnActive').click(function () {
        var fromCouponId = parseInt($("#fromNumber").val(), 10);
        var toCouponId = parseInt($("#toNumber").val(), 10);
        //var _fromCoupon = {};
        //fromCouponId = $(eval).attr("data-serialid");
        //var _toCoupon = {};
        //_toCoupon.CouponId = $(eval).attr("data-serialid");

        var IsActive = true;
        debugger;
        $.ajax({
            type: "POST",
            url: _urlActiveDeactivateCoupon,
            //data: '{_fromCoupon: ' + JSON.stringify(_fromCoupon) + '}',
            data: '{fromCouponId: ' + JSON.stringify(fromCouponId) + ',' + 'toCouponId: ' + JSON.stringify(toCouponId) + ',' + 'isActive: ' + JSON.stringify(IsActive) + '}',
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function () {
                SuccessMessage($("#divAlertMessage"), "Data updated successfully!");
            },
            error: function () {
                ErrorMessage($("#divAlertMessage"), "Error while updating the data!");
            }
        });
    })

    $('#btnDeactive').click(function () {
        var fromCouponId = parseInt($("#fromNumber").val(), 10);
        var toCouponId = parseInt($("#toNumber").val(), 10);

        var IsActive = false;
        debugger;
        $.ajax({
            type: "POST",
            url: _urlActiveDeactivateCoupon,
            data: '{fromCouponId: ' + JSON.stringify(fromCouponId) + ',' +'toCouponId: ' + JSON.stringify(toCouponId) +','+'isActive: ' +JSON.stringify(IsActive)+'}',
            //data: { "fromCouponId": "fromCouponId", "toCouponId": "toCouponId", "isActive": "IsActive" },
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function () {
                SuccessMessage($("#divAlertMessage"), "Data updated successfully!");
            },
            error: function () {
                ErrorMessage($("#divAlertMessage"), "Error while updating the data!");
            }
        });
    })
});

function SuccessMessage(eval, textMessage) {
    $(eval).removeClass("d-none");
    $(eval).removeClass(_alertDanger);
    $(eval).removeClass(_alertSuccess);
    $(eval).find(".alert").addClass(_alertSuccess);
    $(eval).fadeIn(200).delay(3000).fadeOut(200)
    $(eval).find(".alert").find(".textMessage").text(textMessage);
}
function ErrorMessage(eval, textMessage) {
    $(eval).removeClass("d-none");
    $(eval).removeClass(_alertDanger);
    $(eval).removeClass(_alertSuccess);
    $(eval).find(".alert").addClass(_alertDanger);
    $(eval).fadeIn(200).delay(3000).fadeOut(200)
    $(eval).find(".alert").find(".textMessage").text(textMessage);
}




function reloadDataTable(newData, used) {
    var datatable = $('#tblCouponDetails').DataTable();

    if ($.fn.dataTable.isDataTable('#tblCouponDetails')) {
        datatable.destroy();
    }
    datatable = $('#tblCouponDetails').DataTable({
        "paging": true,
        "serverSide": true,
        "searching": true,
        "ordering": true,
        "orderMulti":true,
        "info": true,
        "autoWidth": false,
        "lengthchange": true,
        "pagelength": 20,
        "pagingType": "full_numbers",
        "scroller": true,
        //"deferRender": true,
        "lengthMenu": [10, 25, 50],
        "data": newData,
        "columns": [
            { "data": "CouponId" },
            { "data": "CouponCode" },
            { "data": "IsActive" },
            { "data": "SponsorerName" },
            { "data": "CreatedDate" },
        ],
        dom: 'Bfrtip',
        buttons: ['excelHtml5']
    });
}


function FilterData() {
    debugger;
    //var table = $('#tblCouponDetails').DataTable();

    //$.fn.dataTable.ext.search.push(
    //    function (settings, data, dataIndex) {
    //        var min = parseInt($("#fromNumber").val(), 10);
    //        var max = parseInt($("#toNumber").val(), 10);
    //        var couponId = parseFloat(data[0]) || 0;
    //        if (((isNaN(min) && isNaN(max)) || (isNaN(min) && couponId <= max) ||
    //            (min <= couponId && isNaN(max)) ||
    //            (min <= couponId && couponId <= max))) {
    //            return true;
    //        }
    //        else {
    //            return false;
    //        }
    //        return false;
    //    });

    //table.draw();


    var from = parseInt($("#fromNumber").val(), 10);
    var to = parseInt($("#toNumber").val(), 10);
    let usedCoupons = $('input[type=radio][name=couponRadio]').val();


    if (isNaN(from) || isNaN(to)) {
        from = 0
        to = 0
    }

    if (usedCoupons == "used") {
        LoadData(true, from, to);
    }
}


