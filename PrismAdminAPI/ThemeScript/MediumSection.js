﻿var _alertDanger = "alert-danger"
var _alertSuccess = "alert-success";

function ActiveDeactive(eval) {
    var _mediumSection = {};
    _mediumSection.SectionId = $(eval).attr("data-sectionId");
    _mediumSection.MediumId = $(eval).attr("data-mediumId");
    _mediumSection.IsActive = $(eval).prop("checked");
    $.ajax({
        type: "POST",
        url: _urlActiveDeactiveMediumSection,
        data: '{_mediumSection: ' + JSON.stringify(_mediumSection) + '}',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function () {
            SuccessMessage($("#divAlertMessage"), "Data updated successfully!");
        },
        error: function () {
            ErrorMessage($("#divAlertMessage"), "Error while updating the data!");
        }
    });
    return false;
}

function SuccessMessage(eval, textMessage) {
    $(eval).removeClass("d-none");
    $(eval).removeClass(_alertDanger);
    $(eval).removeClass(_alertSuccess);
    $(eval).find(".alert").addClass(_alertSuccess);
    $(eval).fadeIn(200).delay(3000).fadeOut(200)
    $(eval).find(".alert").find(".textMessage").text(textMessage);
}
function ErrorMessage(eval, textMessage) {
    $(eval).removeClass("d-none");
    $(eval).removeClass(_alertDanger);
    $(eval).removeClass(_alertSuccess);
    $(eval).find(".alert").addClass(_alertDanger);
    $(eval).fadeIn(200).delay(3000).fadeOut(200)
    $(eval).find(".alert").find(".textMessage").text(textMessage);
}
