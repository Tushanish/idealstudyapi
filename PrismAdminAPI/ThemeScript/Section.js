﻿var _alertDanger = "alert-danger"
var _alertSuccess = "alert-success";

function ActiveDeactive(eval) {
    var _section = {};
    _section.SectionId = $(eval).attr("data-serialid");
    _section.IsActive = $(eval).prop("checked");
    $.ajax({
        type: "POST",
        url: _urlActiveDeactiveSection,
        data: '{_section: ' + JSON.stringify(_section) + '}',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function () {
            SuccessMessage($("#divAlertMessage"), "Data updated successfully!");
        },
        error: function () {
            ErrorMessage($("#divAlertMessage"), "Error while updating the data!");
        }
    });
    return false;
}

function ActiveDeactiveStandardSection(eval) {
    var _section = {};
    _section.SectionId = $(eval).attr("data-serialid");
    _section.IsStandardSection = $(eval).prop("checked");
    $.ajax({
        type: "POST",
        url: _urlActiveDeactiveStandardSection,
        data: '{_section: ' + JSON.stringify(_section) + '}',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function () {
            SuccessMessage($("#divAlertMessage"), "Data updated successfully!");
        },
        error: function () {
            ErrorMessage($("#divAlertMessage"), "Error while updating the data!");
        }
    });
    return false;
}

function ActiveDeactiveChapterSection(eval) {
    var _section = {};
    _section.SectionId = $(eval).attr("data-serialid");
    _section.IsChapterSection = $(eval).prop("checked");
    $.ajax({
        type: "POST",
        url: _urlActiveDeactiveChapterSection,
        data: '{_section: ' + JSON.stringify(_section) + '}',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function () {
            SuccessMessage($("#divAlertMessage"), "Data updated successfully!");
        },
        error: function () {
            ErrorMessage($("#divAlertMessage"), "Error while updating the data!");
        }
    });
    return false;
}

function SuccessMessage(eval, textMessage) {
    $(eval).removeClass("d-none");
    $(eval).removeClass(_alertDanger);
    $(eval).removeClass(_alertSuccess);
    $(eval).find(".alert").addClass(_alertSuccess);
    $(eval).fadeIn(200).delay(3000).fadeOut(200)
    $(eval).find(".alert").find(".textMessage").text(textMessage);
}
function ErrorMessage(eval, textMessage) {
    $(eval).removeClass("d-none");
    $(eval).removeClass(_alertDanger);
    $(eval).removeClass(_alertSuccess);
    $(eval).find(".alert").addClass(_alertDanger);
    $(eval).fadeIn(200).delay(3000).fadeOut(200)
    $(eval).find(".alert").find(".textMessage").text(textMessage);
}
