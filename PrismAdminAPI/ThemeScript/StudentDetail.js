﻿var _alertDanger = "alert-danger"
var _alertSuccess = "alert-success";
var isSubjectEdit = false;

//function ActiveDeactive(eval) {
//    //var _sponsorer = {};
//    //_sponsorer.SponsorerId = $(eval).attr("data-serialid");
//    //_sponsorer.IsActive = $(eval).prop("checked");
//    //debugger;
//    //$.ajax({
//    //    type: "POST",
//    //    url: _urlActiveDeactiveSubject,
//    //    data: '{_sponsorer: ' + JSON.stringify(_sponsorer) + '}',
//    //    dataType: "json",
//    //    contentType: "application/json; charset=utf-8",
//    //    success: function () {
//    //        SuccessMessage($("#divAlertMessage"), "Data updated successfully!");
//    //    },
//    //    error: function () {
//    //        debugger;
//    //        ErrorMessage($("#divAlertMessage"), "Error while updating the data!");
//    //    }
//    //});
//    //return false;
//}

$(document).ready(function () {
    $('input[type=radio][name=couponRadio]').change(function () {
        if (this.value == 'unused') {
            LoadData(false,0,0);
            //$.ajax({
            //    type: "GET",
            //    url: _urlGetUnUsedCoupons + '',
            //    data: {
            //        unused: false
            //    },
            //    dataType: 'JSON',
            //    success: function (response) {
            //        LoadData();
            //    },
            //    error: function () {
            //        alert("Dynamic content load failed.");
            //    }
            //});
        }
        else if (this.value == 'used') {
            LoadData(true,0,0);
            //$.ajax({
            //    type: "GET",
            //    url: _urlGetUnUsedCoupons,
            //    data: {
            //        unused: true
            //    },
            //    dataType: 'JSON',
            //    success: function (response) {
            //        //reloadDataTable(response,true)
            //    },
            //    error: function () {
            //        alert("Dynamic content load failed.");
            //    }
            //});
        }
    });

    

   
    $('#filterData').click(function () {
        FilterData();
    });
    $('#clearDateFilter').click(function () {
        var lastLoginDate = $("#lastLoginDate").val();
        $("#lastLoginDate").val('');
    });
    $('#clearDateFilter').click(function () {
        var lastLoginDate = $("#lastLoginDate").val();
        $("#lastLoginDate").val('');
    });
    

});


function reloadDataTable(newData,used) {
    var datatable = $('#tblStudentDetails').DataTable();

    if ($.fn.dataTable.isDataTable('#tblStudentDetails')) {
        datatable.destroy();
    }
    datatable = $('#tblStudentDetails').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "pagelength": 20,
        "pagingType": "full_numbers",
        "scroller": true,
        "deferRender": true,
        "lengthMenu": [10, 25, 50],
        "data": newData,
        "columns": [
            { "data": "CouponId" },
            { "data": "CouponCode" },
            { "data": "FirstName" },
            { "data": "FatherName" },
            { "data": "LastName" },
            { "data": "MediumName" },
            { "data": "PhoneNumber" },
            { "data": "Password" },
            { "data": "DateOfBirth" },
            { "data": "LoginDate" },
            { "data": "LastLoginDate" },
            { "data": "SponsorerName" },
            {
                "data" : "LoginId",
                "render": function (data) {
                    if (used) {
                        return '<button class="btn btn-primary" onclick="javascript: location.href= '+ "'editPassword/" + data + '\'">Edit</button>'
                    }
                    else {
                        return ''
                    }
                }
            },
        ],
        dom: 'Bfrtip',
        buttons: ['excelHtml5']
    });
}

function EditPassword(loginId) {
    $.ajax({
        type: "POST",
        url: _urlEditPassword,
        data: { 'loginId': loginId },
        dataType : 'JSON',
        success: function (response) {
            $('#editPasswordModal').html(response);
            $('#editPasswordModal').modal('show');

        },
        error: function () {
            alert("Failed to load the edit password form!!!");
        }
    });     
}

function SuccessMessage(eval, textMessage) {
    $(eval).removeClass("d-none");
    $(eval).removeClass(_alertDanger);
    $(eval).removeClass(_alertSuccess);
    $(eval).find(".alert").addClass(_alertSuccess);
    $(eval).fadeIn(200).delay(3000).fadeOut(200)
    $(eval).find(".alert").find(".textMessage").text(textMessage);
}
function ErrorMessage(eval, textMessage) {
    $(eval).removeClass("d-none");
    $(eval).removeClass(_alertDanger);
    $(eval).removeClass(_alertSuccess);
    $(eval).find(".alert").addClass(_alertDanger);
    $(eval).fadeIn(200).delay(3000).fadeOut(200)
    $(eval).find(".alert").find(".textMessage").text(textMessage);
}


function FilterData() {
    var from = parseInt($("#fromNumber").val(), 10);
    var to = parseInt($("#toNumber").val(), 10);
    let usedCoupons = $('input[type=radio][name=couponRadio]').val();


    if (isNaN(from) || isNaN(to)) {
        from = 0
        to = 0
    }

    if (usedCoupons == "used") {
        LoadData(true, from, to);
    }
   

   
    //if ($.fn.dataTable.isDataTable('#tblStudentDetails')) {
    //    datatable.clear().destroy();
    //}

    ////$("#tblStudentDetails").DataTable({
    ////    "serverSide": "true",
    ////    "processing": "true",
    ////    "search": "true",
    ////    "paging": "true",
    ////    "pageLength": 1000,
    ////    "order": [0, "asc"],
    ////    "language": {
    ////        "processing": "Loading records... please wait!!"
    ////    },
    ////    dom: 'Bfrtip',
    ////    buttons: ['excelHtml5'],
    ////    "ajax": {
    ////        "url": "/StudentDetails/GetFromToCouponData",
    ////        "type": "POST",
    ////        "datatype": "json",
    ////        "data": {
    ////            "fromCouponId": from,
    ////            "toCouponId": to,
    ////            "usedCoupons": usedCoupons
    ////        }
    ////    },
    ////    "columns": [
    ////        { "data": "CouponId", "name": "Coupon Id", "autoWidth": true },
    ////        { "data": "CouponCode", "name": "Coupon Code", "autoWidth": true },
    ////        { "data": "FirstName", "name": "First Name", "autoWidth": true },
    ////        { "data": "FatherName", "name": "Father Name", "autoWidth": true },
    ////        { "data": "LastName", "name": "Last Name", "autoWidth": true },
    ////        { "data": "MediumName", "name": "Medium Name", "autoWidth": true },
    ////        { "data": "PhoneNumber", "name": "Phone Number", "autoWidth": true },
    ////        { "data": "Password", "name": "Password", "autoWidth": true },
    ////        { "data": "DateOfBirth", "name": "Date Of Birth", "autoWidth": true },
    ////        { "data": "LoginDate", "name": "Login Date", "autoWidth": true },
    ////        { "data": "LastLoginDate", "name": "Last Login Date", "autoWidth": true },
    ////        { "data": "SponsorerName", "name": "Sponsorer Name", "autoWidth": true },
    ////        {
    ////            "data": "LoginId",
    ////            "render": function (data) {
    ////                return '<button class="btn btn-primary" onclick="javascript: location.href= ' + "'editPassword/" + data + '\'">Edit</button>'
    ////            }
    ////        },
    ////    ],

    ////});

    //var table = $("#tblStudentDetails").DataTable();
    //$('#tblStudentDetails_filter input').unbind();
    //$('#tblStudentDetails_filter input').bind('keyup', function (e) {
    //    if (e.keyCode == 13) {
    //        table.search(this.value).draw();
    //    }
    //});

    //$.fn.dataTable.ext.search.push(
    //    function (settings, data, dataIndex) {
    //        var min = parseInt($("#fromNumber").val(), 10);
    //        var max = parseInt($("#toNumber").val(), 10);
    //        var lastLoginDate = $("#lastLoginDate").val();
    //        var couponId = parseFloat(data[0]) || 0; 
    //        var applyLastLogin = false;
    //        if (lastLoginDate !== undefined && lastLoginDate !== "") applyLastLogin = true

    //        if (applyLastLogin && data[10] !== undefined && data[10] !== "") {
    //            if (((isNaN(min) && isNaN(max)) || (isNaN(min) && couponId <= max) ||
    //                (min <= couponId && isNaN(max)) ||
    //                (min <= couponId && couponId <= max)) && data[10].split(' ')[0] == new Date(lastLoginDate).toLocaleDateString()) {
    //                return true;
    //            }
    //            else
    //                return false;
    //        }

    //        if (((isNaN(min) && isNaN(max)) || (isNaN(min) && couponId <= max) ||
    //            (min <= couponId && isNaN(max)) ||
    //                    (min <= couponId && couponId <= max)) && applyLastLogin == false) {
    //                return true;
    //            }
    //            else {
    //                return false;
    //            }
    //        return false;
    //    });

    //table.draw();
}

