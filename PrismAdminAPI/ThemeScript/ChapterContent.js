﻿function OnStandardChange() {
    var _standardId = $(".ddlStandards").val();
    var ddlMedium = $("#ddlMediums");
    ddlMedium.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading Mediums.....</option>');

    $.ajax({
        type: "GET",
        url: _urlMediumList,
        data: { standardId: _standardId },
        dataType: "json",
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (response) {

            ddlMedium.empty().append('<option selected="selected" value="0">-- Select Medium --</option>');
            $.each(response, function () {

                ddlMedium.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        },
        error: function (response) {

        }
    });
    return false;
}

function onMediumChange() {
    var _standardId = $(".ddlStandards").val();
    var _mediumId = $("#ddlMediums").val();
    $("#MediumId").val(_mediumId);

    var ddlSubject = $("#ddlSubjects");
    ddlSubject.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading Subjects.....</option>');
    $.ajax({
        type: "GET",
        url: _urlSubjectList,
        data: { standardId: _standardId, mediumId: _mediumId, },
        dataType: "json",
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            ddlSubject.empty().append('<option selected="selected" value="0">-- Select Subject --</option>');
            $.each(response, function () {
                ddlSubject.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        },
        error: function (response) {

        }
    });
    return false;
}

function onSubjectChange() {
    debugger;
    var _standardId = $(".ddlStandards").val();
    var _mediumId = $("#ddlMediums").val();
    var _subjectId = $("#ddlSubjects").val();
    $("#SubjectId").val(_subjectId);

    var ddlChapters = $("#ddlChapters");
    ddlChapters.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading Chapters.....</option>');

    $.ajax({
        type: "GET",
        url: _urlChapterList,
        data: { standardId: _standardId, mediumId: _mediumId, subjectId: _subjectId },
        dataType: "json",
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            ddlChapters.empty().append('<option selected="selected" value="0">-- Select Chapters --</option>');
            $.each(response, function () {
                ddlChapters.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        },
        error: function (response) {

        }
    });
    return false;
}

function onChapterChange() {
    var _mediumId = $("#ddlMediums").val();
    var _chapterId = $("#ddlChapters").val();

    $("#ChapterId").val(_chapterId);

    var ddlSections = $("#ddlSections");
    ddlSections.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading Sections.....</option>');

    $.ajax({
        type: "GET",
        url: _urlSectionList,
        data: { chapterId: _chapterId, mediumId: _mediumId },
        dataType: "json",
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            ddlSections.empty().append('<option selected="selected" value="0">-- Select Chapters --</option>');
            $.each(response, function () {
                ddlSections.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        },
        error: function (response) {

        }
    });
    return false;
}

function ShowHideContent() {
    if ($(".ddlChapterContentType option:selected").text() == "Text") {
        $("div.ChapterImageContents").hide();
        $("div.ChapterVideoContents").hide();
        $("div.ChapterTextContents").show();
    }
    else if ($(".ddlChapterContentType option:selected").text() == "Image") {
        $("div.ChapterImageContents").show();
        $("div.ChapterVideoContents").hide();
        $("div.ChapterTextContents").hide();
    }
    else if ($(".ddlChapterContentType option:selected").text() == "Video") {
        $("div.ChapterImageContents").hide();
        $("div.ChapterVideoContents").show();
        $("div.ChapterTextContents").hide();
    }
    $("#ContentTypeString").val($(".ddlChapterContentType").val())
}

function OnSubmitClick() {
    var isValid = true;
    if (parseInt($("#ddlMediums").val()) == 0) {
        $(".errorMediumId").text("");
        $(".errorMediumId").text("Please select medium");
        isValid = false;
    }
    if (parseInt($("#ddlSubjects").val()) == 0) {
        $(".errorSubjectId").text("");
        $(".errorSubjectId").text("Please select subject");
        isValid = false;
    }
    if (parseInt($("#ddlChapters").val()) == 0) {
        $(".errorChapterId").text("");
        $(".errorChapterId").text("Please select chapter");
        isValid = false;
    }
    if (parseInt($("#ddlSections").val()) == 0) {
        $(".errorSectionId").text("");
        $(".errorSectionId").text("Please select section");
        isValid = false;
    }

    if ($(".ddlChapterContentType option:selected").text() == "Image" && $("#ChapterImageContents").val().trim().length == 0) {
        $(".errorChapterImageContents").text("");
        $(".errorChapterImageContents").text("Please upload image");
        isValid = false;
    }
    if ($(".ddlChapterContentType option:selected").text() == "Image" && $("#ChapterImageContents").val().trim().length > 0 && !validateImageFile()) {
        $(".errorChapterImageContents").text("");
        $(".errorChapterImageContents").text("File names should be only in number e.g 1.jpg");
        isValid = false;
    }
    if ($(".ddlChapterContentType option:selected").text() == "Image" && $("#ChapterImageContents").val().trim().length > 0 && CheckDuplicateFileNames()) {
        $(".errorChapterImageContents").text("");
        $(".errorChapterImageContents").text("Duplicate file names not allowed");
        isValid = false;
    }
    if ($(".ddlChapterContentType option:selected").text() == "Video" && $("#ChapterVideoContents").val().trim().length == 0) {
        $(".errorChapterVideoContents").text("");
        $(".errorChapterVideoContents").text("Please upload video");

        isValid = false;
    }
    if ($(".ddlChapterContentType option:selected").text() == "Text" && $("#ChapterTextContents").summernote('isEmpty')) {
        $(".errorChapterTextContents").text("");
        $(".errorChapterTextContents").text("Please enter text content");
        isValid = false;
    }
    ShowHideContent();
    return isValid;
}

function validateImageFile() {
    var isValidFileName = true;
    var files = $('#ChapterImageContents').prop("files");
    var names = $.map(files, function (val) { return val.name; });
    for (var i = 0; i < names.length; i++) {
        var val = names[i];
        var fileName = val.split('.').slice(0, -1).join('.')
        if (isNaN(fileName)) {
            isValidFileName = false;
            break;
        }
    }
    return isValidFileName;
}

function PreviousImageFiles() {
    var imageFiles = [];
    $("#UploadedImageContents").find("table > tbody tr td:nth-child(2)").each(function () {
        var val = $(this).text();
        var fileName = val.split('.').slice(0, -1).join('.')
        imageFiles.push(fileName);
    });
    return imageFiles;
}

function CurrentImageFiles() {
    var imageFiles = [];
    var files = $('#ChapterImageContents').prop("files");
    var names = $.map(files, function (val) { return val.name; });
    for (var i = 0; i < names.length; i++) {
        var val = names[i];
        var fileName = val.split('.').slice(0, -1).join('.')
        imageFiles.push(fileName);
    }
    return imageFiles;
}

function CheckDuplicateFileNames() {
    var isDuplicate = false;
    var _previousImageFiles = PreviousImageFiles();
    if (_previousImageFiles.length > 0) {
        var _currentImageFiles = CurrentImageFiles();
        var duplicates = _previousImageFiles.filter(function (val) {
            return _currentImageFiles.indexOf(val) != -1;
        });
        if (duplicates.length > 0) {
            isDuplicate = true;
        }
    }
    return isDuplicate;
}


var _alertDanger = "alert-danger"
var _alertSuccess = "alert-success";

function SuccessMessage(eval, textMessage) {
    $(eval).removeClass("d-none");
    $(eval).removeClass(_alertDanger);
    $(eval).removeClass(_alertSuccess);
    $(eval).find(".alert").addClass(_alertSuccess);
    $(eval).fadeIn(200).delay(3000).fadeOut(200)
    $(eval).find(".alert").find(".textMessage").text(textMessage);
}
function ErrorMessage(eval, textMessage) {
    $(eval).removeClass("d-none");
    $(eval).removeClass(_alertDanger);
    $(eval).removeClass(_alertSuccess);
    $(eval).find(".alert").addClass(_alertDanger);
    $(eval).fadeIn(200).delay(3000).fadeOut(200)
    $(eval).find(".alert").find(".textMessage").text(textMessage);
}




