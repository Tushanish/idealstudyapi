﻿function OnStandardChange() {
    var _standardId = $(".ddlStandards").val();
    var ddlMedium = $("#ddlMediums");
    ddlMedium.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading Mediums.....</option>');

    $.ajax({
        type: "GET",
        url: _urlMediumList,
        data: { standardId: _standardId },
        dataType: "json",
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (response) {

            ddlMedium.empty().append('<option selected="selected" value="0">-- Select Medium --</option>');
            $.each(response, function () {

                ddlMedium.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        },
        error: function (response) {

        }
    });
    return false;
}

function onMediumChange() {
    var _standardId = $(".ddlStandards").val();
    var _mediumId = $("#ddlMediums").val();
    $("#MediumId").val(_mediumId);

    var ddlSubject = $("#ddlSubjects");
    ddlSubject.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading Subjects.....</option>');
    $.ajax({
        type: "GET",
        url: _urlSubjectList,
        data: { standardId: _standardId, mediumId: _mediumId, },
        dataType: "json",
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            ddlSubject.empty().append('<option selected="selected" value="0">-- Select Subject --</option>');
            $.each(response, function () {
                ddlSubject.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        },
        error: function (response) {

        }
    });
    return false;
}

function GetSectionList() {
    var ddlSections = $("#ddlSections");
    ddlSections.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading Sections.....</option>');

    $.ajax({
        type: "GET",
        url: _urlSectionList,
        data: {},
        dataType: "json",
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            ddlSections.empty().append('<option selected="selected" value="0">-- Select Section --</option>');
            $.each(response, function () {
                ddlSections.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        },
        error: function (response) {

        }
    });
    return false;
}

function OnSubmitClick() {
    var isValid = true;
    
    if (parseInt($("ddlStandards").val()) == 0) {
        $(".errorStandardId").text("");
        $(".errorStandardId").text("Please select Standard");
        isValid = false;
    }
    if (parseInt($("#ddlMediums").val()) == 0) {
        $(".errorMediumId").text("");
        $(".errorMediumId").text("Please select medium");
        isValid = false;
    }
    if (parseInt($("#ddlSections").val()) == 0) {
        $(".errorSectionId").text("");
        $(".errorSectionId").text("Please select section");
        isValid = false;
    }
    if ($("#SubjectTextContents").summernote('isEmpty')) {
        $(".errorSubjectTextContents").text("");
        $(".errorSubjectTextContents").text("Please enter text content");
        isValid = false;
    }
    return isValid;
}
