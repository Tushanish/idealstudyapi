﻿var _alertDanger = "alert-danger"
var _alertSuccess = "alert-success";

function ActiveDeactive(eval) {
    var _standardMedium = {};
    _standardMedium.StandardId = $(eval).attr("data-standardId");
    _standardMedium.MediumId = $(eval).attr("data-mediumId");
    _standardMedium.IsActive = $(eval).prop("checked");
    $.ajax({
        type: "POST",
        url: _urlActiveDeactiveStandardMedium,
        data: '{_standardMedium: ' + JSON.stringify(_standardMedium) + '}',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function () {
            SuccessMessage($("#divAlertMessage"), "Data updated successfully!");
        },
        error: function () {
            ErrorMessage($("#divAlertMessage"), "Error while updating the data!");
        }
    });
    return false;
}

function SuccessMessage(eval, textMessage) {
    $(eval).removeClass("d-none");
    $(eval).removeClass(_alertDanger);
    $(eval).removeClass(_alertSuccess);
    $(eval).find(".alert").addClass(_alertSuccess);
    $(eval).fadeIn(200).delay(3000).fadeOut(200)
    $(eval).find(".alert").find(".textMessage").text(textMessage);
}
function ErrorMessage(eval, textMessage) {
    $(eval).removeClass("d-none");
    $(eval).removeClass(_alertDanger);
    $(eval).removeClass(_alertSuccess);
    $(eval).find(".alert").addClass(_alertDanger);
    $(eval).fadeIn(200).delay(3000).fadeOut(200)
    $(eval).find(".alert").find(".textMessage").text(textMessage);
}
