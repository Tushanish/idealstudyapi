﻿function AddNewQuestion(event) {
    var id;
    id = parseInt($("#questionDiv").children().last().attr('id')) + 1;
    var html = getQuestionDivHtml(id, 1);
    $("#questionDiv").append(html);
}

function DeleteQuestion(element) {
    var id = parseInt(element.parentElement.parentElement.parentElement.parentElement.id);
    $("#questionDiv").children()[$("#" + element.id + "").index()].remove();
}

function addAnswer(event, questionId) {
    var answerDivId = parseInt($("#answerDiv" + questionId).children().last().attr('id')) + 1;
    var answerHtml = getAnswerDivHtml(answerDivId, questionId);
    $("#answerDiv" + questionId).append(answerHtml);
}

function deleteAnswer(event, id) {
    var index = $("#" + event.parentElement.parentElement.parentElement.parentElement.id + "").index();
    $("#answerDiv" + id).children()[$("#" + event.id + "").index()].remove();
}

function getQuestionDivHtml(id, answerId) {
    return '<div class="card card-dark" id="' + id.toString() + '"  > <div class="card-header" > <div class="row">'
        + '<div class="col-sm-10"> '
        + '<h1 class="card-title questionPadding" style="text-align:center; font-size:20px;">Question No ' + id.toString() + '</h1>'
        + '</div>'
        + ' <div class="col-sm-1">'
        + '<button onclick="AddNewQuestion(this)" id="' + id.toString() + '" class="btn">'
        + '<i class="fa fa-plus"></i>'
        + '</button>'
        + '</div>'
        + '<div class="col-sm-1">'
        + '<button onclick="DeleteQuestion(this)" id="' + id.toString() + '" class="btn">'
        + '<i class="fa fa-minus"></i>'
        + '</button>'
        + '</div>'
        + '</div>'
        + '</div>'
        + '<div class="card-body questionBack" style="font-size:20px;">'
        + '<div class="row">'
        + '<div class="col-md-9">'
        + '<input type="text" class="form-control" placeholder="Enter the question here...." id="txtbox' + id.toString() + '" />'
        + '<label id = "lbl' + id.toString() + '" style = "font-size:20px;" hidden="hidden" ></label>'
        + '</div> '
        + ' <div class="col-md-3"> '
        + ' <button class="btn btn-success" id = "' + answerId.toString() + '" onClick = "addAnswer(this,' + id.toString() + ')">'
        + ' Add Answer '
        + ' <i class="fa fa-plus" ></i > '
        + ' </button>'
        + ' <button class="btn btn-success" id="' + answerId.toString() + '" onClick="editQuestionAnswer(this,' + id.toString() + ' )"> Edit Q & A '
        + ' <i class="fa fa-edit" ></i > '
        + ' </button>'
        + ' <button class="btn btn-success" id="' + answerId.toString() + '" onClick="saveQuestionAnswer(this,' + id.toString() + ' )"> Save '
        + ' <i class="fa fa-save" ></i > '
        + ' </button>'
        + ' </div>'
        + ' </div>'
        + ' </div>'
        + '<div class="card-body answerCard-back" id="answerDiv' + id.toString() + '" >'
        + '<div class="card answerCard" id="' + answerId.toString() + '">'
        + '<div class="card-body answercard-body">'
        + '<div class="row">'
        + '<div class="col-md-10" style="font-size:25px;">'
        + '<input type="text" name = "' + answerId.toString() + '" placeholder="Enter Answer here...." class="form-control" id="' + answerId.toString() + '"/>'
        + ' <label id="lbl' + answerId.toString() + '" style = "font-size:20px;" ></label > '
        + '</div> '
        + '<div class="col-md-1"> '
        + '<button class="btn"'
        + ' onclick="answerChecked(this,answerDiv' + id.toString() + ')"'
        + 'id="' + answerId.toString() + '"> '
        + '<span> '
        + ' <i class="hoverIcon fa fa-circle-o" style="font-size:28px;color:green"> '
        + '</i> '
        + '</span> '
        + '</button> '
        + '</div> '
        + '<div class="col-md-1">'
        + '<button class="btn btn-danger"'
        + ' onclick = "deleteAnswer(this,' + id.toString() + ')" '
        + ' id="' + answerId.toString() + '"> <i class="fa fa-times"></i></button> '
        + '</div>'
        + '</div>'
        + '</div>'
        + '</div>'
        + '</div>'
        + '</div>'
        + '</div>'
}

function getAnswerDivHtml(id, questionId) {
    return '<div id="' + id.toString() + '" class="card answerCard"> '
        + '<div class="card-body answercard-body"> '
        + '<div class="row">'
        + '<div class="col-md-10" style="font-size:25px;color:white;"> '
        + ' <input type="text" placeholder="Enter Answer here...." class="form-control" id="' + id.toString() + '" /> '
        + '<label id="lbl' + id.toString() + '" style = "font-size:20px;" ></label>'
        + '</div> '
        + '<div class="col-md-1"> '
        + '<button class="btn"'
        + 'onclick="answerChecked(this,answerDiv' + questionId.toString() + ')"'
        + 'id="' + id.toString() + '"> '
        + '<span> '
        + '<i class="hoverIcon fa fa-circle-o" style="font-size:28px;color:green"> '
        + '</i> '
        + '</span> '
        + '</button> '
        + '</div> '
        + '<div class="col-md-1"> '
        + '<button class="btn btn-danger" '
        + 'onclick = "deleteAnswer(this,' + questionId.toString() + ')" '
        + ' id="' + id.toString() + '"> <i class="fa fa-times"></i></button> '
        + '</div>'
        + '</div>'
        + '</div>'
        + '</div>';
}

function answerChecked(event, answerDivId) {
    debugger;
    var eventDiv = $("#" + answerDivId.id + "").children()[$("#" + event.id+"").index()];
    //var eventDiv = $("#" + answerDivId.id + "").find($("#" + event.id + ""));
    var divs = $("#" + answerDivId.id + "").children().toArray();
    for (var i = 0; i < divs.length; i++) {
        if (divs[i].id === event.id) {
            eventDiv = divs[i];
        }
    }

    if ($(eventDiv).find('i').first().hasClass('fa-circle-o')) {
        $("#" + answerDivId.id + "").children()
            .find('.fa-check-circle')
            .removeClass('fa-check-circle')
            .addClass('fa-circle-o');

        //$("#" + answerDivId.id + "").children().find('i').addClass('fa-circle-o');

        $(eventDiv).find('.fa-circle-o').removeClass('fa-circle-o').addClass('fa-check-circle');
    }
    else {
        //$("#" + answerDivId.id + "").children().find('i').removeClass('fa-check-circle');

        $(eventDiv).find('.fa-check-circle').removeClass('fa-check-circle').addClass('fa-circle-o');
    }
}

function editQuestionAnswer(id, answerId) {

    $('#questionDiv').children().eq($("#" + answerId + "").index()).find('input').attr('hidden', false);

    $('#questionDiv').children().eq($("#" + answerId + "").index()).find('label').attr('hidden', true);
}

function saveQuestionAnswer(id, answerId) {

    
    var childrens = $('#questionDiv').children().eq($("#" + answerId + "").index()).find('input').toArray();
    for (var i = 0; i < childrens.length; i++) {
        value = childrens[i].value;
        $('#questionDiv').children().eq($("#" + answerId + "").index()).first().find('label')[i].innerText = value;
    }

    $('#questionDiv').children().eq($("#" + answerId + "").index()).find('input').attr('hidden', true);

    $('#questionDiv').children().eq($("#" + answerId + "").index()).find('label').attr('hidden', false);

   
}


$("#loadMcq").click(function () {
    $("#btnSubmit").prop('hidden', false);
    var standardId = $("#standardId").val();
    var mediumId = $("#mediumId").val();
    var subjectId = $("#subjectId").val();
    var chapterId = $("#chapterId").val();
    var sectionId = $("#sectionId").val();
    
  
    $.ajax({
        type: "GET",
        url: '/Mcq/GetMcq',     //your action
        data: 'standardId=' + standardId + '&mediumId=' + mediumId + '&subjectId=' + subjectId + '&chapterId=' + chapterId + '&sectionId=' + sectionId,
        contentType: "application/json; charset=utf-8",
        dataType: 'html',
      
        success: function (result) {
            $("#divMcq").html(result);
          
        },
        error: function (xhr) {
            console.log(xhr);
        }
    });
});

function SubmitMcq() {
  
    var model =  PrepareObject();
    $.ajax({
        type: "POST",
        url: '/Mcq/SaveMcq',     //your action
        data: JSON.stringify(model),
        contentType: "application/json; charset=utf-8",
        dataType: 'html',
        success: function (result) {
            //alert("sucessfully added !!!");
            FilterMcqAfterSave();
            $("#btnSubmit").prop('hidden', true);
        },
        error: function (xhr) {
            console.log(xhr);
        }
    });
}

function GetCorrectAnswersOnly() {
    FilterMcqAfterSave();
    $("#btnSubmit").prop('hidden', true);
}

function FilterMcqAfterSave() {
    var standardId = $("#standardId").val();
    var mediumId = $("#mediumId").val();
    var subjectId = $("#subjectId").val();
    var chapterId = $("#chapterId").val();
    var sectionId = $("#sectionId").val();

    $.ajax({
        type: "GET",
        url: '/Mcq/GetMcqAfterSubmit',     //your action
        data: 'standardId=' + standardId + '&mediumId=' + mediumId + '&subjectId=' + subjectId + '&chapterId=' + chapterId + '&sectionId=' + sectionId,
        contentType: "application/json; charset=utf-8",
        dataType: 'html',

        success: function (result) {
            $("#divMcq").html(result);
        },
        error: function (xhr) {
            console.log(xhr);
        }
    });
}

class LnkQuestionChapter {
    standardId;
    subjectId;
    chapterId;
    mediumId;
    sectionId;
    questions = [];
}

class Question {
    chapterId
    questionId;
    questionNo;
    questionName;
    answers = [];
}

class Answer {
    answerId;
    answerNo;
    questionNo; 
    isCorrect;
    answerName;
}

function PrepareObject() {
    var chapterObj = new LnkQuestionChapter();

    var standardId = $("#standardId").val();
    var mediumId = $("#mediumId").val();
    var subjectId = $("#subjectId").val();
    var chapterId = $("#chapterId").val();
    var sectionId = $("#sectionId").val();

    chapterObj.chapterId = chapterId;
    chapterObj.mediumId = mediumId;
    chapterObj.sectionId = sectionId;
    chapterObj.subjectId = subjectId;
    chapterObj.standardId = standardId;

    var divs = $('#questionDiv').children().toArray();
    
    for (var i = 0; i < divs.length; i++) {
        var id = $('#questionDiv').children().eq(i).attr('id');
        var questionBox = $('#questionDiv').children().find('.questionBack').find('input').toArray();
        var answerBoxes = $('#questionDiv').children().find('.answerCard-back').eq(i).find('input').toArray();
            
        var questionObj = new Question();
        questionObj.questionId = id;
        questionObj.questionName = questionBox[i].value;
        questionObj.questionNo = i + 1;

        questionObj.answers = [];
        for (var j = 0; j < answerBoxes.length; j++) {

            var inputBox = answerBoxes[j];
           
            var checkedAnswer = $(inputBox.parentElement.parentElement).find('i').hasClass('fa-check-circle');

            var answer = new Answer();
            answer.answerId = inputBox.id;
            answer.answerNo = j+1;
            answer.answerName = inputBox.value;
            answer.isCorrect = checkedAnswer;
            answer.questionNo = questionObj.questionNo;
            questionObj.answers.push(answer);
        }
        chapterObj.questions.push(questionObj);
    }

    return chapterObj;
}

function OnStandardChange() {
    var _standardId = $(".ddlStandard").val();
    var ddlMedium = $("#mediumId");
    ddlMedium.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading Mediums.....</option>');

    $.ajax({
        type: "GET",
        url: _urlMediumList,
        data: { standardId: _standardId },
        dataType: "json",
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (response) {

            ddlMedium.empty().append('<option selected="selected" value="0">-- Select Medium --</option>');
            $.each(response, function () {

                ddlMedium.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        },
        error: function (response) {

        }
    });
    return false;
}

function onMediumChange() {
    var _standardId = $(".ddlStandard").val();
    var _mediumId = $("#mediumId").val();
    $("#mediumId").val(_mediumId);

    var ddlSubject = $("#subjectId");
    ddlSubject.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading Subjects.....</option>');
    $.ajax({
        type: "GET",
        url: _urlSubjectList,
        data: { standardId: _standardId, mediumId: _mediumId },
        dataType: "json",
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            ddlSubject.empty().append('<option selected="selected" value="0">-- Select Subject --</option>');
            $.each(response, function () {
                ddlSubject.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        },
        error: function (response) {

        }
    });
    return false;
}

function onSubjectChange() {
    var _standardId = $(".ddlStandard").val();
    var _mediumId = $("#mediumId").val();
    var _subjectId = $("#subjectId").val();
    $("#subjectId").val(_subjectId);

    var ddlChapters = $("#chapterId");
    ddlChapters.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading Chapters.....</option>');

    $.ajax({
        type: "GET",
        url: _urlChapterList,
        data: { standardId: _standardId, mediumId: _mediumId, subjectId: _subjectId },
        dataType: "json",
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            ddlChapters.empty().append('<option selected="selected" value="0">-- Select Chapters --</option>');
            $.each(response, function () {
                ddlChapters.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        },
        error: function (response) {

        }
    });
    return false;
}

function onChapterChange() {
    var _mediumId = $("#mediumId").val();
    var _chapterId = $("#chapterId").val();

    $("#chapterId").val(_chapterId);

    var ddlSections = $("#sectionId");
    ddlSections.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading Sections.....</option>');

    $.ajax({
        type: "GET",
        url: _urlSectionList,
        data: { chapterId: _chapterId, mediumId: _mediumId },
        dataType: "json",
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            ddlSections.empty().append('<option selected="selected" value="0">-- Select Chapters --</option>');
            $.each(response, function () {
                ddlSections.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        },
        error: function (response) {

        }
    });
    return false;
}
