﻿using Dapper;
using PrismAdminAPI.BAL;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Repository
{
    public class SubjectContentRepository : ISubjectContentRepository
    {
        public long InsertSubjectContent(SubjectContent _subjectContent)
        {
            long _addResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _subjectContentParameters = new DynamicParameters();
                    _subjectContentParameters.Add("@StandardId", _subjectContent.StandardId);
                    _subjectContentParameters.Add("@SubjectId", _subjectContent.SubjectId);
                    _subjectContentParameters.Add("@MediumId", _subjectContent.MediumId);
                    _subjectContentParameters.Add("@SectionId", _subjectContent.SectionId);
                    _subjectContentParameters.Add("@SubjectTextContents", _subjectContent.SubjectTextContents);
                    _subjectContentParameters.Add("@SubjectContentId", _subjectContent.SubjectContentId, DbType.Int64, ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspInsertSubjectContent", _subjectContentParameters, commandType: CommandType.StoredProcedure);
                    _addResult = _subjectContentParameters.Get<long>("SubjectContentId");
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _addResult;
        }

        public List<SubjectContentViewModel> GetSubjectContent(long? _standardId = null,long ? _mediumId = null, long? _subjectId = null,  long? _sectionId = null)
        {
            List<SubjectContentViewModel> _SubjectContentViewModel = new List<SubjectContentViewModel>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _subjectContentParameters = new DynamicParameters();
                    _subjectContentParameters.Add("@StandardId", _standardId);
                    _subjectContentParameters.Add("@MediumId", _mediumId);
                    _subjectContentParameters.Add("@SubjectId", _subjectId);
                    _subjectContentParameters.Add("@SectionId ", _sectionId);
                    _SubjectContentViewModel = _sqlConnection.Query<SubjectContentViewModel>("uspGetSubjectContent", _subjectContentParameters, commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _SubjectContentViewModel;
        }
    }
}   