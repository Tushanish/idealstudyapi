﻿using Dapper;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Repository
{
    public class AdminRepository : IAdminRepository
    {
        public Admin ValidateAdmin(UserLogin _userLogin)
        {
            Admin _admin = null;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _adminParameters = new DynamicParameters();
                    _adminParameters.Add("@UserName", _userLogin.UserName);
                    _adminParameters.Add("@Password", _userLogin.Password);

                    _admin = _sqlConnection.Query<Admin>("Admin.uspLoginAdmin", _adminParameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _admin;
        }

        public string[] GetUserRoles(string _userName)
        {
            Admin _admin = null;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    DynamicParameters _adminParameters = new DynamicParameters();
                    _adminParameters.Add("@UserName", _userName);
                    _admin = _sqlConnection.Query<Admin>("Admin.uspGetUserRoles", _adminParameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            if (_admin != null)
            {
                return new string[] { _admin.RoleName };
            }
            else
            {
                return null;
            }
        }

        public bool InsertAdmin(Admin admin)
        {
            long _addResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _adminParameters = new DynamicParameters();
                    _adminParameters.Add("@UserName", admin.UserName);
                    _adminParameters.Add("@Password", admin.Password);
                    _adminParameters.Add("@RoleId", admin.RoleId);
                    _adminParameters.Add("@IsActive", admin.IsActive);
                    _adminParameters.Add("@ReturnId",0, DbType.Int64, ParameterDirection.Output);
                    
                    var returnValue = _sqlConnection.Execute("uspInsertAdmin", _adminParameters, commandType: CommandType.StoredProcedure);
                    _addResult = _adminParameters.Get<long>("ReturnId");
                    _sqlConnection.Close();

                    if (_addResult > 0)
                    {
                        return true;
                    }
                    else return false;
                   
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool UpdateAdmin(Admin admin)
        {
            long _updResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _adminParameters = new DynamicParameters();
                    _adminParameters.Add("@UserName", admin.UserName);
                    _adminParameters.Add("@Password", admin.Password);
                    _adminParameters.Add("@RoleId", admin.RoleId);
                    _adminParameters.Add("@IsActive", admin.IsActive);
                    _adminParameters.Add("@ReturnId", 0, DbType.Int64, ParameterDirection.Output);

                    var returnValue = _sqlConnection.Execute("uspUpdateAdmin", _adminParameters, commandType: CommandType.StoredProcedure);
                    _updResult = _adminParameters.Get<long>("ReturnId");
                    _sqlConnection.Close();

                    if (_updResult > 0)
                    {
                        return true;
                    }
                    else return false;

                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool ActivateDeactivateAdmin(long _adminId, bool _isActive)
        {
            bool _updateResult;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _adminParameters = new DynamicParameters();
                    _adminParameters.Add("@AdminId", _adminId);
                    _adminParameters.Add("@IsActive", _isActive);
                    _adminParameters.Add("@RowCount", 0, DbType.Int32, ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspActiveDeactivateAdmin", _adminParameters, commandType: CommandType.StoredProcedure);
                    var updateResult = _adminParameters.Get<int>("RowCount");
                    _sqlConnection.Close();

                    if (updateResult > 0) _updateResult = true;
                    else _updateResult = false;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _updateResult;
        }

        public IEnumerable<Admin> GetAdminListByAdminId(long adminId)
        {
            IList<Admin> _adminList = new List<Admin>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _adminParameters = new DynamicParameters();
                    _adminParameters.Add("@AdminId", adminId);

                    _adminList = _sqlConnection.Query<Admin>("uspGetAllAdmins", _adminParameters, commandType: CommandType.StoredProcedure)
                                               .ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _adminList;
        }

        public IEnumerable<Role> GetRolesByRoleId(long roleId)
        {
            IList<Role> _adminList = new List<Role>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _roleParameters = new DynamicParameters();
                    _roleParameters.Add("@RoleId", roleId);

                    _adminList = _sqlConnection.Query<Role>("uspGetRoles", _roleParameters, commandType: CommandType.StoredProcedure)
                                               .ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _adminList;
        }
    }
}