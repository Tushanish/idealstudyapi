﻿using Dapper;
using PrismAdminAPI.BAL;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Repository
{
    public class SponsorerAdminRepository : ISponsorerAdminRepository
    {
        private readonly IDbConnection connection = new SqlConnection(WebConfig.ConnectionString);

        public IEnumerable<SponsorerCoupon> GetCouponsForSponsorer(long sponsorerId)
        {
            IList<SponsorerCoupon> _coupons = new List<SponsorerCoupon>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _couponParameters = new DynamicParameters();
                    _couponParameters.Add("@SponsorerId", sponsorerId);

                    _coupons = _sqlConnection.Query<SponsorerCoupon>("uspGetAllCouponsForSponsorer", _couponParameters, commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _coupons;
        }

        public IEnumerable<StudentDetail> GetStudentDetailsForSponsorer(bool unused ,long sponsorerId)
        {
            IList<StudentDetail> _studentDetails = new List<StudentDetail>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();

                    DynamicParameters _studentDetailsParameters = new DynamicParameters();
                    _studentDetailsParameters.Add("@SponsorerId", sponsorerId);
                    _studentDetailsParameters.Add("@UsedCoupon", unused);

                    _studentDetails = _sqlConnection.Query<StudentDetail>("uspGetSponsorerStudentInformation", _studentDetailsParameters, commandType: CommandType.StoredProcedure).ToList();

                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _studentDetails;
        }
    }
}