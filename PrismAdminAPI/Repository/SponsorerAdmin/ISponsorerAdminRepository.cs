﻿using System.Collections.Generic;
using PrismAdminAPI.Models;

namespace PrismAdminAPI.Repository
{
    public interface ISponsorerAdminRepository
    {
        IEnumerable<SponsorerCoupon> GetCouponsForSponsorer(long sponsorerId);

        IEnumerable<StudentDetail> GetStudentDetailsForSponsorer(bool unusedCoupons,long sponsorerId);
    }
}