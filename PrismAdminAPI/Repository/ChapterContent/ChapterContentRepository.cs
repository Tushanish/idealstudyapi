﻿using Dapper;
using PrismAdminAPI.BAL;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Repository
{
    public class ChapterContentRepository : IChapterContentRepository
    {
        public long InsertChapterContent(ChapterContent _chapterContent)
        {
            long _addResult = 0;
            try
            {
                
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    if (_chapterContent.ContentTypeString.Equals(((int)ChapterContentType.Video).ToString()))
                    {
                        _chapterContent = ChapterContentUtility.UploadChapterContents(_chapterContent);
                        DynamicParameters _chapterParameters = new DynamicParameters();
                        _chapterParameters.Add("@ChapterId", _chapterContent.ChapterId);
                        _chapterParameters.Add("@SectionId", _chapterContent.SectionId);
                        _chapterParameters.Add("@ContentType", (ChapterContentType)Enum.Parse(typeof(ChapterContentType), _chapterContent.ContentTypeString));
                        _chapterParameters.Add("@ChapterTextContents", _chapterContent.ChapterTextContents);
                        _chapterParameters.Add("@ChapterVideoContents", _chapterContent.ChapterVideoContentsPath);
                        _chapterParameters.Add("@ChapterImageContents", _chapterContent.ChapterImageContentsPath);
                        _chapterParameters.Add("@IsSectionDisabled", _chapterContent.IsSectionDisabled);
                        _chapterParameters.Add("@ChapterContentId", _chapterContent.ChapterContentId, DbType.Int64, ParameterDirection.Output);
                        var result = _sqlConnection.Execute("uspInsertChapterContent", _chapterParameters, commandType: CommandType.StoredProcedure);
                        _addResult = _chapterParameters.Get<long>("ChapterContentId");
                        _sqlConnection.Close();
                    }
                    if (_chapterContent.ContentTypeString.Equals(((int)ChapterContentType.Image).ToString()))
                    {
                        _chapterContent = ChapterContentUtility.UploadChapterContents(_chapterContent);
                        foreach (var item in _chapterContent.ChapterImageContentsPath)
                        {
                            DynamicParameters _chapterParameters = new DynamicParameters();
                            _chapterParameters.Add("@ChapterId", _chapterContent.ChapterId);
                            _chapterParameters.Add("@SectionId", _chapterContent.SectionId);
                            _chapterParameters.Add("@ContentType", (ChapterContentType)Enum.Parse(typeof(ChapterContentType), _chapterContent.ContentTypeString));
                            _chapterParameters.Add("@ChapterTextContents", _chapterContent.ChapterTextContents);
                            _chapterParameters.Add("@ChapterVideoContents", _chapterContent.ChapterVideoContentsPath);
                            _chapterParameters.Add("@ChapterImageContents", item);
                            _chapterParameters.Add("@IsSectionDisabled", _chapterContent.IsSectionDisabled);
                            _chapterParameters.Add("@ChapterContentId", _chapterContent.ChapterContentId, DbType.Int64, ParameterDirection.Output);

                            var result = _sqlConnection.Execute("uspInsertChapterContent", _chapterParameters, commandType: CommandType.StoredProcedure);
                            _addResult = _chapterParameters.Get<long>("ChapterContentId");
                            _sqlConnection.Close();
                        }
                    }
                    if (_chapterContent.ContentTypeString.Equals(((int)ChapterContentType.Text).ToString()))
                    {
                        DynamicParameters _chapterParameters = new DynamicParameters();
                        _chapterParameters.Add("@ChapterId", _chapterContent.ChapterId);
                        _chapterParameters.Add("@SectionId", _chapterContent.SectionId);
                        _chapterParameters.Add("@ContentType", (ChapterContentType)Enum.Parse(typeof(ChapterContentType), _chapterContent.ContentTypeString));
                        _chapterParameters.Add("@ChapterTextContents", _chapterContent.ChapterTextContents);
                        _chapterParameters.Add("@ChapterVideoContents", _chapterContent.ChapterVideoContentsPath);
                        _chapterParameters.Add("@ChapterImageContents", _chapterContent.ChapterImageContentsPath);
                        _chapterParameters.Add("@IsSectionDisabled", _chapterContent.IsSectionDisabled);
                        _chapterParameters.Add("@ChapterContentId", _chapterContent.ChapterContentId, DbType.Int64, ParameterDirection.Output);

                        var result = _sqlConnection.Execute("uspInsertChapterContent", _chapterParameters, commandType: CommandType.StoredProcedure);
                        _addResult = _chapterParameters.Get<long>("ChapterContentId");
                        _sqlConnection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _addResult;
        }


        public List<ChapterContentViewModel> GetChapterContent(long? _standardId = null, long? _mediumId = null, long? _chapterId = null, long? _sectionId = null, long? _subjectId = null, bool? _isSingleRecord = true)
        {
            List<ChapterContentViewModel> _chapterContentViewModel = new List<ChapterContentViewModel>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _studentInfoParameters = new DynamicParameters();
                    _studentInfoParameters.Add("@StandardId", _standardId);
                    _studentInfoParameters.Add("@MediumId", _mediumId);
                    _studentInfoParameters.Add("@ChapterId", _chapterId);
                    _studentInfoParameters.Add("@SectionId ", _sectionId);
                    _studentInfoParameters.Add("@SubjectId", _subjectId);
                    _studentInfoParameters.Add("@IsSingleRecord", _isSingleRecord);
                    _chapterContentViewModel = _sqlConnection.Query<ChapterContentViewModel>("uspGetChapterContent", _studentInfoParameters, commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _chapterContentViewModel;
        }

        public bool DeleteChapterContent(long _chapterContentId)
        {
            bool IsDeleted = false;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _studentInfoParameters = new DynamicParameters();
                    _studentInfoParameters.Add("@ChapterContentId", _chapterContentId);
                    //int isDeleted = _sqlConnection.Query<int>("UPDATE Mas_ChapterContent SET IsActive=0 WHERE ChapterContentId=@ChapterContentId", _studentInfoParameters, commandType: CommandType.Text).FirstOrDefault();
                    int isDeleted = _sqlConnection.Query<int>("DELETE from Mas_ChapterContent WHERE ChapterContentId=@ChapterContentId", _studentInfoParameters, commandType: CommandType.Text).FirstOrDefault();
                    _sqlConnection.Close();

                    IsDeleted = isDeleted == 0; 
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return IsDeleted;
        }

        public bool EnableDisableChapterSection(long _chapterId, long _sectionId,bool _isDisable)
        {
            int _updateResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _chapterContentParameters = new DynamicParameters();
                    _chapterContentParameters.Add("@ChapterId", _chapterId);
                    _chapterContentParameters.Add("@SectionId", _sectionId);
                    _chapterContentParameters.Add("@IsDisable", _isDisable);

                    _chapterContentParameters.Add("@RowCount", 0, DbType.Int32, ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspDisableChapterSection", _chapterContentParameters, commandType: CommandType.StoredProcedure);
                    _updateResult = _chapterContentParameters.Get<int>("RowCount");
                    _sqlConnection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _updateResult > 0 ? true : false;
        }
    }
}   