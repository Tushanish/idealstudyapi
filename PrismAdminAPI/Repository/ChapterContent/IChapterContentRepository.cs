﻿using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrismAdminAPI.Repository
{
    public interface IChapterContentRepository
    {
        long InsertChapterContent(ChapterContent _chapterContent);

        List<ChapterContentViewModel> GetChapterContent(long? _standardId = null, long? _mediumId = null, long? _chapterId = null, long? _sectionId = null, long? _subjectId = null, bool? _isSingleRecord = true);

        bool DeleteChapterContent(long _chapterContentId);

        bool EnableDisableChapterSection(long _chapterId, long _sectionId, bool _isDisable);
    }
}
