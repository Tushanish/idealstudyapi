﻿using Dapper;
using PrismAdminAPI.Models;
using PrismAdminAPI.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Repository
{
    public class ConfigurationRepository : IConfigurationRepository
    {
        #region Standard Medium Configuration 
        public IEnumerable<StandardMedium> GetStandardMediamConfiguration(long? _standardMediumId = null)
        {
            IList<StandardMedium> _standardMediumList = new List<StandardMedium>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _standardParameters = new DynamicParameters();
                    _standardParameters.Add("@StandardMediumId", _standardMediumId);

                    _standardMediumList = _sqlConnection.Query<StandardMedium>("uspGetAllStandardMediumConfig", _standardParameters, commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _standardMediumList;
        }

        public long InsertStandardMedium(StandardMedium _standardMedium)
        {
            long _addResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _standardmediumParameters = new DynamicParameters();
                    _standardmediumParameters.Add("@StandardId", _standardMedium.StandardId);
                    _standardmediumParameters.Add("@MediumId", _standardMedium.MediumId);
                    _standardmediumParameters.Add("@IsActive", _standardMedium.IsActive);
                    _standardmediumParameters.Add("@StandardMediumId", _standardMedium.StandardMediumId, DbType.Int64, ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspInsertUpdateStandardMediumConfig", _standardmediumParameters, commandType: CommandType.StoredProcedure);
                    _addResult = _standardmediumParameters.Get<long>("StandardMediumId");
                    _sqlConnection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _addResult;
        }

        public IEnumerable<StandardMedium> GetConfiguredStandardMediamConfiguration(long? _standardId = null)
        {
            IList<StandardMedium> _standardMediumList = new List<StandardMedium>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _standardParameters = new DynamicParameters();
                    _standardParameters.Add("@StandardId", _standardId);

                    _standardMediumList = _sqlConnection.Query<StandardMedium>("uspGetConfiguredStandardMedium", _standardParameters, commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _standardMediumList;
        }

        public IEnumerable<SubjectViewModel> GetConfiguredSubjectList(long _standardId, long _mediumId)
        {
            IList<SubjectViewModel> _subjectList = new List<SubjectViewModel>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _standardParameters = new DynamicParameters();
                    _standardParameters.Add("@StandardId", _standardId);
                    _standardParameters.Add("@MediumId", _mediumId);

                    _subjectList = _sqlConnection.Query<SubjectViewModel>("uspGetConfiguredSubjectList", _standardParameters, commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _subjectList;
        }

        #endregion Standard Medium Configuration 

        #region Medium Section Configuration 
        public IEnumerable<MediumSections> GetMediamSectionConfiguration(long? _mediumSectionId = null)
        {
            IList<MediumSections> _mediumSectionList = new List<MediumSections>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _sectionParameters = new DynamicParameters();
                    _sectionParameters.Add("@MediumSectionId", _mediumSectionId);

                    _mediumSectionList = _sqlConnection.Query<MediumSections>("uspGetAllMediumSectionConfig", _sectionParameters, commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _mediumSectionList;
        }

        public long InsertMediumSection(MediumSections _mediumSection)
        {
            long _addResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _mediumSectionParameters = new DynamicParameters();
                    _mediumSectionParameters.Add("@SectionId", _mediumSection.SectionId);
                    _mediumSectionParameters.Add("@MediumId", _mediumSection.MediumId);
                    _mediumSectionParameters.Add("@IsActive", _mediumSection.IsActive);
                    _mediumSectionParameters.Add("@MediumSectionId", _mediumSection.MediumSectionId, DbType.Int64, ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspInsertUpdateMediumSectionConfig", _mediumSectionParameters, commandType: CommandType.StoredProcedure);
                    _addResult = _mediumSectionParameters.Get<long>("MediumSectionId");
                    _sqlConnection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _addResult;
        }

        #endregion Medium Section Configuration 

    }
}