﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using PrismAdminAPI.Models;

namespace PrismAdminAPI.Repository.StudentDetails
{
    public class StudentDetailsRepository : IStudentDetailsRepository
    {
        public IEnumerable<StudentDetail> GetStudentDetails( int loginId , bool usedCoupons, out long totalrecords, int start = 0, int length = 0)
        {
            IEnumerable<StudentDetail> _studentDetails = new List<StudentDetail>();
            try
            {
                totalrecords = 0;
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    
                    _sqlConnection.Open();
                    
                    DynamicParameters _studentDetailsParameters = new DynamicParameters();
                    _studentDetailsParameters.Add("@LoginId", loginId);
                    _studentDetailsParameters.Add("@UsedCoupon", usedCoupons);
                    _studentDetailsParameters.Add("@Start", start);
                    _studentDetailsParameters.Add("@end", length);

                    _studentDetails = _sqlConnection.Query<StudentDetail>("uspGetStudentInformation", _studentDetailsParameters, commandType: CommandType.StoredProcedure, buffered:true,commandTimeout:1000).ToList();
                   
                    _sqlConnection.Close();
                }

                if (usedCoupons)
                {
                    using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                    {
                        _sqlConnection.Open();
                        totalrecords = _sqlConnection.ExecuteScalar<long>($"Select COUNT(couponId) from mas_coupon where isused = 1",null,null,null,null);
                    }
                }
                else
                {
                    using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                    {
                        _sqlConnection.Open();
                        totalrecords = _sqlConnection.ExecuteScalar<long>($"Select COUNT(couponId) from mas_coupon where isused = 0", null, null, null, null);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            
            return _studentDetails;
        }

        public bool UpdatePassword(int loginId , string password,string mobileNumber,string firstName , string lastName, int mediumId)
        {
            try
            {
                int studentDetailsValue;
                int mediumChangeValue;
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    string studentDetailsSql = $"UPDATE MAS_LOGIN SET Password = N'{password}' , PhoneNumber = '{mobileNumber}' , FirstName = N'{firstName}' , LastName = N'{lastName}' where loginId = {loginId}";
                    studentDetailsValue = _sqlConnection.Execute(studentDetailsSql, null, null, null, CommandType.Text);
                    string mediumChangeSql = $"UPDATE MAS_STUDENT SET MediumId = {mediumId} where loginId = {loginId}";
                    mediumChangeValue = _sqlConnection.Execute(mediumChangeSql, null, null, null, CommandType.Text);

                    DynamicParameters _singleSignParameters = new DynamicParameters();
                    _singleSignParameters.Add("@CouponId", 0);
                    _singleSignParameters.Add("@LoginId", loginId);
                    _singleSignParameters.Add("@ReturnValue", null, DbType.Int16, ParameterDirection.Output);

                    _sqlConnection.Execute("uspDeleteSingleSignIn", _singleSignParameters, commandType: CommandType.StoredProcedure);

                    _sqlConnection.Close();
                }

                if(studentDetailsValue > 0 && mediumChangeValue > 0) 
                    return true;
            }
            catch (Exception ex)
            {
                
            }
            return false;
        }


        public IEnumerable<StudentDetail> GetFilterStudentDetails(string searchValue, bool isUsedCoupons,long fromCouponId,long toCouponId)
        {
            IEnumerable<StudentDetail> _studentDetails = new List<StudentDetail>();
            using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
            {

                _sqlConnection.Open();

                DynamicParameters _studentDetailsParameters = new DynamicParameters();
                _studentDetailsParameters.Add("@SearchValue", searchValue);
                _studentDetailsParameters.Add("@isUsed", isUsedCoupons);
                _studentDetailsParameters.Add("@fromCouponId", fromCouponId);
                _studentDetailsParameters.Add("@toCouponId", toCouponId);

                _studentDetails = _sqlConnection.Query<StudentDetail>("uspFilterStudentDetails", _studentDetailsParameters, commandType: CommandType.StoredProcedure, buffered: true, commandTimeout: 1000).ToList();

                _sqlConnection.Close();
            }
            return _studentDetails;
        }

    }
}