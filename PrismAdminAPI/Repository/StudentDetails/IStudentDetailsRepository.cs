﻿using System.Collections.Generic;
using PrismAdminAPI.Models;

namespace PrismAdminAPI.Repository.StudentDetails
{
    public interface IStudentDetailsRepository
    {
        IEnumerable<StudentDetail> GetStudentDetails(int loginId, bool usedCoupons,  out long totalrecords, int start, int length);
        bool UpdatePassword(int loginId, string password , string mobileNumber,
            string firstName, string lastName, int mediumId);

        IEnumerable<StudentDetail> GetFilterStudentDetails(string searchValue, bool isusedCoupons, long fromCouponId, long toCouponId);
    }
}