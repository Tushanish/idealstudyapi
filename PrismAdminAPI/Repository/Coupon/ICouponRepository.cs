﻿using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Repository
{
    public interface ICouponRepository
    {
        bool SaveBulkCoupon<T>(int batchSize, string tableName, List<T> _list);

        IEnumerable<Coupon> GetAllCoupons(long? couponId = null, bool? isActive = null);

        bool ActivateDeactivateCoupon(long? fromCouponId, long? toCouponId, bool IsActive);

        IEnumerable<LnkCouponDetails> GetAllLnkCouponDetails(long? couponId = null, long? sponsorerId = null);

        IEnumerable<Coupon> GetCouponsForIds(List<string> couponIds);

        IEnumerable<CouponDetails> GetCouponDetails(string searchValue,bool usedCoupons, out long totalrecords, long fromCouponId = 0, long toCouponId = 0, long start = 0, long length = 0);
    }
}