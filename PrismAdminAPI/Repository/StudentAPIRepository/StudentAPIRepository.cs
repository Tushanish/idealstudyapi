﻿using Dapper;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;
using PrismAdminAPI.ViewModel;

namespace PrismAdminAPI.Repository
{
    public class StudentAPIRepository : IStudentAPIRepository
    {
        public IEnumerable<Medium> GetMediumList()
        {
            IList<Medium> _mediumList = new List<Medium>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    _mediumList = _sqlConnection.Query<Medium>("Student.uspGetmediumList", commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _mediumList;
        }

        public Student StudentRegistration(Login _login, bool parseDate)
        {
            Student _student = new Student();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _studentInfoParameters = new DynamicParameters();
                    _studentInfoParameters.Add("@PhoneNumber", _login.PhoneNumber);
                    _studentInfoParameters.Add("@FirstName", _login.FirstName);
                    _studentInfoParameters.Add("@FatherName", _login.FatherName);
                    _studentInfoParameters.Add("@LastName", _login.LastName);
                    _studentInfoParameters.Add("@Password", _login.Password);

                    if (parseDate)
                        _studentInfoParameters.Add("@DateOfBirth", DateTime.ParseExact(_login.DateOfBirth, "dd/mm/yyyy", null).ToString("mm/dd/yyyy"));
                    else
                        _studentInfoParameters.Add("@DateOfBirth", _login.DateOfBirth);

                    _studentInfoParameters.Add("@StandardId", _login.StandardId);
                    _studentInfoParameters.Add("@MediumId", _login.MediumId);
                    _studentInfoParameters.Add("@CouponId", _login.CouponId);
                    _studentInfoParameters.Add("@ExpiryDate", Convert.ToDateTime(ConfigurationManager.AppSettings["ExpiryDate"]));
                    _student = _sqlConnection.Query<Student>("Student.uspRegisterUser", _studentInfoParameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _student;
        }


        public Student StudentLogin(Login _login)
        {
            Student _student = new Student();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _studentInfoParameters = new DynamicParameters();
                    _studentInfoParameters.Add("@PhoneNumber", _login.PhoneNumber);
                    _studentInfoParameters.Add("@Password", _login.Password);
                    _student = _sqlConnection.Query<Student>("Student.uspLoginUser", _studentInfoParameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _student;
        }

        public IEnumerable<SubjectViewModel> GetSubjectList(long loginId)
        {
            IList<SubjectViewModel> _listSubject = new List<SubjectViewModel>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _studentInfoParameters = new DynamicParameters();
                    _studentInfoParameters.Add("@LoginId", loginId);
                    _listSubject = _sqlConnection.Query<SubjectViewModel>("Student.uspSubjectList", _studentInfoParameters, commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _listSubject;
        }


        public IEnumerable<Chapter> GetChapterList(long _standardId, long _subjectId, long _mediumId)
        {
            IList<Chapter> _listChapter = new List<Chapter>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _studentInfoParameters = new DynamicParameters();
                    _studentInfoParameters.Add("@SubjectId", _subjectId);
                    _studentInfoParameters.Add("@StandardId", _standardId);
                    _studentInfoParameters.Add("@MediumId", _mediumId);
                    _listChapter = _sqlConnection.Query<Chapter>("Student.uspChapterList", _studentInfoParameters, commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _listChapter;
        }

        public IEnumerable<SectionMediumViewModel> GetChapterSection(bool _isChapterSection, long _mediumId, bool _isActive, long _subjectId, long _chapterId)
        {
            IList<SectionMediumViewModel> _sectionMediumList = new List<SectionMediumViewModel>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _sectionParameters = new DynamicParameters();
                    _sectionParameters.Add("@IsChapterSection", _isChapterSection);
                    _sectionParameters.Add("@MediumId", _mediumId);
                    _sectionParameters.Add("@IsActive", _isActive);
                    _sectionParameters.Add("@SubjectId", _subjectId);
                    _sectionParameters.Add("@ChapterId", _chapterId);


                    _sectionMediumList = _sqlConnection.Query<SectionMediumViewModel>("Student.uspGetChapterSection", _sectionParameters, commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _sectionMediumList;
        }

        public IEnumerable<SectionMediumViewModel> GetStandardSection(bool _isStandardSection, long _subjectId, long _mediumId, bool _isActive)
        {
            IList<SectionMediumViewModel> _sectionMediumList = new List<SectionMediumViewModel>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _sectionParameters = new DynamicParameters();
                    _sectionParameters.Add("@IsStandardSection", _isStandardSection);
                    _sectionParameters.Add("@MediumId", _mediumId);
                    _sectionParameters.Add("@SubjectId", _subjectId);
                    _sectionParameters.Add("@IsActive", _isActive);


                    _sectionMediumList = _sqlConnection.Query<SectionMediumViewModel>("Student.uspGetStandardSection", _sectionParameters, commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _sectionMediumList;
        }

        public List<ChapterContentViewModel> GetChapterContentForSection(long chapterId, long sectionId)
        {
            List<ChapterContentViewModel> _chapterContentViewModel = new List<ChapterContentViewModel>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _studentInfoParameters = new DynamicParameters();
                    _studentInfoParameters.Add("@ChapterId", chapterId);
                    _studentInfoParameters.Add("@SectionId", sectionId);
                    _chapterContentViewModel = _sqlConnection.Query<ChapterContentViewModel>("Student.uspGetSectionContents", _studentInfoParameters, commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _chapterContentViewModel;
        }


        public SubjectContentViewModel GetSubjectContentForSection(long subjectId, long sectionId)
        {
            SubjectContentViewModel _subjectContentViewModel = new SubjectContentViewModel();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _studentInfoParameters = new DynamicParameters();
                    _studentInfoParameters.Add("@SubjectId", subjectId);
                    _studentInfoParameters.Add("@SectionId", sectionId);
                    _subjectContentViewModel = _sqlConnection.Query<SubjectContentViewModel>("Student.uspGetSubjectSectionContents", _studentInfoParameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _subjectContentViewModel;
        }

        public Coupon ValidateCoupon(string _couponCode)
        {
            Coupon _coupon = new Coupon();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _standardParameters = new DynamicParameters();
                    _standardParameters.Add("@CouponCode", _couponCode);

                    _coupon = _sqlConnection.Query<Coupon>("Student.uspValidateCoupon", _standardParameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _coupon;
        }

        public bool ValidateMobileNumber(string _phoneNumber)
        {
            bool isDuplicateMobileNumber = false;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _studentParameters = new DynamicParameters();
                    _studentParameters.Add("@PhoneNumber", _phoneNumber);
                    _studentParameters.Add("@IsDuplicatePhoneNumber", isDuplicateMobileNumber, DbType.Boolean, ParameterDirection.Output);
                    var result = _sqlConnection.Execute("Student.uspValidateMobileNumber", _studentParameters, commandType: CommandType.StoredProcedure);
                    isDuplicateMobileNumber = _studentParameters.Get<bool>("IsDuplicatePhoneNumber");
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isDuplicateMobileNumber;
        }

        public bool UpdateAppActiveTime(long loginId)
        {
            using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
            {
                try
                {
                    //int lastLoginUpdate;
                    //int logActiveUserTimeSpentInsert;
                    //_sqlConnection.Open();
                    //    string lastLoginUpdateSql = $"UPDATE MAS_LOGIN SET LastLoginDate = GETDATE(),IsLoginActive = 0 where loginId = {loginId}";
                    //lastLoginUpdate = _sqlConnection.Execute(lastLoginUpdateSql, null, null, null, CommandType.Text);

                    //string logInsert = $"INSERT INTO Log_AppTimeSpent (LoginId,InTime,OutTime,CreatedDate) VALUES({loginId}, GETDATE(), GETDATE(), GETDATE())";
                    //logActiveUserTimeSpentInsert = _sqlConnection.Execute(logInsert, null, null, null, CommandType.Text);
                    //_sqlConnection.Close();

                    //if (lastLoginUpdate > 0 && logActiveUserTimeSpentInsert > 0)
                    //    return true;
                    //else
                    //    return false;
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }


        }

    }
}