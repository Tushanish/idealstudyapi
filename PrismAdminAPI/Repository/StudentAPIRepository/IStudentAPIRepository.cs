﻿using PrismAdminAPI.Models;
using PrismAdminAPI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrismAdminAPI.Repository
{
    public interface IStudentAPIRepository
    {
        IEnumerable<Medium> GetMediumList();

        Student StudentRegistration(Login _login , bool parseDate);

        Student StudentLogin(Login _login);

        IEnumerable<SubjectViewModel> GetSubjectList(long loginId);

        IEnumerable<Chapter> GetChapterList(long _standardId, long _subjectId, long _mediumId);

        IEnumerable<SectionMediumViewModel> GetChapterSection(bool _isChapterSection, long _mediumId, bool _isActive,long _subjectId, long _chapterId);

        IEnumerable<SectionMediumViewModel> GetStandardSection(bool _isStandardSection, long _subjectId, long _mediumId, bool _isActive);

        List<ChapterContentViewModel> GetChapterContentForSection(long chapterId, long sectionId);

        SubjectContentViewModel GetSubjectContentForSection(long subjectId, long sectionId);

        Coupon ValidateCoupon(string _couponCode);

        bool ValidateMobileNumber(string _phoneNumber);

        bool UpdateAppActiveTime(long loginId);
    }
}
