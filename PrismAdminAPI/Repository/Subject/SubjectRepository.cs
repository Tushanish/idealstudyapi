﻿using Dapper;
using PrismAdminAPI.BAL;
using PrismAdminAPI.Models;
using PrismAdminAPI.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Repository
{
    public class SubjectRepository : ISubjectRepository
    {
        public IEnumerable<SubjectViewModel> GetSubjectList(long? subjectId = null, bool? isActive = null)
        {
            IList<SubjectViewModel> _subjectList = new List<SubjectViewModel>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _subjectParameters = new DynamicParameters();
                    _subjectParameters.Add("@SubjectId", subjectId);
                    _subjectParameters.Add("@IsActive", isActive);

                    _subjectList = _sqlConnection.Query<SubjectViewModel>("uspGetAllSubject", _subjectParameters, commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _subjectList;
        }

        public long InsertSubject(Subject _subject)
        {
            long _addResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    if (_subject.SubjectThumbnail != null)
                    {
                        _subject = ChapterContentUtility.UploadSubjectThumbnail(_subject);
                    }

                    _sqlConnection.Open();
                    DynamicParameters _subjectParameters = new DynamicParameters();
                    _subjectParameters.Add("@SubjectName", _subject.SubjectName);
                    _subjectParameters.Add("@SubjectThumbnail", _subject.SubjectThumbnailPath);
                    _subjectParameters.Add("@Description", _subject.Description);
                    
                    _subjectParameters.Add("@SubjectId", _subject.SubjectId, DbType.Int64, ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspInsertSubject", _subjectParameters, commandType: CommandType.StoredProcedure);
                    _addResult = _subjectParameters.Get<long>("SubjectId");
                    InsertSubjectDetails(_subject.StandardId, _addResult, _subject.Description, _subject.MediumIdList);
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _addResult;
        }

        public int UpdateSubject(Subject _subject)
        {
            int _updateResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    if (_subject.SubjectThumbnail != null)
                    {
                        _subject = ChapterContentUtility.UploadSubjectThumbnail(_subject);
                    }

                    DynamicParameters _subjectParameters = new DynamicParameters();
                    _subjectParameters.Add("@SubjectId", _subject.SubjectId);
                    _subjectParameters.Add("@SubjectName", _subject.SubjectName);
                    _subjectParameters.Add("@SubjectThumbnail", _subject.SubjectThumbnailPath);
                    _subjectParameters.Add("@Description", _subject.Description);

                    _subjectParameters.Add("@RowCount", 0, DbType.Int32, ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspUpdateSubject", _subjectParameters, commandType: CommandType.StoredProcedure);
                    _updateResult = _subjectParameters.Get<int>("RowCount");
                    InsertSubjectDetails(_subject.StandardId, _subject.SubjectId, _subject.Description, _subject.MediumIdList);
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _updateResult;
        }

        public int ActivateDeactivateSubject(long _subjectId, bool _isActive)
        {
            int _updateResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _standardParameters = new DynamicParameters();
                    _standardParameters.Add("@SubjectId", _subjectId);
                    _standardParameters.Add("@IsActive", _isActive);

                    _standardParameters.Add("@RowCount", 0, DbType.Int32, ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspActiveDeactivateSubject", _standardParameters, commandType: CommandType.StoredProcedure);
                    _updateResult = _standardParameters.Get<int>("RowCount");
                    _sqlConnection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _updateResult;
        }

        private void InsertSubjectDetails(long _standardId, long _subjectId,string _description , string _mediumList)
        {
            long[] mediumIdInt = _mediumList.Split(',').Select(long.Parse).ToArray();
            try
            {
                foreach (var mediumId in mediumIdInt)
                {
                    long _subjectDetailsId = 0;
                    using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                    {
                        _sqlConnection.Open();
                        DynamicParameters _subjectParameters = new DynamicParameters();
                        _subjectParameters.Add("@StandardId", _standardId);
                        _subjectParameters.Add("@SubjectId", _subjectId);
                        _subjectParameters.Add("@MediumId", mediumId);
                        _subjectParameters.Add("@IsActive", true);
                        _subjectParameters.Add("@SubjectDetailsId", _subjectDetailsId, DbType.Int64, ParameterDirection.Output);

                        var result = _sqlConnection.Execute("uspInsertSubjectDetails", _subjectParameters, commandType: CommandType.StoredProcedure);
                        _subjectDetailsId = _subjectParameters.Get<long>("SubjectDetailsId");
                        _sqlConnection.Close();
                    }
                }
                
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}