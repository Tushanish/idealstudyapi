﻿using PrismAdminAPI.Models;

namespace PrismAdminAPI.Repository
{
    public interface ISingleSignInUserRepository
    {
        SingleSignInUser GetSingleSignInDetails(long? _loginId, long? _couponId,string uuid);

        bool RemoveSingleSignInUser(long? _loginId, long? _couponId,string uuid);

        bool IsDuplicateLogin(SingleSignInUser details, string uuid);
    }
}