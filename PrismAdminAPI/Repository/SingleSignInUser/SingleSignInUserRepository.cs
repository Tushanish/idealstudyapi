﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using PrismAdminAPI.Models;

namespace PrismAdminAPI.Repository
{
    public class SingleSignInUserRepository : ISingleSignInUserRepository
    {
        private readonly IDbConnection connection = new SqlConnection(WebConfig.ConnectionString);

        public SingleSignInUser GetSingleSignInDetails(long? _loginId,long? _couponId,string uuid)
        {
            IList<SingleSignInUser> singleSignInDetails = new List<SingleSignInUser>();
            SingleSignInUser details;
            try
            {
                using (connection)
                {
                    connection.Open();
                    DynamicParameters _singleSignParameters = new DynamicParameters();
                    _singleSignParameters.Add("@CouponId", _couponId);
                    _singleSignParameters.Add("@LoginId", _loginId);
                    _singleSignParameters.Add("@UUID", uuid);

                    details = connection.Query<SingleSignInUser>("uspValidateSingleSignIn", _singleSignParameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return details;
        }

        public bool RemoveSingleSignInUser(long? _loginId,long? _couponId, string uuid)
        {
            try
            {
                using (connection)
                {
                    connection.Open();
                    DynamicParameters _singleSignParameters = new DynamicParameters();
                    _singleSignParameters.Add("@CouponId", _couponId);
                    _singleSignParameters.Add("@LoginId", _loginId);
                    _singleSignParameters.Add("@ReturnValue", null, DbType.Int16, ParameterDirection.Output);

                    connection.Execute("uspDeleteSingleSignIn", _singleSignParameters, commandType: CommandType.StoredProcedure);
                    long.TryParse(_singleSignParameters.Get<object>("ReturnValue").ToString(), out long result);
                    connection.Close();
                    return result > 0;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool IsDuplicateLogin(SingleSignInUser details, string uuid)
        {
            if(details != null && details.MobileUUID != null && details.MobileUUID != uuid)
            {
                return true;
            }

            return false;
        }
    }
}