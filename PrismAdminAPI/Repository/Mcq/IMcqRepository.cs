﻿using PrismAdminAPI.Models;
using System.Collections.Generic;
using System.Data;

namespace PrismAdminAPI.Repository
{
    public interface IMcqRepository
    {
        bool InsertMcq(long? subjectId, long? chapterId, long? mediumId, long? sectionId, long? standardId, DataTable questionTable, DataTable answerTable);
        bool UpdateMcq(long? subjectId, long? chapterId, long? mediumId, long? sectionId, long? standardId, DataTable questionTable, DataTable answerTable);

        IEnumerable<Question> GetQuestions(long? subjectId, long? chapterId, long? mediumId, long? sectionId, long? standardId);

        IEnumerable<Answer> GetAnswers(long? questionNo, long? chapterId);
    }
}