﻿using Dapper;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Repository
{
    public class McqRepository : IMcqRepository
    {

        public bool InsertMcq(long? subjectId,long? chapterId, long? mediumId,long? sectionId,long? standardId,DataTable questionTable,
            DataTable answerTable)
        {
            long _addResult = 0;
            try
            {
                using (SqlConnection connection = new SqlConnection(WebConfig.ConnectionString))
                {
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.Parameters.AddWithValue("@SubjectId", subjectId);
                    command.Parameters.AddWithValue("@ChapterId", chapterId);
                    command.Parameters.AddWithValue("@MediumId", mediumId);
                    command.Parameters.AddWithValue("@SectionId", sectionId);
                    command.Parameters.AddWithValue("@StandardId", standardId);
                    command.Parameters.Add(new SqlParameter { ParameterName = "@Questions", Value = questionTable, SqlDbType = SqlDbType.Structured  });
                    command.Parameters.Add(new SqlParameter { ParameterName = "@Answers", Value = answerTable, SqlDbType = SqlDbType.Structured });
                    command.Parameters.Add(new SqlParameter { ParameterName = "@RowCount", Direction=ParameterDirection.Output ,Size = 10 });
                    command.CommandText = "uspInsertMcq";
                    command.CommandType = CommandType.StoredProcedure;
                    connection.Open();
                    var result = command.ExecuteNonQuery();

                    int.TryParse(command.Parameters["@RowCount"].Value.ToString(),out int output);
                    _addResult = output;
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _addResult > 0 ? true : false ;
        }

        public bool UpdateMcq(long? subjectId, long? chapterId, long? mediumId, long? sectionId, long? standardId, DataTable questionTable,
            DataTable answerTable)
        {
            long _addResult = 0;
            try
            {
                using (SqlConnection connection = new SqlConnection(WebConfig.ConnectionString))
                {
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection
                    };
                    command.Parameters.AddWithValue("@SubjectId", subjectId);
                    command.Parameters.AddWithValue("@ChapterId", chapterId);
                    command.Parameters.AddWithValue("@MediumId", mediumId);
                    command.Parameters.AddWithValue("@SectionId", sectionId);
                    command.Parameters.AddWithValue("@StandardId", standardId);
                    command.Parameters.Add(new SqlParameter { ParameterName = "@Questions", Value = questionTable, SqlDbType = SqlDbType.Structured });
                    command.Parameters.Add(new SqlParameter { ParameterName = "@Answers", Value = answerTable, SqlDbType = SqlDbType.Structured });
                    command.Parameters.Add(new SqlParameter { ParameterName = "@RowCount", Direction = ParameterDirection.Output, Size = 10 });
                    command.CommandText = "uspUpdateMcq";
                    command.CommandType = CommandType.StoredProcedure;
                    connection.Open();
                    var result = command.ExecuteNonQuery();

                    _addResult = result;
                    connection.Close();
                }
                }
            catch (Exception)
            {
                throw;
            }
            return _addResult > 0 ? true : false;
        }


        public IEnumerable<Question> GetQuestions(long? subjectId, long? chapterId, long? mediumId, long? sectionId, long? standardId)
        {
            IList<Question> _questions = new List<Question>();
            try
            {
                using (SqlConnection connection = new SqlConnection(WebConfig.ConnectionString))
                {
                    connection.Open();
                    DynamicParameters _questionParameters = new DynamicParameters();
                    _questionParameters.Add("@ChapterId", chapterId);
                    _questionParameters.Add("@SubjectId", subjectId);
                    _questionParameters.Add("@StandardId", standardId);
                    _questionParameters.Add("@MediumId", mediumId);
                    _questionParameters.Add("@SectionId", sectionId);
                    _questions = connection.Query<Question>("uspGetMcqQuestions", _questionParameters, commandType: CommandType.StoredProcedure).ToList();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _questions;
            
        }

        public IEnumerable<Answer> GetAnswers(long? questionNo, long? chapterId)
        {
            IList<Answer> _answers = new List<Answer>();
            try
            {
                using (SqlConnection connection = new SqlConnection(WebConfig.ConnectionString))
                {
                    connection.Open();
                    DynamicParameters _answerParameters = new DynamicParameters();
                    _answerParameters.Add("@ChapterId", chapterId);
                    _answerParameters.Add("@QuestionNo", questionNo);
                    _answers = connection.Query<Answer>("uspGetMcqAnswers", _answerParameters, commandType: CommandType.StoredProcedure).ToList();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _answers;
        }
    }
}