﻿using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace PrismAdminAPI.BAL
{
    public class SponsorerContentUtility
    {
        public static Sponsorer UploadSponsererImageContent(Sponsorer _sponsorerContent)
        {
            StringBuilder fullpath = new StringBuilder("~/UploadSponsorerImages");
            if (_sponsorerContent.LandingImageFile.InputStream != null)
            {
                fullpath = new StringBuilder("~/UploadSponsorerImages");
                fullpath.Append("/LandingImage")
                .Append($"/Img_{ Guid.NewGuid().ToString()}");

                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(fullpath.ToString())))
                {
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(fullpath.ToString()));
                }

                string _FileName = Path.GetFileNameWithoutExtension(_sponsorerContent.LandingImageFile.FileName) + Path.GetExtension(_sponsorerContent.LandingImageFile.FileName);
                string _path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(fullpath.ToString()), _FileName);
                _sponsorerContent.LandingImageFile.SaveAs(_path);

                _sponsorerContent.LandingImagePath = String.Format("{0}/{1}", fullpath.ToString(), _FileName);
            }

            if (_sponsorerContent.ThumbnailImageFile.InputStream != null)
            {
                fullpath = new StringBuilder("~/UploadSponsorerImages");
                fullpath.Append("/ThumbnailImages")
                .Append($"/Img_{ Guid.NewGuid().ToString()}");

                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(fullpath.ToString())))
                {
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(fullpath.ToString()));
                }
                string _FileName = Path.GetFileNameWithoutExtension(_sponsorerContent.ThumbnailImageFile.FileName) + Path.GetExtension(_sponsorerContent.ThumbnailImageFile.FileName);
                string _path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(fullpath.ToString()), _FileName);
                _sponsorerContent.ThumbnailImageFile.SaveAs(_path);

                _sponsorerContent.ThumbnailImagePath = String.Format("{0}/{1}", fullpath.ToString(), _FileName);
            }

            return _sponsorerContent;
        }
    }
}