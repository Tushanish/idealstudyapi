﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.BAL
{
    public class CouponGenerator
    {

        private static Random random = new Random();
        public static string RandomString()
        {
            int length = 2;
            const string StringChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string NumberChars = "0123456789";

            string firstPart = new string(Enumerable.Repeat(StringChars, length)
              .Select(s => s[random.Next(StringChars.Length)]).ToArray());
            string secondPart = new string(Enumerable.Repeat(NumberChars, length)
             .Select(s => s[random.Next(NumberChars.Length)]).ToArray());
            string thirdPart = new string(Enumerable.Repeat(StringChars, length)
             .Select(s => s[random.Next(StringChars.Length)]).ToArray());
            string fourthPart = new string(Enumerable.Repeat(NumberChars, length)
             .Select(s => s[random.Next(NumberChars.Length)]).ToArray());

            return String.Format("{0}{1}{2}{3}", firstPart, secondPart, thirdPart, fourthPart);
        }
    }
}