﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.BAL
{
    public class Helper
    {
        public static byte[] GetByteArray(HttpPostedFileBase _subjectThumbnail)
        {
            byte[] uploadedFile = null;
            if (_subjectThumbnail != null)
            {
                uploadedFile = new byte[_subjectThumbnail.InputStream.Length];
                _subjectThumbnail.InputStream.Read(uploadedFile, 0, uploadedFile.Length);
            }
            return uploadedFile;
        }
        public static IEnumerable<SelectListItem> ToSelectListItems<T, Q>(Dictionary<T, Q> keyValuePairs)
        {
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            foreach (var item in keyValuePairs)
            {
                selectListItems.Add(new SelectListItem { Text = item.Value.ToString(), Value = item.Key.ToString() });
            }
            return selectListItems;
        }
    }
}