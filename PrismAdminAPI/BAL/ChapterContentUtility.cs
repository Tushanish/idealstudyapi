﻿using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace PrismAdminAPI.BAL
{
    public class ChapterContentUtility
    {
        public static ChapterContent UploadChapterContents(ChapterContent _chapterContent)
        {
            StringBuilder fullpath = new StringBuilder("~/UploadedContentFiles");
            if (_chapterContent.ContentTypeString.Equals(((int)ChapterContentType.Image).ToString()))
            {
                List<string> _listPages = new List<string>();
                foreach (var file in _chapterContent.ChapterImageContents)
                {
                    fullpath = new StringBuilder("~/UploadedContentFiles");
                    fullpath.Append("/Image")
                    .Append(String.Format("/Standard_{0}", _chapterContent.StandardId))
                    .Append(String.Format("/Medium_{0}", _chapterContent.MediumId))
                    .Append(String.Format("/Subject_{0}", _chapterContent.SubjectId))
                    .Append(String.Format("/Chapter_{0}", _chapterContent.ChapterId))
                    .Append(String.Format("/Section_{0}", _chapterContent.SectionId));

                    if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(fullpath.ToString())))
                    {
                        Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(fullpath.ToString()));
                    }

                    string _FileName = Path.GetFileNameWithoutExtension(file.FileName) + Path.GetExtension(file.FileName);
                    string _path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(fullpath.ToString()), _FileName);
                    file.SaveAs(_path);
                    _listPages.Add(String.Format("{0}/{1}", fullpath.ToString(), _FileName));
                }
                _chapterContent.ChapterImageContentsPath = _listPages;
            }
            else if (_chapterContent.ContentTypeString.Equals(((int)ChapterContentType.Video).ToString()))
            {
                fullpath.Append("/Video")
                    .Append(String.Format("/Standard_{0}", _chapterContent.StandardId))
                    .Append(String.Format("/Medium_{0}", _chapterContent.MediumId))
                    .Append(String.Format("/Subject_{0}", _chapterContent.SubjectId))
                    .Append(String.Format("/Chapter_{0}", _chapterContent.ChapterId))
                    .Append(String.Format("/Section_{0}", _chapterContent.SectionId));

                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(fullpath.ToString())))
                {
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(fullpath.ToString()));
                }

                var file = _chapterContent.ChapterVideoContents;

                string _FileName = Path.GetFileNameWithoutExtension(file.FileName) + '_' + Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
                string _path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(fullpath.ToString()), _FileName);
                file.SaveAs(_path);

                _chapterContent.ChapterVideoContentsPath = String.Format("{0}/{1}", fullpath.ToString(), _FileName); ;
            }
           
            return _chapterContent;
        }

        public static Subject UploadSubjectThumbnail(Subject _subject)
        {
            if (_subject.SubjectThumbnail != null)
            {
                StringBuilder fullpath = new StringBuilder("~/UploadedContentFiles");
                fullpath.Append("/SubjectThumbnail")
                        .Append(String.Format("/Standard_{0}", _subject.StandardId));

                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(fullpath.ToString())))
                {
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(fullpath.ToString()));
                }

                var file = _subject.SubjectThumbnail;

                string _FileName = Path.GetFileNameWithoutExtension(file.FileName) + '_' + Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
                string _path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(fullpath.ToString()), _FileName);
                file.SaveAs(_path);

                _subject.SubjectThumbnailPath = String.Format("{0}/{1}", fullpath.ToString(), _FileName);
            }
            return _subject;
        }


    }
}