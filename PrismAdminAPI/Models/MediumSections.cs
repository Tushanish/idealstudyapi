﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Models
{
    public class MediumSections
    {
        public long MediumSectionId { get; set; }

        public long SectionId { get; set; }
        public string SectionName { get; set; }
        public string SectionDescription { get; set; }

        public long MediumId { get; set; }
        public string MediumName { get; set; }
        public string MediumDescription { get; set; }

        public bool IsActive { get; set; }

        public long CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}