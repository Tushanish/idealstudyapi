﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Models
{
    public class Answer
    {
        public long? AnswerId { get; set; }

        public long? AnswerNo { get; set; }

        public long? QuestionId { get; set; }

        public long? QuestionNo { get; set; }

        public long? ChapterId { get; set; }

        public bool? IsCorrect { get; set; }

        public string AnswerName { get; set; }

        public string ActionType { get; set; }
    }
}