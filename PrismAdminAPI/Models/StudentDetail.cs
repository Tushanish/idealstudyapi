﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Models
{
    public class StudentDetail
    {
        public int LoginId { get; set; }
        public long CouponId { get; set; }
        public string LoginDate { get; set; }
        public string LastLoginDate { get; set; }
        public string FirstName { get; set; }
        public string FatherName { get; set; }
        public string LastName { get; set; }
        public long MediumId { get; set; }
        public string MediumName { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public string DateOfBirth { get; set; }
        public string CouponCode { get; set; }
        public bool IsUsed { get; set; }
        public bool IsActive { get; set; }
        public string SponsorerName { get; set; }

        public List<SelectListItem> MediumList { get; set; } 
    }
}