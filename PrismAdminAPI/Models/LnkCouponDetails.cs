﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Models
{
    public class LnkCouponDetails
    {
        public int CouponDetailsId { get; set; }

        [Required(ErrorMessage = "Please insert From Coupon Id")]
        //[DataType("System.int",ErrorMessage = "Please enter number",ErrorMessageResourceType = typeof(int))]
        public long FromCouponId { get; set; }

        [Required(ErrorMessage = "Please insert To Coupon Id")]
        //[DataType("System.int", ErrorMessage = "Please enter number", ErrorMessageResourceType = typeof(int))]
        public long ToCouponId { get; set; }

        public long CouponId { get; set; }

        public int SponsorerId { get; set; }

        public List<SelectListItem> SponsorersList { get; set; } = new List<SelectListItem>();
    }
}