﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Models
{
    public class Question
    {
        public long? QuestionId { get; set; }

        public long QuestionNo { get; set; }

        public long? ChapterId { get; set; }

        public string QuestionName { get; set; }

        public List<Answer> Answers { get; set; }
    }
}