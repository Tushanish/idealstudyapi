﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Models
{
    public class ChapterContent
    {
        public long ChapterContentId { get; set; }

        [DisplayName("Standard")]
        public long StandardId { get; set; }

        [DisplayName("Medium")]
        public long MediumId { get; set; }

        [DisplayName("Subject")]
        public long SubjectId { get; set; }

        [DisplayName("Chapter")]
        public long ChapterId { get; set; }

        [DisplayName("Section")]
        public long SectionId { get; set; }

        [AllowHtml]
        [DisplayName("Content")]
        public string ChapterTextContents { get; set; }

        public List<string> ChapterImageContentsPath { get; set; }

        [DisplayName("Upload Image")]
        public HttpPostedFileBase[] ChapterImageContents { get; set; }

        public string ChapterVideoContentsPath { get; set; }

        [DisplayName("Upload Video")]
        public HttpPostedFileBase ChapterVideoContents { get; set; }

        public string ContentTypeString { get; set; }

        [Display(Name = "Content Type")]
        public ChapterContentType ContentType { get; set; }

        public bool IsActive { get; set; }

        [Display(Name = "Disable Section")]
        public bool? IsSectionDisabled { get; set; }

        public long CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

        public IEnumerable<SelectListItem> StandardList { get; set; }
    }

    public enum ChapterContentType
    {
        Text = 1,
        Image = 2,
        Video = 3   
    }

    public class ChapterContentViewModel
    {
        public long ChapterContentId { get; set; }
        public long SectionId { get; set; }
        public string SectionName { get; set; }
        public string ContentType { get; set; }
        public long ChapterId { get; set; }
        public string ChapterName { get; set; }
        public long SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string ChapterImageContents { get; set; }
        public string ChapterVideoContents { get; set; }
        [AllowHtml]
        public string ChapterTextContents { get; set; }
        public bool IsSectionDisabled { get; set; }
        public long StandardId { get; set; }
        public string StandardName { get; set; }
        public long MediumId { get; set; }
        public string MediumName { get; set; }
    }


}