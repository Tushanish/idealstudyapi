﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Models
{
    public class SubjectContent
    {
        public long SubjectContentId { get; set; }
        public long StandardId { get; set; }
        public long SubjectId { get; set; }
        public long MediumId { get; set; }
        public long SectionId { get; set; }
        [AllowHtml]
        [DisplayName("Content")]
        public string SubjectTextContents { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

        public IEnumerable<SelectListItem> StandardList { get; set; }
    }

    public class SubjectContentViewModel
    {
        public long SubjectContentId { get; set; }
        public long SectionId { get; set; }
        public string SectionName { get; set; }

        public long StandardId { get; set; }
        public string StandardName { get; set; }

        public long SubjectId { get; set; }
        public string SubjectName { get; set; }

        public long MediumId { get; set; }
        public string MediumName { get; set; }

        public string SubjectTextContents { get; set; }
        
    }

}