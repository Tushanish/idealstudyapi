﻿using System;

namespace PrismAdminAPI.Models
{
    public class StandardMedium
    {
        public long StandardMediumId { get; set; }

        public long StandardId { get; set; }
        public string StandardName { get; set; }
        public string StandardDescription { get; set; }

        public long MediumId { get; set; }
        public string MediumName { get; set; }
        public string MediumDescription { get; set; }

        public bool IsActive { get; set; }

        public long CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}