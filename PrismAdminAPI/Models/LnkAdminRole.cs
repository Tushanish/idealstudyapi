﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Models
{
    public class LnkAdminRole
    {
        public Admin Admin { get; set; }

        public IEnumerable<SelectListItem> Roles { get; set; }
    }
}