﻿using System;

namespace PrismAdminAPI.Models
{
    public class Login
    {
        public long LoginId { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string FatherName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string DateOfBirth { get; set; }
        public string EmailId { get; set; }
        public string City { get; set; }
        public string SchoolName { get; set; }
        public string ContantNumber { get; set; }
        public DateTime ExpiryDate { get; set; }
        public bool IsActive { get; set; }

        public long StandardId { get; set; }

        public long MediumId { get; set; }

        public long CouponId { get; set; }
    }
}