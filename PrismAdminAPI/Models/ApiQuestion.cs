﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Models
{
    public class ApiQuestion
    {
        public long QuestionId { get; set; }
        public string QuestionValue { get; set; }
    }
}