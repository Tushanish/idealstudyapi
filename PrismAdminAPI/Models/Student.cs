﻿using System;

namespace PrismAdminAPI.Models
{
    public class Student
    {
        public long LoginId { get; set; }
        public long StudentId { get; set; }
        
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public long StandardId { get; set; }
        public string StandardName { get; set; }

        public long MediumId { get; set; }
        public string MediumName { get; set; }

        public DateTime ExpiryDate { get; set; }

        public long CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

        public long SponsorerId { get; set; }
        public string SponsorerName { get; set; }
        public string LandingImagePath { get; set; }
        public string ThumbnailImagePath { get; set; }
    }
}