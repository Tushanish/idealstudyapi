﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Models
{
    public class ApiAnswer
    {
        public long? AnswerId { get; set; }
        public long? QuestionId { get; set; }
        public string AnswerValue { get; set; }
        public bool? IsCorrectAnswer { get; set; }
        public bool SelectedValue { get; set; }
    }
}