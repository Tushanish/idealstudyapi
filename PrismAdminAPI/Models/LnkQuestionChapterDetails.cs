﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Models
{
    public class LnkQuestionChapterDetails
    {
        public long? StandardId { get; set; }

        public long? SubjectId { get; set; }

        public long? ChapterId { get; set; }

        public long? MediumId { get; set; }

        public long? SectionId { get; set; }

        public List<Question> Questions { get; set; }
    }
}