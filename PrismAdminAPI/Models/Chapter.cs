﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Models
{
    public class Chapter
    {
        public long ChapterId { get; set; }
        [Required(ErrorMessage = "Please enter Chapter")]
        public string ChapterName { get; set; }

        [DisplayName("Standard")]
        [Required(ErrorMessage = "Please select standard")]
        public long StandardId { get; set; }
        public string StandardName { get; set; }

        [DisplayName("Mediums")]
        [Required(ErrorMessage = "Please select medium")]
        public long MediumId { get; set; }
        public string MediumName { get; set; }

        [DisplayName("Subject")]
        [Required(ErrorMessage = "Please select subject")]
        public long SubjectId { get; set; }
        public string SubjectName { get; set; }

        public string ChapterThumbnailPath { get; set; }

        public string Description { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public IEnumerable<SelectListItem> StandardList { get; set; }
    }

    
}