﻿
namespace PrismAdminAPI.Models
{
    public class SingleSignInUser
    {
        public long Id { get; set; }
        public long LoginId { get; set; }
        public long CouponId { get; set; }
        public bool IsActiveLogin { get; set; }
        public bool IsLoggedOut { get; set; }
        public string MobileUUID { get; set; }
    }
}