﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Models
{
    public class Coupon
    {
        public long? CouponId { get; set; }
        public string CouponCode { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }

    public class SponsorerCoupon : Coupon
    {
        public bool IsUsed { get; set; }
        public long SponsorerId { get; set; }
        public string SponsorerName { get; set; }
    }

    public class CouponDetails
    {
        public long? CouponId { get; set; }
        public string CouponCode { get; set; }
        public bool IsActive { get; set; }
        public string ModifiedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; } 
        public bool IsUsed { get; set; }
        public long SponsorerId { get; set; }
        public string SponsorerName { get; set; }
    }
}