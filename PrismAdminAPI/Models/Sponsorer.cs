﻿using PrismAdminAPI.BAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Models
{
    public class Sponsorer
    {
        public long SponsorerId { get; set; }

        [DisplayName("Sponsorer Name")]
        [Required(ErrorMessage = "Please add sponsorer name.")]
        public string SponsorerName { get; set; }

        [DisplayName("Thumbnail Image")]
        [Required(ErrorMessage = "Please upload thumbnail Image.")]
       
        public HttpPostedFileBase ThumbnailImageFile { get; set; }

        [DisplayName("Landing Image")]
        [Required(ErrorMessage = "Please upload landing Image.")]
       
        public HttpPostedFileBase LandingImageFile { get; set; }

        public byte[] ThumbnailImageByteData { get; set; }
        public byte[] LandingImageByteData { get; set; }


        public string LandingImagePath { get; set; }
        public string ThumbnailImagePath { get; set; }

        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; } = DateTime.Now; 
        public DateTime? ModifiedDate { get; set; } = null;

        public string ThumbnailImage
        {
            get
            {
                if (this.ThumbnailImageByteData != null)
                {
                    //this.ThumbnailImageFile.InputStream.Read(ThumbnailImageByteData, 0, ThumbnailImageByteData.Length);
                    return Convert.ToBase64String(ThumbnailImageByteData);
                }
                return string.Empty;
            }
        }

        public string LandingImage
        {
            get
            {
                if (this.LandingImageByteData != null)
                {
                    //this.LandingImageFile.InputStream.Read(LandingImageByteData, 0, LandingImageByteData.Length);
                    return Convert.ToBase64String(LandingImageByteData);
                }
                return string.Empty;
            }
        }

    }
}