﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Extensions
{
    public static class MVCDrawImage
    {
        public static MvcHtmlString DrawImage(this HtmlHelper helper, string imageUrl, string alt)
        {
            if (CanBrowserHandleWebPImages())
            {
                // Get the file type
                string fileType = Path.GetExtension(imageUrl);
                if (fileType != null)
                {
                    imageUrl = imageUrl.Replace(fileType, ".webp");
                }

                return new MvcHtmlString(String.Format("<img style=\"max - width:100px; max - height:100px\" alt=\"{0}\" " + "src=\"{1}\" />", alt, imageUrl));
            }

            return new MvcHtmlString(String.Format("<img style=\"max - width:100px; max - height:100px\" alt=\"{0}\" " + "src=\"{1}\" />", alt, imageUrl));
        }

        private static bool CanBrowserHandleWebPImages()
        {
            HttpRequest httpRequest = HttpContext.Current.Request;
            HttpBrowserCapabilities browser = httpRequest.Browser;

            if (browser.Type.Contains("Chrome") || browser.Type.Contains("Opera") || browser.Type.Contains("Android"))
            {
                return true;
            }

            return false;
        }
    }
}