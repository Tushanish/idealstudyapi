﻿-------------------------------------------------------------------------------------------------------------------------
-- START OF Admin Scripts
-------------------------------------------------------------------------------------------------------------------------
/****** Object:  StoredProcedure [dbo].[uspActiveDeactivateSponsorer]    Script Date: 3/8/2020 6:49:29 PM ******/
SET ANSI_NULLS ON
GO-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <07-03-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAllAdmins] 
	@AdminId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
	SELECT	MA.AdminId,MA.UserName,MA.Password,MR.RoleName,
			MA.IsActive FROM Mas_Admin MA
			INNER JOIN Mas_ROLE MR ON MA.ROLEID = MR.ROLEID
			WHERE (ISNULL(@AdminId,0)=0 OR MA.AdminId =@AdminId )
END
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <07-03-2020>
-- Description:	<Insert Sponsorer Details>
-- =============================================
CREATE PROCEDURE [dbo].[uspInsertAdmin]
	@Password  nvarchar(200),
	@RoleId nvarchar(200),
	@UserName nvarchar(200),
	@IsActive bit = null,
	@ReturnId bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       INSERT INTO Mas_Admin(UserName,Password,RoleId,IsActive) 
	   VALUES (@UserName,@Password,@RoleId,@IsActive)
	   
	   SET @ReturnId =SCOPE_IDENTITY()
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @ReturnId =0
		ROLLBACK TRANSACTION ;
    END CATCH
END
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <07-03-2020>
-- Description:	<Acitvate Deactivate Sponsorer>
-- =============================================
CREATE PROCEDURE [dbo].[uspActiveDeactivateAdmin]
	@AdminId bigint,
	@IsActive bit,
	@RowCount int OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       UPDATE Mas_Admin SET 
	   IsActive=@IsActive
	   WHERE AdminId=@AdminId
	   SET @RowCount= @@RowCount
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount=0
		ROLLBACK TRANSACTION ;
    END CATCH
END
GO

/****** Object:  StoredProcedure [dbo].[uspUpdateAdmin]    Script Date: 5/2/2020 2:58:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspUpdateAdmin]
	@AdminId bigint ,
	@UserName nvarchar(200),
	@IsActive bit ,
	@Password nvarchar(200),
	@RoleId int,
	@RowCount bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY

UPDATE Mas_Admin 
SET
UserName = @UserName,
Password = @Password,
RoleId = @RoleId
where AdminId = @AdminId

SET @RowCount = SCOPE_IDENTITY()
COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount =0
		ROLLBACK TRANSACTION ;
    END CATCH
END
GO


-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <07-03-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetRoles] 
@RoleId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
	SELECT	MR.RoleId , MR.RoleName , MR.IsActive from Mas_ROLE MR 
			WHERE (ISNULL(@RoleId,0)=0 OR MR.RoleId =@RoleId )
END






------------------------------------------------------------------------------------------------------------------




/****** Object:  UserDefinedTableType [dbo].[udt_Answer]    Script Date: 5/2/2020 3:05:13 PM ******/
CREATE TYPE [dbo].[udt_Answer] AS TABLE(
	[AnswerId] [bigint] NOT NULL,
	[AnswerNo] [bigint] NOT NULL,
	[QuestionNo] [bigint] NOT NULL,
	[IsCorrect] [bit] NOT NULL,
	[AnswerName] [nvarchar](400) NOT NULL
)
GO


-------------------------------------------------------------------------------------------------------------------------
-- START OF MCQ Scripts
-------------------------------------------------------------------------------------------------------------------------

/****** Object:  UserDefinedTableType [dbo].[udt_Question]    Script Date: 5/2/2020 3:05:22 PM ******/
CREATE TYPE [dbo].[udt_Question] AS TABLE(
	[QuestionId] [bigint] NOT NULL,
	[QuestionNo] [bigint] NOT NULL,
	[QuestionName] [nvarchar](1000) NOT NULL
)
GO

/****** Object:  Table [dbo].[Mas_Question]    Script Date: 5/2/2020 2:59:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Mas_Question](
	[QuestionId] [bigint] IDENTITY(1,1) NOT NULL,
	[QuestionNo] [bigint] NOT NULL,
	[QuestionName] [nvarchar](500) NOT NULL,
	[ChapterId] [bigint] NOT NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Mas_Question_CreatedDate]  DEFAULT (getdate()),
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL CONSTRAINT [DF_Mas_Question_ModifiedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_Mas_Question] PRIMARY KEY CLUSTERED 
(
	[QuestionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[Lnk_QuestionChapterDetails]    Script Date: 5/2/2020 3:00:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Lnk_QuestionChapterDetails](
	[LnkQuestionChapterId] [bigint] IDENTITY(1,1) NOT NULL,
	[StandardId] [bigint] NULL,
	[MediumId] [bigint] NULL,
	[SubjectId] [bigint] NULL,
	[ChapterId] [bigint] NULL,
	[SectionId] [bigint] NULL,
	[QuestionId] [bigint] NULL,
 CONSTRAINT [PK_Lnk_QuestionChapterDetails] PRIMARY KEY CLUSTERED 
(
	[LnkQuestionChapterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[Mas_Answer]    Script Date: 5/2/2020 3:00:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Mas_Answer](
	[AnswerId] [bigint] IDENTITY(1,1) NOT NULL,
	[AnswerNo] [bigint] NOT NULL,
	[QuestionNo] [bigint] NOT NULL,
	[IsCorrect] [bit] NOT NULL CONSTRAINT [DF_Mas_Answer_IsCorrect]  DEFAULT ((0)),
	[AnswerName] [nvarchar](200) NOT NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Mas_Answer_CreatedDate]  DEFAULT (getdate()),
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL CONSTRAINT [DF_Mas_Answer_ModifiedDate]  DEFAULT (getdate()),
	[ChapterId] [bigint] NULL,
 CONSTRAINT [PK_Mas_Answer] PRIMARY KEY CLUSTERED 
(
	[AnswerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



/****** Object:  StoredProcedure [dbo].[uspGetMcqAnswers]    Script Date: 5/2/2020 2:57:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <07-03-2020>
-- Description:	<Retrive all Mcqs Answer>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetMcqAnswers] 
	@ChapterId bigint = null,
	@QuestionNo bigint = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   IF(@ChapterId > 0 and @QuestionNo > 0 )
   BEGIN
	SELECT ans.AnswerId,ans.AnswerNo,ans.QuestionNo,ans.ChapterId,ans.IsCorrect,ans.AnswerName 
	FROM Mas_Answer ans		
	INNER JOIN Mas_Question mq ON ans.QuestionNo = @QuestionNo 
	WHERE (ISNULL(@ChapterId,0)=0 OR mq.ChapterId = @ChapterId)  AND (ISNULL(@QuestionNo,0)=0 OR mq.QuestionNo =@QuestionNo)
	END
	ELSE 
	SELECT ans.AnswerId,ans.AnswerNo,ans.QuestionNo,ans.ChapterId,ans.IsCorrect,ans.AnswerName 
	FROM Mas_Answer ans		
	INNER JOIN Mas_Question mq ON mq.QuestionNo = ans.QuestionNo
END

GO

/****** Object:  StoredProcedure [dbo].[uspGetMcqQuestions]    Script Date: 5/2/2020 2:58:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <07-03-2020>
-- Description:	<Retrive all Mcqs Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetMcqQuestions] --1,1,1,2,1
	@ChapterId bigint=null,
	@SubjectId bigint=null,
	@StandardId bigint=null,
	@MediumId bigint=null,
	@SectionId bigint=null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
	SELECT q.QuestionId,q.QuestionNo,q.QuestionName,q.ChapterId FROM Lnk_QuestionChapterDetails lnk
	INNER JOIN Mas_Question q ON lnk.QuestionId = q.QuestionId
	WHERE (ISNULL(@ChapterId,0)=0 OR lnk.ChapterId=@ChapterId) AND (ISNULL(@SubjectId,0)=0 OR lnk.SubjectId=@SubjectId)

END

GO

/****** Object:  StoredProcedure [dbo].[uspInsertMcq]    Script Date: 5/2/2020 2:58:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspInsertMcq]
	@SubjectId bigint ,
	@ChapterId bigint ,
	@MediumId bigint ,
	@SectionId bigint ,
	@StandardId bigint ,
	@Questions [dbo].[udt_Question] READONLY,
	@Answers  [dbo].[udt_Answer] READONLY,
	@RowCount bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY

	INSERT INTO Lnk_QuestionChapterDetails(ChapterId,MediumId,QuestionId,SectionId,StandardId,SubjectId)
	SELECT @ChapterId,@MediumId,q.QuestionId,@SectionId,@StandardId,@SubjectId  FROM @Questions q
		
	SET @RowCount = SCOPE_IDENTITY()

	INSERT INTO Mas_Question(QuestionName,QuestionNo,ChapterId)
	SELECT q.QuestionName , q.QuestionNo,@ChapterId FROM @Questions q
	SET @RowCount += SCOPE_IDENTITY()

	INSERT INTO Mas_Answer(AnswerName,AnswerNo,QuestionNo,ChapterId,IsCorrect)
	SELECT ans.AnswerName , ans.AnswerNo, ans.QuestionNo,@ChapterId, ans.IsCorrect FROM @Answers ans

	SET @RowCount += SCOPE_IDENTITY()
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount =0
		ROLLBACK TRANSACTION ;
    END CATCH
END
GO

/****** Object:  StoredProcedure [dbo].[uspUpdateMcq]    Script Date: 5/2/2020 2:58:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspUpdateMcq]
	@SubjectId bigint ,
	@ChapterId bigint ,
	@MediumId bigint ,
	@SectionId bigint ,
	@StandardId bigint ,
	@Questions [dbo].[udt_Question] READONLY,
	@Answers  [dbo].[udt_Answer] READONLY,
	@RowCount bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY

UPDATE Mas_Question 
SET
QuestionName = question.QuestionName
FROM @Questions question
INNER JOIN Mas_Question mq ON mq.QuestionNo = question.QuestionNo
INNER JOIN Lnk_QuestionChapterDetails lnk ON lnk.ChapterId = @ChapterId
where mq.QuestionNo = question.QuestionNo 

SET @RowCount = SCOPE_IDENTITY()

UPDATE Mas_Answer 
SET
AnswerName = answer.AnswerName
FROM @Answers answer
INNER JOIN Mas_Answer ans ON ans.AnswerNo = answer.AnswerNo 
INNER JOIN Mas_Question mq ON ans.QuestionNo = mq.QuestionNo
INNER JOIN Lnk_QuestionChapterDetails lnk ON lnk.ChapterId = @ChapterId
where mq.QuestionNo = ans.QuestionNo 

SET @RowCount += SCOPE_IDENTITY()
		
COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount =0
		ROLLBACK TRANSACTION ;
    END CATCH
END
GO
-------------------------------------------------------------------------------------------------------------------------
-- START OF Coupon Scripts
-------------------------------------------------------------------------------------------------------------------------

/****** Object:  Table [dbo].[Lnk_CouponDetails]    Script Date: 3/7/2020 9:55:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Lnk_CouponDetails](
	[CouponDetailsId] [bigint] IDENTITY(1,1) NOT NULL,
	[CouponId] [nvarchar](max) NOT NULL,
	[SponsorerId] [int] NOT NULL,
 CONSTRAINT [PK_Lnk_CouponDetails] PRIMARY KEY CLUSTERED 
(
	[CouponDetailsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Mas_Coupon]    Script Date: 3/7/2020 9:55:58 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Mas_Coupon](
	[CouponId] [bigint] IDENTITY(1,1) NOT NULL,
	[CouponCode] [nvarchar](max) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Mas_Coupon_IsActive]  DEFAULT ((0)),
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Mas_Coupon_CreatedDate]  DEFAULT (getdate()),
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedDate] [datetime] NULL CONSTRAINT [DF_Mas_Coupon_ModifiedDate]  DEFAULT (getdate()),
	[IsUsed] [bit] NOT NULL CONSTRAINT [DF_Mas_Coupon_IsUsed]  DEFAULT ((0)) ,
 CONSTRAINT [PK_Mas_Coupon] PRIMARY KEY CLUSTERED 
(
	[CouponId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  StoredProcedure [dbo].[uspActiveDeactivateCoupon]    Script Date: 3/8/2020 7:07:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <07-03-2020>
-- Description:	<Acitvate Deactivate Sponsorer>
-- =============================================
CREATE PROCEDURE [dbo].[uspActiveDeactivateCoupon]
	@CouponId bigint,
	@IsActive bit,
	@RowCount int OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       UPDATE Mas_Coupon SET 
	   IsActive=@IsActive,
	   ModifiedDate = GETDATE()
	   WHERE CouponId=@CouponId
	   SET @RowCount= @@RowCount
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount=0
		ROLLBACK TRANSACTION ;
    END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[uspGetAllLnkCouponDetails]    Script Date: 3/8/2020 7:08:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <07-03-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAllLnkCouponDetails] 
	@CouponId bigint=null,
	@SponsorerId bigint=null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
	SELECT LCD.CouponDetailsId,LCD.CouponId,LCD.SponsorerId FROM Lnk_CouponDetails LCD
	WHERE (ISNULL(@SponsorerId,0)=0 OR LCD.SponsorerId=@SponsorerId) AND (ISNULL(@CouponId,0)=0 OR LCD.CouponId=@CouponId)
END
GO

/****** Object:  StoredProcedure [dbo].[uspGetAllCoupons]    Script Date: 3/8/2020 7:08:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAllCoupons] 
	@CouponId bigint=null,
	@IsActive bit=null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
	SELECT MC.CouponId,MC.CouponCode,MC.IsActive,MC.CreatedBy,MC.CreatedDate,MC.ModifiedBy,MC.ModifiedDate FROM Mas_Coupon MC
	WHERE (ISNULL(@CouponId,0)=0 OR MC.CouponId=@CouponId) AND (ISNULL(@IsActive,0)=0 OR MC.IsActive=@IsActive)
END
GO

-------------------------------------------------------------------------------------------------------------------------
-- START OF Sponsorer Scripts
-------------------------------------------------------------------------------------------------------------------------

/****** Object:  Table [dbo].[Mas_Sponsorer]    Script Date: 3/7/2020 9:55:31 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Mas_Sponsorer](
	[SponsorerId] [bigint] IDENTITY(1,1) NOT NULL,
	[SponsorerName] [nvarchar](max) NOT NULL,
	[ThumbnailImage] [varbinary](max) NULL,
	[LandingImage] [varbinary](max) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Mas_Sponsorer_CreatedDate]  DEFAULT (getdate()),
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Mas_Sponsorer] PRIMARY KEY CLUSTERED 
(
	[SponsorerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

SET ANSI_PADDING OFF
GO


/****** Object:  StoredProcedure [dbo].[uspGetAllSponsorer]    Script Date: 3/8/2020 6:43:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <07-03-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAllSponsorer] 
	@SponsorerId bigint=null,
	@IsActive bit=null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
	SELECT MSP.SponsorerId,MSP.SponsorerName,MSP.ThumbnailImage AS ThumbnailImageByteData ,MSP.LandingImage AS LandingImageByteData,MSP.IsActive,MSP.CreatedDate,MSP.CreatedBy FROM Mas_Sponsorer MSP
	WHERE (ISNULL(@SponsorerId,0)=0 OR MSP.SponsorerId=@SponsorerId) AND (ISNULL(@IsActive,0)=0 OR MSP.IsActive=@IsActive)
END
GO

/****** Object:  StoredProcedure [dbo].[uspActiveDeactivateSponsorer]    Script Date: 3/8/2020 6:49:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <07-03-2020>
-- Description:	<Acitvate Deactivate Sponsorer>
-- =============================================
CREATE PROCEDURE [dbo].[uspActiveDeactivateSponsorer]
	@SponsorerId bigint,
	@IsActive bit,
	@RowCount int OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       UPDATE Mas_Sponsorer SET 
	   IsActive=@IsActive
	   WHERE SponsorerId=@SponsorerId
	   SET @RowCount= @@RowCount
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount=0
		ROLLBACK TRANSACTION ;
    END CATCH
END
GO

/****** Object:  StoredProcedure [dbo].[uspInsertSponsorer]    Script Date: 3/8/2020 6:50:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <07-03-2020>
-- Description:	<Insert Sponsorer Details>
-- =============================================
CREATE PROCEDURE [dbo].[uspInsertSponsorer]
	@SponsorerId bigint,
	@SponsorerName nvarchar(200),
	@ThumbnailImage varbinary(MAX)=null,
	@LandingImage varbinary(MAX)=null,
	@IsActive bit = null,
	@CreatedBy bigint = null,
	@ModifiedBy bigint = null,
	@ReturnId bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       INSERT INTO  Mas_Sponsorer(SponsorerName,ThumbnailImage,LandingImage,IsActive,CreatedBy,ModifiedBy) 
	   VALUES (@SponsorerName,@ThumbnailImage,@LandingImage,@IsActive,@CreatedBy,@ModifiedBy)
	   
	   SET @ReturnId =SCOPE_IDENTITY()
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @ReturnId =0
		ROLLBACK TRANSACTION ;
    END CATCH
END
GO

/****** Object:  StoredProcedure [dbo].[uspUpdateSponsorer]    Script Date: 3/8/2020 6:51:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <11-02-2020>
-- Description:	<Update  Sponsorer Details>
-- =============================================
CREATE PROCEDURE [dbo].[uspUpdateSponsorer]
	@SponsorerId bigint,
	@SponsorerName nvarchar(200),
	@ThumbnailImage varbinary(MAX)=null,
	@LandingImage varbinary(MAX)=null,
	@IsActive bit = null,
	@CreatedBy bigint = null,
	@ModifiedBy bigint = null,
	@ReturnId bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       UPDATE Mas_Sponsorer SET 
	   SponsorerName=@SponsorerName,
	   ThumbnailImage=@ThumbnailImage,
	   LandingImage=@LandingImage,
	   IsActive=@IsActive,
	   CreatedBy=@CreatedBy,
	   ModifiedBy=@ModifiedBy,
	   ModifiedDate = GETDATE()
	   WHERE SponsorerId=@SponsorerId

	   SET @ReturnId =@@ROWCOUNT
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @ReturnId =0
		ROLLBACK TRANSACTION ;
    END CATCH
END
GO

/****** Object:  StoredProcedure [dbo].[uspInsertLinkSponsorerCoupon]    Script Date: 3/8/2020 6:52:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspInsertLinkSponsorerCoupon]
	@FromCouponId bigint,
	@ToCouponId bigint,
	@SponsorerId bigint,
	@RowCount bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       INSERT INTO  Lnk_CouponDetails(CouponId,SponsorerId) 
	   SELECT mc.CouponId,@SponsorerId FROM Mas_Coupon mc WHERE mc.CouponId BETWEEN @FromCouponId and @ToCouponId
	   SET @RowCount =SCOPE_IDENTITY()
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount =0
		ROLLBACK TRANSACTION ;
    END CATCH
END
GO




