USE [master]
GO
/****** Object:  Database [PrismSolution]    Script Date: 18-04-2020 19:00:22 ******/
CREATE DATABASE [PrismSolution]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PrismSolution', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\PrismSolution.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'PrismSolution_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\PrismSolution_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [PrismSolution] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PrismSolution].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PrismSolution] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PrismSolution] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PrismSolution] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PrismSolution] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PrismSolution] SET ARITHABORT OFF 
GO
ALTER DATABASE [PrismSolution] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PrismSolution] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PrismSolution] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PrismSolution] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PrismSolution] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PrismSolution] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PrismSolution] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PrismSolution] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PrismSolution] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PrismSolution] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PrismSolution] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PrismSolution] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PrismSolution] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PrismSolution] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PrismSolution] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PrismSolution] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PrismSolution] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PrismSolution] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PrismSolution] SET  MULTI_USER 
GO
ALTER DATABASE [PrismSolution] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PrismSolution] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PrismSolution] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PrismSolution] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [PrismSolution] SET DELAYED_DURABILITY = DISABLED 
GO
USE [PrismSolution]
GO
/****** Object:  Schema [Admin]    Script Date: 18-04-2020 19:00:22 ******/
CREATE SCHEMA [Admin]
GO
/****** Object:  Schema [Student]    Script Date: 18-04-2020 19:00:22 ******/
CREATE SCHEMA [Student]
GO
/****** Object:  Table [dbo].[Lnk_CouponDetails]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lnk_CouponDetails](
	[CouponDetailsId] [bigint] IDENTITY(1,1) NOT NULL,
	[CouponId] [nvarchar](max) NOT NULL,
	[SponsorerId] [int] NOT NULL,
 CONSTRAINT [PK_Lnk_CouponDetails] PRIMARY KEY CLUSTERED 
(
	[CouponDetailsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Lnk_CouponUser]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lnk_CouponUser](
	[CouponUserId] [bigint] IDENTITY(1,1) NOT NULL,
	[CouponId] [bigint] NULL,
	[LoginId] [bigint] NULL,
 CONSTRAINT [PK_Lnk_CouponUser] PRIMARY KEY CLUSTERED 
(
	[CouponUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Lnk_MediumSections]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lnk_MediumSections](
	[MediumSectionId] [bigint] IDENTITY(1,1) NOT NULL,
	[SectionId] [bigint] NULL,
	[MediumId] [bigint] NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Lnk_MediumSections_IsActive]  DEFAULT ((0)),
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Lnk_MediumSections] PRIMARY KEY CLUSTERED 
(
	[MediumSectionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Lnk_StandardMedium]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lnk_StandardMedium](
	[StandardMediumId] [bigint] IDENTITY(1,1) NOT NULL,
	[StandardId] [bigint] NOT NULL,
	[MediumId] [bigint] NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Lnk_StandardMedium_IsActive]  DEFAULT ((1)),
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Lnk_StandardMedium] PRIMARY KEY CLUSTERED 
(
	[StandardMediumId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Lnk_SubjectDetails]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lnk_SubjectDetails](
	[SubjectDetailsId] [bigint] IDENTITY(1,1) NOT NULL,
	[StandardId] [bigint] NOT NULL,
	[MediumId] [bigint] NOT NULL,
	[SubjectId] [bigint] NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Lnk_SubjectDetails_IsActive]  DEFAULT ((1)),
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Lnk_SubjectDetails] PRIMARY KEY CLUSTERED 
(
	[SubjectDetailsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Mas_Admin]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Mas_Admin](
	[AdminId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](300) NOT NULL,
	[Password] [varchar](300) NOT NULL,
	[RoleId] [bigint] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Mas_Admin] PRIMARY KEY CLUSTERED 
(
	[AdminId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Mas_Chapter]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mas_Chapter](
	[ChapterId] [bigint] IDENTITY(1,1) NOT NULL,
	[StandardId] [bigint] NOT NULL,
	[SubjectId] [bigint] NOT NULL,
	[MediumId] [bigint] NOT NULL,
	[ChapterName] [nvarchar](150) NOT NULL,
	[ChapterThumbnail] [nvarchar](500) NULL,
	[Description] [nvarchar](100) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Mas_Chapter_IsActive]  DEFAULT ((0)),
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Mas_Chapter] PRIMARY KEY CLUSTERED 
(
	[ChapterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Mas_ChapterContent]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mas_ChapterContent](
	[ChapterContentId] [bigint] IDENTITY(1,1) NOT NULL,
	[ChapterId] [bigint] NOT NULL,
	[SectionId] [bigint] NOT NULL,
	[ChapterTextContents] [nvarchar](max) NULL,
	[ChapterImageContents] [nvarchar](500) NULL,
	[ChapterVideoContents] [nvarchar](500) NULL,
	[ContentType] [int] NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Mas_ChapterContent_IsActive]  DEFAULT ((0)),
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Mas_ChapterContent] PRIMARY KEY CLUSTERED 
(
	[ChapterContentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Mas_Coupon]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mas_Coupon](
	[CouponId] [bigint] IDENTITY(1,1) NOT NULL,
	[CouponCode] [nvarchar](max) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Mas_Coupon_IsActive]  DEFAULT ((0)),
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Mas_Coupon_CreatedDate]  DEFAULT (getdate()),
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedDate] [datetime] NULL CONSTRAINT [DF_Mas_Coupon_ModifiedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_Mas_Coupon] PRIMARY KEY CLUSTERED 
(
	[CouponId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Mas_ErrorLogs]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Mas_ErrorLogs](
	[ErrorLogId] [bigint] IDENTITY(1,1) NOT NULL,
	[ErrorProcedure] [varchar](300) NOT NULL,
	[ErrorLine] [bigint] NULL,
	[ErrorMessage] [nvarchar](max) NULL,
 CONSTRAINT [PK_Mas_ErrorLogs] PRIMARY KEY CLUSTERED 
(
	[ErrorLogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Mas_Login]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Mas_Login](
	[LoginId] [bigint] IDENTITY(1,1) NOT NULL,
	[PhoneNumber] [varchar](50) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](100) NOT NULL,
	[DateOfBirth] [datetime] NULL,
	[EmailId] [nvarchar](150) NULL,
	[City] [nvarchar](200) NULL,
	[SchoolName] [nvarchar](200) NULL,
	[ContantNumber] [nvarchar](50) NULL,
	[ExpiryDate] [datetime] NOT NULL CONSTRAINT [DF_Mas_Login_ExpiryDate]  DEFAULT (getdate()),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Mas_Login_IsActive]  DEFAULT ((0)),
 CONSTRAINT [PK_Mas_Login] PRIMARY KEY CLUSTERED 
(
	[LoginId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Mas_Medium]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mas_Medium](
	[MediumId] [bigint] IDENTITY(1,1) NOT NULL,
	[MediumName] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](100) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Mas_Medium_IsActive]  DEFAULT ((0)),
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Mas_Medium] PRIMARY KEY CLUSTERED 
(
	[MediumId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Mas_Menu]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mas_Menu](
	[MenuId] [bigint] IDENTITY(1,1) NOT NULL,
	[MenuName] [nvarchar](300) NULL,
	[MenuActionLink] [nvarchar](300) NULL,
	[MenuStyleClass] [nvarchar](200) NULL,
	[IsActive] [bit] NULL,
	[ParentMenuId] [bigint] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Mas_Role]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mas_Role](
	[RoleId] [bigint] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](200) NULL,
	[IsActive] [nchar](10) NULL,
 CONSTRAINT [PK_Mas_Role] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Mas_RoleMenu]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mas_RoleMenu](
	[RoleMenuId] [bigint] IDENTITY(1,1) NOT NULL,
	[MenuId] [bigint] NULL,
	[RoleId] [bigint] NULL,
 CONSTRAINT [PK_Mas_RoleMenu] PRIMARY KEY CLUSTERED 
(
	[RoleMenuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Mas_Section]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mas_Section](
	[SectionId] [bigint] IDENTITY(1,1) NOT NULL,
	[SectionName] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](100) NULL,
	[IsStandardSection] [bit] NULL CONSTRAINT [DF_Mas_Section_IsStandardSection]  DEFAULT ((0)),
	[IsChapterSection] [bit] NULL CONSTRAINT [DF_Mas_Section_IsChapterSection]  DEFAULT ((0)),
	[IsActive] [bit] NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Mas_Section] PRIMARY KEY CLUSTERED 
(
	[SectionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Mas_Sponsorer]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Mas_Sponsorer](
	[SponsorerId] [bigint] IDENTITY(1,1) NOT NULL,
	[SponsorerName] [nvarchar](max) NOT NULL,
	[ThumbnailImage] [varbinary](max) NULL,
	[LandingImage] [varbinary](max) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Mas_Sponsorer] PRIMARY KEY CLUSTERED 
(
	[SponsorerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Mas_Standard]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mas_Standard](
	[StandardId] [bigint] IDENTITY(1,1) NOT NULL,
	[StandardName] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](100) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Mas_Standard_IsActive]  DEFAULT ((0)),
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Mas_Standard] PRIMARY KEY CLUSTERED 
(
	[StandardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Mas_Student]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mas_Student](
	[StudentId] [bigint] IDENTITY(1,1) NOT NULL,
	[LoginId] [bigint] NULL,
	[StandardId] [bigint] NULL,
	[MediumId] [bigint] NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Mas_Student] PRIMARY KEY CLUSTERED 
(
	[StudentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Mas_Subject]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mas_Subject](
	[SubjectId] [bigint] IDENTITY(1,1) NOT NULL,
	[SubjectName] [nvarchar](250) NOT NULL,
	[SubjectThumbnail] [nvarchar](500) NULL,
	[Description] [nvarchar](100) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Mas_Subject_IsActive]  DEFAULT ((0)),
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Mas_Subject] PRIMARY KEY CLUSTERED 
(
	[SubjectId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Mas_SubjectContent]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mas_SubjectContent](
	[SubjectContentId] [bigint] IDENTITY(1,1) NOT NULL,
	[StandardId] [bigint] NOT NULL,
	[SubjectId] [bigint] NULL,
	[MediumId] [bigint] NOT NULL,
	[SectionId] [bigint] NOT NULL,
	[SubjectTextContents] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Mas_StandardContent_IsActive]  DEFAULT ((0)),
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Mas_StandardContent] PRIMARY KEY CLUSTERED 
(
	[SubjectContentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Lnk_CouponUser] ON 

GO
INSERT [dbo].[Lnk_CouponUser] ([CouponUserId], [CouponId], [LoginId]) VALUES (1, 1, 2)
GO
SET IDENTITY_INSERT [dbo].[Lnk_CouponUser] OFF
GO
SET IDENTITY_INSERT [dbo].[Lnk_MediumSections] ON 

GO
INSERT [dbo].[Lnk_MediumSections] ([MediumSectionId], [SectionId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 1, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_MediumSections] ([MediumSectionId], [SectionId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 1, 2, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_MediumSections] ([MediumSectionId], [SectionId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 2, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_MediumSections] ([MediumSectionId], [SectionId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, 2, 2, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_MediumSections] ([MediumSectionId], [SectionId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, 3, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_MediumSections] ([MediumSectionId], [SectionId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, 3, 2, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_MediumSections] ([MediumSectionId], [SectionId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, 4, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_MediumSections] ([MediumSectionId], [SectionId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, 4, 2, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_MediumSections] ([MediumSectionId], [SectionId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (9, 5, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_MediumSections] ([MediumSectionId], [SectionId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10, 5, 2, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_MediumSections] ([MediumSectionId], [SectionId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (11, 6, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_MediumSections] ([MediumSectionId], [SectionId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (12, 6, 2, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_MediumSections] ([MediumSectionId], [SectionId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (13, 7, 3, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_MediumSections] ([MediumSectionId], [SectionId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10013, 10007, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_MediumSections] ([MediumSectionId], [SectionId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10014, 10007, 2, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_MediumSections] ([MediumSectionId], [SectionId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10015, 10008, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_MediumSections] ([MediumSectionId], [SectionId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10016, 10008, 2, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_MediumSections] ([MediumSectionId], [SectionId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10017, 10009, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_MediumSections] ([MediumSectionId], [SectionId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10018, 10009, 2, 1, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Lnk_MediumSections] OFF
GO
SET IDENTITY_INSERT [dbo].[Lnk_StandardMedium] ON 

GO
INSERT [dbo].[Lnk_StandardMedium] ([StandardMediumId], [StandardId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 1, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_StandardMedium] ([StandardMediumId], [StandardId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 1, 2, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_StandardMedium] ([StandardMediumId], [StandardId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 2, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_StandardMedium] ([StandardMediumId], [StandardId], [MediumId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, 1, 3, 0, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Lnk_StandardMedium] OFF
GO
SET IDENTITY_INSERT [dbo].[Lnk_SubjectDetails] ON 

GO
INSERT [dbo].[Lnk_SubjectDetails] ([SubjectDetailsId], [StandardId], [MediumId], [SubjectId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 1, 1, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_SubjectDetails] ([SubjectDetailsId], [StandardId], [MediumId], [SubjectId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 1, 2, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_SubjectDetails] ([SubjectDetailsId], [StandardId], [MediumId], [SubjectId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 1, 3, 2, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_SubjectDetails] ([SubjectDetailsId], [StandardId], [MediumId], [SubjectId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, 1, 1, 2, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_SubjectDetails] ([SubjectDetailsId], [StandardId], [MediumId], [SubjectId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, 1, 2, 2, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_SubjectDetails] ([SubjectDetailsId], [StandardId], [MediumId], [SubjectId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, 1, 3, 1, 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_SubjectDetails] ([SubjectDetailsId], [StandardId], [MediumId], [SubjectId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, 1, 1, 3, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_SubjectDetails] ([SubjectDetailsId], [StandardId], [MediumId], [SubjectId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, 1, 2, 3, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_SubjectDetails] ([SubjectDetailsId], [StandardId], [MediumId], [SubjectId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (9, 1, 3, 3, 0, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_SubjectDetails] ([SubjectDetailsId], [StandardId], [MediumId], [SubjectId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10, 1, 1, 4, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_SubjectDetails] ([SubjectDetailsId], [StandardId], [MediumId], [SubjectId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (11, 1, 2, 4, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_SubjectDetails] ([SubjectDetailsId], [StandardId], [MediumId], [SubjectId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (12, 1, 1, 5, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_SubjectDetails] ([SubjectDetailsId], [StandardId], [MediumId], [SubjectId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (13, 1, 2, 5, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_SubjectDetails] ([SubjectDetailsId], [StandardId], [MediumId], [SubjectId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (14, 1, 1, 6, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Lnk_SubjectDetails] ([SubjectDetailsId], [StandardId], [MediumId], [SubjectId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (15, 1, 2, 6, 1, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Lnk_SubjectDetails] OFF
GO
SET IDENTITY_INSERT [dbo].[Mas_Admin] ON 

GO
INSERT [dbo].[Mas_Admin] ([AdminId], [UserName], [Password], [RoleId], [IsActive]) VALUES (1, N'tushar.h', N'JayUsha@81185', 1, 1)
GO
INSERT [dbo].[Mas_Admin] ([AdminId], [UserName], [Password], [RoleId], [IsActive]) VALUES (2, N'amol.k', N'ideal@123', 1, 1)
GO
INSERT [dbo].[Mas_Admin] ([AdminId], [UserName], [Password], [RoleId], [IsActive]) VALUES (3, N'atul.k', N'ideal@123', 1, 1)
GO
INSERT [dbo].[Mas_Admin] ([AdminId], [UserName], [Password], [RoleId], [IsActive]) VALUES (4, N'vandana.k', N'ideal@123', 1, 1)
GO
SET IDENTITY_INSERT [dbo].[Mas_Admin] OFF
GO
SET IDENTITY_INSERT [dbo].[Mas_Chapter] ON 

GO
INSERT [dbo].[Mas_Chapter] ([ChapterId], [StandardId], [SubjectId], [MediumId], [ChapterName], [ChapterThumbnail], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 1, 1, 1, N'जय जय हे भारत देशा ', N'傉䝎਍ਚ ഀ䡉剄 送 送؈ 耀㚿Ì 琙塅却景睴牡e摁扯⁥浉条剥慥祤쥱㱥̀ꎔ䑉呁�뷬耇坤♵�⨗馞ᦞ鹍⣑ᢌ⏑ঔĚ砦ᆍ睩q믉᭯ﻰ莮ﾐ�ᙫ᭣̛鈎㛖ǁㆄ␘䣌ठ␑䚍ᨳァꜹ��箜ꡟ溪ᡉ䄯䓵㷑啝뼯鷳鷰⣯靴됃㻬࿼郇㗧篡럴踸쟩聵벆槏醭䤤竛彼饁⛏蘺燖䷱選콵㫳㫍찦皺�켼劃床쾞黼➷겶ﭾ㼮謈黪緲စﺇ뭪쫲큃飾㡆䱨飽엯壆㳫ᡘร럲湈䕶几̄送샼翼�顫㓏矍ﬓɹ肄驟ᄾ飍Ꮯ℆᝚ྃ蚁뉻帬脅ｧ楾兄滳✂䆝䰐㾚퓮鯚뫘柮틮逅틮빝﶑ᬟ쩒攼Ңຽ첈飏♞ꢲ뮘撶貀螹迀䘊ග俍礟ᅐ턀瀩쿈㶤⃂儀䔔솀ᭃￌ矓領活聛᥄預淦立連줤鮢激淚̫㸦᛿둅量᡼버㣾ᏻ蠆㙶㣎ᱪꢦ孕楾箢璇⺗璀院龇돾웵蹘绹劅흊鞙�爟�笙ꪕ翏ﯬ挍⍴訆矼滚ᕶꠐไ桔贝蠄웪埐ꐙ䨔菰䤀蘈ꏞ쓈@䶚᭞襁奙౒綅ᓢƗ⃃館恗蓀헖쁤ᓲ╇ⱼ䀆R�#槙綾�Ǆﷺஊ땿ኛ⻫]⻩�鏋㾇쁎㶰鈉挱꼬䘳챳픘㈡ږ퉀᧏준爙᪝襓谦껷ഥ理濮퀵撯즈컘쿏蔄篭⥾仝බꁣ念惒�왬☄䧢崤䔕᫷㰿ま釀ṅ⑀⧈娾�ឣ붆嗦緦햩౪Р䄞ʀ킀冿䋴ि≰舋偍찚넽墉ﰴ꾻먿䪑䠂亮巾쏿⇵盔莭싫枰磌揿�嵹៏ઢය뽽㦜嫫膳�ﴅ䁎蘐㖡컮‌˘튵�ꠊ耐㆐몦紆펆腇褀䋲맳ᩲ퀁闟萁獦', N'जय जय हे भारत देशा ', 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Chapter] ([ChapterId], [StandardId], [SubjectId], [MediumId], [ChapterName], [ChapterThumbnail], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 1, 1, 1, N'बोलतो मराठी ', N'傉䝎਍ਚ ഀ䡉剄 送 送؈ 耀㚿Ì 琙塅却景睴牡e摁扯⁥浉条剥慥祤쥱㱥̀ꎔ䑉呁�뷬耇坤♵�⨗馞ᦞ鹍⣑ᢌ⏑ঔĚ砦ᆍ睩q믉᭯ﻰ莮ﾐ�ᙫ᭣̛鈎㛖ǁㆄ␘䣌ठ␑䚍ᨳァꜹ��箜ꡟ溪ᡉ䄯䓵㷑啝뼯鷳鷰⣯靴됃㻬࿼郇㗧篡럴踸쟩聵벆槏醭䤤竛彼饁⛏蘺燖䷱選콵㫳㫍찦皺�켼劃床쾞黼➷겶ﭾ㼮謈黪緲စﺇ뭪쫲큃飾㡆䱨飽엯壆㳫ᡘร럲湈䕶几̄送샼翼�顫㓏矍ﬓɹ肄驟ᄾ飍Ꮯ℆᝚ྃ蚁뉻帬脅ｧ楾兄滳✂䆝䰐㾚퓮鯚뫘柮틮逅틮빝﶑ᬟ쩒攼Ңຽ첈飏♞ꢲ뮘撶貀螹迀䘊ග俍礟ᅐ턀瀩쿈㶤⃂儀䔔솀ᭃￌ矓領活聛᥄預淦立連줤鮢激淚̫㸦᛿둅量᡼버㣾ᏻ蠆㙶㣎ᱪꢦ孕楾箢璇⺗璀院龇돾웵蹘绹劅흊鞙�爟�笙ꪕ翏ﯬ挍⍴訆矼滚ᕶꠐไ桔贝蠄웪埐ꐙ䨔菰䤀蘈ꏞ쓈@䶚᭞襁奙౒綅ᓢƗ⃃館恗蓀헖쁤ᓲ╇ⱼ䀆R�#槙綾�Ǆﷺஊ땿ኛ⻫]⻩�鏋㾇쁎㶰鈉挱꼬䘳챳픘㈡ږ퉀᧏준爙᪝襓谦껷ഥ理濮퀵撯즈컘쿏蔄篭⥾仝බꁣ念惒�왬☄䧢崤䔕᫷㰿ま釀ṅ⑀⧈娾�ឣ붆嗦緦햩౪Р䄞ʀ킀冿䋴ि≰舋偍찚넽墉ﰴ꾻먿䪑䠂亮巾쏿⇵盔莭싫枰磌揿�嵹៏ઢය뽽㦜嫫膳�ﴅ䁎蘐㖡컮‌˘튵�ꠊ耐㆐몦紆펆腇褀䋲맳ᩲ퀁闟萁獦', N'बोलतो मराठी ', 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Chapter] ([ChapterId], [StandardId], [SubjectId], [MediumId], [ChapterName], [ChapterThumbnail], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 1, 1, 1, N'आजी : कुटुंबाच आगळ', N'傉䝎਍ਚ ഀ䡉剄 送 送؈ 耀㚿Ì 琙塅却景睴牡e摁扯⁥浉条剥慥祤쥱㱥̀ꎔ䑉呁�뷬耇坤♵�⨗馞ᦞ鹍⣑ᢌ⏑ঔĚ砦ᆍ睩q믉᭯ﻰ莮ﾐ�ᙫ᭣̛鈎㛖ǁㆄ␘䣌ठ␑䚍ᨳァꜹ��箜ꡟ溪ᡉ䄯䓵㷑啝뼯鷳鷰⣯靴됃㻬࿼郇㗧篡럴踸쟩聵벆槏醭䤤竛彼饁⛏蘺燖䷱選콵㫳㫍찦皺�켼劃床쾞黼➷겶ﭾ㼮謈黪緲စﺇ뭪쫲큃飾㡆䱨飽엯壆㳫ᡘร럲湈䕶几̄送샼翼�顫㓏矍ﬓɹ肄驟ᄾ飍Ꮯ℆᝚ྃ蚁뉻帬脅ｧ楾兄滳✂䆝䰐㾚퓮鯚뫘柮틮逅틮빝﶑ᬟ쩒攼Ңຽ첈飏♞ꢲ뮘撶貀螹迀䘊ග俍礟ᅐ턀瀩쿈㶤⃂儀䔔솀ᭃￌ矓領活聛᥄預淦立連줤鮢激淚̫㸦᛿둅量᡼버㣾ᏻ蠆㙶㣎ᱪꢦ孕楾箢璇⺗璀院龇돾웵蹘绹劅흊鞙�爟�笙ꪕ翏ﯬ挍⍴訆矼滚ᕶꠐไ桔贝蠄웪埐ꐙ䨔菰䤀蘈ꏞ쓈@䶚᭞襁奙౒綅ᓢƗ⃃館恗蓀헖쁤ᓲ╇ⱼ䀆R�#槙綾�Ǆﷺஊ땿ኛ⻫]⻩�鏋㾇쁎㶰鈉挱꼬䘳챳픘㈡ږ퉀᧏준爙᪝襓谦껷ഥ理濮퀵撯즈컘쿏蔄篭⥾仝බꁣ念惒�왬☄䧢崤䔕᫷㰿ま釀ṅ⑀⧈娾�ឣ붆嗦緦햩౪Р䄞ʀ킀冿䋴ि≰舋偍찚넽墉ﰴ꾻먿䪑䠂亮巾쏿⇵盔莭싫枰磌揿�嵹៏ઢය뽽㦜嫫膳�ﴅ䁎蘐㖡컮‌˘튵�ꠊ耐㆐몦紆펆腇褀䋲맳ᩲ퀁闟萁獦', N'आजी : कुटुंबाच आगळ', 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Chapter] ([ChapterId], [StandardId], [SubjectId], [MediumId], [ChapterName], [ChapterThumbnail], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, 1, 4, 1, N'क्षेत्रभेट', N'傉䝎਍ਚ ഀ䡉剄 送 送؈ 耀㚿Ì 琙塅却景睴牡e摁扯⁥浉条剥慥祤쥱㱥Ѐᰁ䑉呁�뷬鐉흝ᵵ㕏ŗ슅ʌ豀섄⤢ᑊ襉⢠铙䕭䃔蹋鄇꽶✕񋡒�华쯘嬝噶ᥚ햜㪔❉�攒雙入⠄㎊艀映鹢啑ᕀﱪ믓˽뛝軛ꚣ밿﻿杻珟�ࣻ潨됭悔鼣ㄛ著횴萠⇜ꢿ腻폿ﳽ甃Ꮛ㟩ൾ㝍빢嫑驍�罋䈯ꞈ봄ﴚ讻¾㹌ꗺ뙿ﶷ땉랷⛂蟚붠⽽轭꯽睧삘膌ß꿥ꟸ᠝嶷윀ꗯୁ┡⥚瑐鴇苅썿∷ꎺ翦ᬙ＠굚‷አあㅘ҄狰뷏뻋�髵ﾹ䓞즊﵏鷊�璟死䠃死罯ﹿጾ䝌薊ꑤꚘቫ⌉黎锹辢䅀⽜၅愄㢣叝ഩƙᜑ㇤毊Ɇ콁簩矀⇀D栋逅柨㹼脕綇繦灎쉀↠黂༊癔ꐟ㬁耩䧿쿳찧乖笀勵宓�䡧死䠃死景쳻籿᡹ࢳ▁ഥ飷㯐艢Ȱ᠈脀驒ƀ猏脳䓕⥖䳢䝁馁疃谫�ЫఐЛ픑␂耱鯘㿌횏령遚梗ﳻϚ䭊⍾虂鏣舞䓑홲遂䗯겻荇锫攽㡾繥ﯞ翏孼Ⓕ�Ú�〠レљ鶖퇮赊阀䒣搔꾣쓅蠂킚狽豽㇨ċ㊄ఏ೮ᤠⅧ䀎⨜꿄朙ㄢ䂀㖔빈뢻ϬᲳ惍鸓ؾ鬏綠寯ᄲ᠘鐡⃊䢀䀀ݻ䫌䐐뮯茶汏杫ත篭ﭷ彗㰺ꉬ軜嫙黣媹䑽㴫䢒ࠋ樢獃즥鲗㿨ꄩ湨⒅旱ᢝ搙쎡�ᵥ鯦뾌ၚ遑⩥瑮⣻瀞䁉鉋䯡鉢↲㗳ẑ⡀⯀姍霎犳䮙⠀艗ꔋ隸䜬康궙ᇀ㱺台䯉ﴋƔ嶤ﲏ눳떽ꐁ붽띩穟Ὤᡥᒉ⸟爤ㅇ堑芞竹�됚俲ۀ᭑甚酘꽣Ûⶆ�縕緂ﷆἁꏩ輺쥮琾嬨埑䛎ᥲై儠Ꝿ', N'१. क्षेत्रभेट', 1, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Mas_Chapter] OFF
GO
SET IDENTITY_INSERT [dbo].[Mas_ChapterContent] ON 

GO
INSERT [dbo].[Mas_ChapterContent] ([ChapterContentId], [ChapterId], [SectionId], [ChapterTextContents], [ChapterImageContents], [ChapterVideoContents], [ContentType], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 1, 3, NULL, N'~/UploadedContentFiles/Image/Standard_1/Medium_1/Subject_1/Chapter_1/Section_3/1.png', NULL, 2, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_ChapterContent] ([ChapterContentId], [ChapterId], [SectionId], [ChapterTextContents], [ChapterImageContents], [ChapterVideoContents], [ContentType], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 3, 4, N'<p><br></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: center;"><span style="font-size: 18px;"><span style="font-weight: bolder;">&nbsp;तू बुद्धि दे</span></span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: center;"><span style="font-size: 18px;"><span style="font-weight: bolder;">गुरु ठाकूर</span></span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;"><span style="font-weight: bolder;">प्रस्तावना :</span></span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">प्रसिद्ध कवी, गीतकार, स्तंभलेखकक, नाट्यकार, कथा-पटकथा, आणि संवाद लेखक असलेले गुरु ठाकूर यांची ही प्रार्थना असून डॉ. प्रकाश बाबा आमटे या चित्रपटात या प्रार्थनेचा समावेश आहे.</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">या प्रार्थनेत गुरु ठाकूर यांनी सन्मार्ग सं</span><span style="font-size: 18px;">न्म</span><span style="font-size: 18px;">ती आणि सत्संगती याचे महत्त्व स्पष्ट केले आहे. कायम सत्याची कास धरता यावी संवेदनशीलता जपण्यासाठी ताकद मिळावी अनाथांचे नाथ होण्यास बळ मिळावे व शाश्वत सौंदर्याचा ध्यास लागावा या भावना प्रार्थनेतून व्यक्त झाल्या आहेत.</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;"><span style="font-weight: bolder;">&nbsp;* YouTube -</span>&nbsp;वर तुम्ही ही प्रार्थना ऐकू शकता '' तू बुद्धि दे '' असे टाईप करून शोधल्यावर आपल्याला या प्रार्थनेतील कन्या आनंद घेता येईल.</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">&nbsp;* सदर कविता ही केवळ कन्यानंदासाठी असून त्यावर आधारित स्वाध्याय (कृती )नाहीत.</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">* कवितेवर आधारित कृती नसल्यातरी सदर कवितेचा उपयोग आपण उपयोजित लेखनात करू शकतो त्यामुळे कविता वाचून त्याचा अर्थ समजून घेणे आवश्यक आहे.</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;"><span style="font-weight: bolder;">* कवितेचा अर्थ -&nbsp;</span></span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;"># तू बुद्धि दे तू तेज दे नवचेतना विश्वास दे जे सत्य सुंदर सर्वथा आजन्म त्याचा ध्यास दे</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;"><span style="font-weight: bolder;">उत्तर -</span>&nbsp;या प्रार्थनेद्वारे&nbsp;</span><span style="font-size: 18px;">कवी</span><span style="font-size: 18px;">&nbsp;सांगतात की ; हे ईश्वरा तू आम्हाला बुद्धी दे नव विचारातून तेज दे नवचेतना जागवण्याचा विश्वास दे सर्व अर्थाने जे सत्य सुंदर आहे अशाच गोष्टींचा ध्यास मला लागू दे.</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;"># हरवले आभाळ ज्यांचे हो तयांचा सोबती, सापडेना वाट ज्यांना हो तयांचा सारथी साधना करिती तुझी जे नित्य सहवास दे.</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;"><span style="font-weight: bolder;">उत्तर -&nbsp;</span>ज्यांच्या डोक्यावर मायेचे छत्र नाही, सांभाळ करणारा कोणी नाही त्यांचा तू सोबती- सखा बन. जीवनाच्या प्रवासात ज्यांची वाट चुकली आहे त्यांचा सारथी होऊन तू योग्य मार्गावर घेऊन जा दररोज तुझी जे साधना करतात त्यांच्या सहवासात तू सदैव राहा</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;"># जानावया दुर्बलांचे दुःख आणि वेदना तेवत्या&nbsp; राही सदा रंध्रातून धमण्यातल्या रूधि रास या खल भेटण्याची आस दे सामर्थ्य या शब्दांस अर्थ या जगण्यास दे&nbsp;</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;"><span style="font-weight: bolder;">उत्तर -</span>&nbsp;या जगातील दुर्बलांचे दुःख&nbsp; वेदना जाणून घेण्यासाठी माझ्या शरीरातील रंध्रात संवेदना तेवत्या ठेवण्याचे काम तू कर माझ्या शरीरात सळसळणाऱ्या रक्तास प्रत्येक दृष्ट वाईट गोष्टीवर मात करण्याची ताकद दे ही शब्दरूपी प्रार्थना मी तुला करत असून यातील शब्दांना सामर्थ्य देते आणि माझं जीवन सार्थकी लागू दे.</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;"># सन्मार्ग आणि सन्मती लाभो सदा सस्तंगती नीती ना ही भ्रष्ट हो जरी संकटे आली किती पंखास&nbsp; या बळ दे&nbsp; नवे झेपावण्या आकाश दे.</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;"><span style="font-weight: bolder;">उत्तर -&nbsp;</span>मला सतत चांगल्या मार्गावर जाण्याची सद्बुद्धी दे तसेच सज्जनांचा सहवास लाभू दे माझ्या जीवनात कितीही संकटे आली तरी माझी नीतिभ्रष्ट होऊ देऊ नकोस चांगली व्यक्ती जीवन जगण्यासाठी माझ्या पंखांना बळ दे .कृतीसाठी नवे आकाश दे म्हणजेच माझे कर्तुत्व सिद्ध करण्यासाठी नवनवीन संधी दे .</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 17.6px;"><br></span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;"><span style="font-weight: bolder;">शब्दार्थ -&nbsp;</span></span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">आजन्म - जन्मभर</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">ध्यास&nbsp; - उत्कट इच्छा</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">तयांचा - त्यांचा</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">सारथी - सारथ्य करणारा</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">साधना -&nbsp; तपश्चर्या</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">नित्य - नेहमी , सतत</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">तव- त्यांना</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">दुर्बल- बल (शक्ती) कमी असणारा.</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">जाणावया - जाणून घेण्यासाठी</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">रंन्ध्र - त्वचेवरील अति सूक्ष्म छिद्र</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">धमन्या - रक्तवाहिन्या</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">रुधिर - नाही रवन्त&nbsp;</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">खल&nbsp; - दृष्ट</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">आस&nbsp; - इच्छा</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">सामर्थ्य&nbsp; - शक्ती</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">सन्मार्ग&nbsp; - चांगला मार्ग</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">संन्मती&nbsp; - चांगली बुद्धी</span></p><p style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.4; text-align: justify;"><span style="font-size: 18px;">सत्संगती - चांगली सोबत&nbsp; (संगत)</span></p>', NULL, NULL, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_ChapterContent] ([ChapterContentId], [ChapterId], [SectionId], [ChapterTextContents], [ChapterImageContents], [ChapterVideoContents], [ContentType], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10002, 2, 3, NULL, N'~/UploadedContentFiles/Image/Standard_1/Medium_1/Subject_1/Chapter_2/Section_3/1.png', NULL, 2, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_ChapterContent] ([ChapterContentId], [ChapterId], [SectionId], [ChapterTextContents], [ChapterImageContents], [ChapterVideoContents], [ContentType], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10003, 2, 3, NULL, N'~/UploadedContentFiles/Image/Standard_1/Medium_1/Subject_1/Chapter_2/Section_3/2.png', NULL, 2, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_ChapterContent] ([ChapterContentId], [ChapterId], [SectionId], [ChapterTextContents], [ChapterImageContents], [ChapterVideoContents], [ContentType], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10004, 2, 3, NULL, N'~/UploadedContentFiles/Image/Standard_1/Medium_1/Subject_1/Chapter_2/Section_3/3.png', NULL, 2, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_ChapterContent] ([ChapterContentId], [ChapterId], [SectionId], [ChapterTextContents], [ChapterImageContents], [ChapterVideoContents], [ContentType], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10005, 1, 4, N'<p class="MsoNormal" style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.7em; text-align: center;"><span style="font-size: 16pt; line-height: 24.5333px; font-family: Mangal, serif;"><span style="font-weight: bolder;"><span lang="EN-US" style="font-size: 24px; line-height: 27.6px; color: rgb(204, 0, 102); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">जय जय हे भारत देशा</span></span><font color="#ff0000"><br></font></span></p><p class="MsoNormal" style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.7em; text-align: right;"><span style="font-size: 16pt; line-height: 24.5333px; font-family: Mangal, serif;"><font color="#ff0000">मंगेश पाडगावकर (१९२९ ते २०१५)</font></span></p><p class="MsoNormal" style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.7em;"><span style="line-height: 20.24px;"><span style="font-weight: bolder;"><span lang="EN-US" style="font-size: 14pt; line-height: 21.4667px; font-family: Mangal, serif; color: rgb(0, 153, 0); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">प्रस्तावना:</span></span><br></span></p><p class="MsoNormal" style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.5; text-align: justify;"><span style="font-family: Mangal, serif; font-size: 18px;">प्रथितयश कवी. वृत्तबद्ध रचना, शब्दप्रभुत्व, गेयता ही त्यांच्या काव्याची वैशिष्ट्ये आहेत. त्यांचे ‘धारानृत्य’, ‘जिप्सी’, ‘छोरी’, ‘उत्सव’, ‘विदूषक’, ‘सलाम’, ‘गझल’, ‘भटके&nbsp; &nbsp;पक्षी’, ‘बोलगाणी’ हे कवितासंग्रह; संत मीराबाई, संत कबीर आणि संत तुलसीदास यांच्या काव्याचे भावानुवाद; ‘बोरकरांची कविता’ व ‘संहिता’ ही महत्त्वपूर्ण संपादने; ‘निंबोणीच्या झाडामागे’ हा ललित निबंधसंग्रह; ‘भोलानाथ’, ‘बबलगम’, ‘चांदोमामा’ हे बालगीतसंग्रह इत्यादी लेखन प्रसिद्ध आहे. ‘वात्रटिका’ हा संग्रह त्यांच्या वाङ्मयीन व्यक्तिमत्त्वाचा आणखी एक पैलू दाखवणारा आहे.</span><br></p><p class="MsoNormal" style="font-family: Poppins, sans-serif; font-size: 1.1em; line-height: 1.5; text-align: justify;"><span style="font-family: Mangal, serif; font-size: 18px;">प्रस्तुत गीतात देशप्रेम आणि एकात्मता यांचा आविष्कार झालेला दिसतो.</span></p>', NULL, NULL, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_ChapterContent] ([ChapterContentId], [ChapterId], [SectionId], [ChapterTextContents], [ChapterImageContents], [ChapterVideoContents], [ContentType], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10006, 1, 6, N'<p><span style="font-family: Poppins, sans-serif; font-size: 17.6px;">&nbsp; &nbsp; &nbsp; &nbsp;</span><span style="font-family: Poppins, sans-serif; font-size: 17.6px; text-align: center;"><font color="#003163"><span style="font-weight: bolder;"><span style="font-size: 24px;">(प्रस्तुत गीत हे काव्यानंदासाठी घेतले&nbsp;आहे. )</span>&nbsp;</span></font></span><br></p>', NULL, NULL, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_ChapterContent] ([ChapterContentId], [ChapterId], [SectionId], [ChapterTextContents], [ChapterImageContents], [ChapterVideoContents], [ContentType], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10007, 1, 3, NULL, N'~/UploadedContentFiles/Image/Standard_1/Medium_1/Subject_1/Chapter_1/Section_3/2.jpg', NULL, 2, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_ChapterContent] ([ChapterContentId], [ChapterId], [SectionId], [ChapterTextContents], [ChapterImageContents], [ChapterVideoContents], [ContentType], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10009, 2, 4, NULL, NULL, N'~/UploadedContentFiles/Video/Standard_1/Medium_1/Subject_1/Chapter_2/Section_4/S1 Ch1_80cdfa38-7838-48c0-be00-9c178d82953c.mp4', 3, 1, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Mas_ChapterContent] OFF
GO
SET IDENTITY_INSERT [dbo].[Mas_Coupon] ON 

GO
INSERT [dbo].[Mas_Coupon] ([CouponId], [CouponCode], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'ABCD', 1, NULL, CAST(N'2020-04-18 18:27:53.423' AS DateTime), NULL, CAST(N'2020-04-18 18:27:53.423' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Mas_Coupon] OFF
GO
SET IDENTITY_INSERT [dbo].[Mas_ErrorLogs] ON 

GO
INSERT [dbo].[Mas_ErrorLogs] ([ErrorLogId], [ErrorProcedure], [ErrorLine], [ErrorMessage]) VALUES (1, N'Inserted', 12, N'Inserted')
GO
INSERT [dbo].[Mas_ErrorLogs] ([ErrorLogId], [ErrorProcedure], [ErrorLine], [ErrorMessage]) VALUES (2, N'Inserted', 12, N'Inserted')
GO
INSERT [dbo].[Mas_ErrorLogs] ([ErrorLogId], [ErrorProcedure], [ErrorLine], [ErrorMessage]) VALUES (3, N'Inserted', 12, N'Inserted')
GO
INSERT [dbo].[Mas_ErrorLogs] ([ErrorLogId], [ErrorProcedure], [ErrorLine], [ErrorMessage]) VALUES (4, N'Inserted', 12, N'Inserted')
GO
INSERT [dbo].[Mas_ErrorLogs] ([ErrorLogId], [ErrorProcedure], [ErrorLine], [ErrorMessage]) VALUES (5, N'Inserted', 12, N'Inserted')
GO
INSERT [dbo].[Mas_ErrorLogs] ([ErrorLogId], [ErrorProcedure], [ErrorLine], [ErrorMessage]) VALUES (6, N'Inserted', 12, N'Inserted')
GO
INSERT [dbo].[Mas_ErrorLogs] ([ErrorLogId], [ErrorProcedure], [ErrorLine], [ErrorMessage]) VALUES (7, N'Inserted', 12, N'Inserted')
GO
INSERT [dbo].[Mas_ErrorLogs] ([ErrorLogId], [ErrorProcedure], [ErrorLine], [ErrorMessage]) VALUES (8, N'Inserted', 12, N'Inserted')
GO
INSERT [dbo].[Mas_ErrorLogs] ([ErrorLogId], [ErrorProcedure], [ErrorLine], [ErrorMessage]) VALUES (9, N'Inserted', 12, N'Inserted')
GO
SET IDENTITY_INSERT [dbo].[Mas_ErrorLogs] OFF
GO
SET IDENTITY_INSERT [dbo].[Mas_Login] ON 

GO
INSERT [dbo].[Mas_Login] ([LoginId], [PhoneNumber], [FirstName], [LastName], [Password], [DateOfBirth], [EmailId], [City], [SchoolName], [ContantNumber], [ExpiryDate], [IsActive]) VALUES (1, N'9763968153', N'Tushar', N'Harlikar', N'JayUsha@81185', CAST(N'1985-11-08 00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, CAST(N'2021-05-01 00:00:00.000' AS DateTime), 0)
GO
INSERT [dbo].[Mas_Login] ([LoginId], [PhoneNumber], [FirstName], [LastName], [Password], [DateOfBirth], [EmailId], [City], [SchoolName], [ContantNumber], [ExpiryDate], [IsActive]) VALUES (2, N'9763991456', N'Mayuri', N'Barge', N'mayu', CAST(N'2020-04-07 00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, CAST(N'2021-05-01 00:00:00.000' AS DateTime), 0)
GO
SET IDENTITY_INSERT [dbo].[Mas_Login] OFF
GO
SET IDENTITY_INSERT [dbo].[Mas_Medium] ON 

GO
INSERT [dbo].[Mas_Medium] ([MediumId], [MediumName], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'मराठी माध्यम', N'मराठी  माध्यम', 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Medium] ([MediumId], [MediumName], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'सेमी English माध्यम', N'सेमी English माध्यम', 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Medium] ([MediumId], [MediumName], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'English Medium', N'English Medium', 1, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Mas_Medium] OFF
GO
SET IDENTITY_INSERT [dbo].[Mas_Role] ON 

GO
INSERT [dbo].[Mas_Role] ([RoleId], [RoleName], [IsActive]) VALUES (1, N'Super Admin', N'1         ')
GO
INSERT [dbo].[Mas_Role] ([RoleId], [RoleName], [IsActive]) VALUES (2, N'Admin', N'1         ')
GO
INSERT [dbo].[Mas_Role] ([RoleId], [RoleName], [IsActive]) VALUES (3, N'Employee', N'1         ')
GO
SET IDENTITY_INSERT [dbo].[Mas_Role] OFF
GO
SET IDENTITY_INSERT [dbo].[Mas_Section] ON 

GO
INSERT [dbo].[Mas_Section] ([SectionId], [SectionName], [Description], [IsStandardSection], [IsChapterSection], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'कृतिपत्रिकेचे स्वरुप', N'कृतिपत्रिकेचे स्वरुप', 1, 0, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Section] ([SectionId], [SectionName], [Description], [IsStandardSection], [IsChapterSection], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'मार्गदर्शन', N'मार्गदर्शन', 1, 0, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Section] ([SectionId], [SectionName], [Description], [IsStandardSection], [IsChapterSection], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'पाठ्यपुस्तक', N'पाठ्यपुस्तक', 0, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Section] ([SectionId], [SectionName], [Description], [IsStandardSection], [IsChapterSection], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'नोट्स', N'नोट्स', 0, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Section] ([SectionId], [SectionName], [Description], [IsStandardSection], [IsChapterSection], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, N'स्वाध्याय', N'स्वाध्याय', 0, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Section] ([SectionId], [SectionName], [Description], [IsStandardSection], [IsChapterSection], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, N'महत्वाचे प्रश्न', N'महत्वाचे प्रश्न', 0, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Section] ([SectionId], [SectionName], [Description], [IsStandardSection], [IsChapterSection], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, N'Exercise', N'Exercise', 1, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Section] ([SectionId], [SectionName], [Description], [IsStandardSection], [IsChapterSection], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10007, N'प्रश्नपत्रिका १', N'प्रश्नपत्रिका १', 1, 0, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Section] ([SectionId], [SectionName], [Description], [IsStandardSection], [IsChapterSection], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10008, N'उत्तरपत्रिका १', N'उत्तरपत्रिका १', 1, 0, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Section] ([SectionId], [SectionName], [Description], [IsStandardSection], [IsChapterSection], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10009, N'प्रश्नपत्रिका २', N'प्रश्नपत्रिका २', 1, 0, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Section] ([SectionId], [SectionName], [Description], [IsStandardSection], [IsChapterSection], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10010, N'प्रश्नपत्रिका ३', N'प्रश्नपत्रिका ३', 1, 0, 1, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Mas_Section] OFF
GO
SET IDENTITY_INSERT [dbo].[Mas_Standard] ON 

GO
INSERT [dbo].[Mas_Standard] ([StandardId], [StandardName], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'10', N'10th Standard', 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Standard] ([StandardId], [StandardName], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'9', N'9th Standard', 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Standard] ([StandardId], [StandardName], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'8', N'8th Standard', 0, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Mas_Standard] OFF
GO
SET IDENTITY_INSERT [dbo].[Mas_Student] ON 

GO
INSERT [dbo].[Mas_Student] ([StudentId], [LoginId], [StandardId], [MediumId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 1, 1, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Student] ([StudentId], [LoginId], [StandardId], [MediumId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 2, 1, 1, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Mas_Student] OFF
GO
SET IDENTITY_INSERT [dbo].[Mas_Subject] ON 

GO
INSERT [dbo].[Mas_Subject] ([SubjectId], [SubjectName], [SubjectThumbnail], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'कुमारभारती', NULL, N'कुमारभारती', 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Subject] ([SubjectId], [SubjectName], [SubjectThumbnail], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'MY English Course', NULL, N'MY English Course', 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Subject] ([SubjectId], [SubjectName], [SubjectThumbnail], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'इतिहास व राज्यशास्र', NULL, N'इतिहास व राज्यशास्र', 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Subject] ([SubjectId], [SubjectName], [SubjectThumbnail], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'भूगोल', NULL, N'भूगोल', 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Subject] ([SubjectId], [SubjectName], [SubjectThumbnail], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, N'हिंदी लोकवानी', N'~/UploadedContentFiles/SubjectThumbnail/Standard_1/Hindi_7d1b6a95-bfdc-4ecb-85bd-0bcfc1332c89.png', N'हिंदी लोकवानी', 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_Subject] ([SubjectId], [SubjectName], [SubjectThumbnail], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, N'हिंदी लोकभारती ', N'~/UploadedContentFiles/SubjectThumbnail/Standard_1/Hindi_ee3791ad-44ce-42ea-808b-d1a5393a7eca.png', N'हिंदी लोकभारती ', 1, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Mas_Subject] OFF
GO
SET IDENTITY_INSERT [dbo].[Mas_SubjectContent] ON 

GO
INSERT [dbo].[Mas_SubjectContent] ([SubjectContentId], [StandardId], [SubjectId], [MediumId], [SectionId], [SubjectTextContents], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 1, 1, 1, 1, N'<p>Test 1234<br></p>', 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Mas_SubjectContent] ([SubjectContentId], [StandardId], [SubjectId], [MediumId], [SectionId], [SubjectTextContents], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 1, 1, 1, 2, N'<p>Margdarshan 1234<br></p>', 1, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Mas_SubjectContent] OFF
GO
ALTER TABLE [dbo].[Mas_Sponsorer] ADD  CONSTRAINT [DF_Mas_Sponsorer_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  StoredProcedure [Admin].[uspGetUserRoles]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Admin].[uspGetUserRoles]
	@UserName varchar(300)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT MA.AdminId,MA.UserName,MA.IsActive,
	MA.RoleId,MR.RoleName
	FROM Mas_Admin MA LEFT JOIN Mas_Role MR
	ON MA.RoleId=MR.RoleId
	WHERE UserName=@UserName
END

GO
/****** Object:  StoredProcedure [Admin].[uspLoginAdmin]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Admin].[uspLoginAdmin]
	@UserName varchar(300),
	@Password varchar(300)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT MA.AdminId,MA.UserName,MA.IsActive,
	MA.RoleId,MR.RoleName
	FROM Mas_Admin MA LEFT JOIN Mas_Role MR
	ON MA.RoleId=MR.RoleId
	WHERE UserName=@UserName AND Password=@Password
END

GO
/****** Object:  StoredProcedure [dbo].[uspActiveDeactivateChapter]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Standard Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspActiveDeactivateChapter]
	@ChapterId bigint,
	@IsActive bit,
	@RowCount int OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       UPDATE Mas_Chapter SET 
	   IsActive=@IsActive
	   WHERE ChapterId=@ChapterId
	   SET @RowCount= @@RowCount
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount=0
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspActiveDeactivateChapterSection]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Active/Deactive medium Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspActiveDeactivateChapterSection]
	@SectionId bigint,
	@IsChapterSection bit,
	@RowCount int OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       UPDATE Mas_Section SET 
	   IsChapterSection=@IsChapterSection
	   WHERE SectionId=@SectionId
	   SET @RowCount=@@ROWCOUNT;
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount=0;
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspActiveDeactivateCoupon]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <07-03-2020>
-- Description:	<Acitvate Deactivate Sponsorer>
-- =============================================
CREATE PROCEDURE [dbo].[uspActiveDeactivateCoupon]
	@CouponId bigint,
	@IsActive bit,
	@RowCount int OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       UPDATE Mas_Coupon SET 
	   IsActive=@IsActive,
	   ModifiedDate = GETDATE()
	   WHERE CouponId=@CouponId
	   SET @RowCount= @@RowCount
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount=0
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspActiveDeactivateMedium]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Active/Deactive medium Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspActiveDeactivateMedium]
	@MediumId bigint,
	@IsActive bit,
	@RowCount int OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       UPDATE Mas_Medium SET 
	   IsActive=@IsActive
	   WHERE MediumId=@MediumId
	   SET @RowCount=@@ROWCOUNT;
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount=0;
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspActiveDeactivateSection]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Active/Deactive medium Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspActiveDeactivateSection]
	@SectionId bigint,
	@IsActive bit,
	@RowCount int OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       UPDATE Mas_Section SET 
	   IsActive=@IsActive
	   WHERE SectionId=@SectionId
	   SET @RowCount=@@ROWCOUNT;
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount=0;
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspActiveDeactivateSponsorer]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <07-03-2020>
-- Description:	<Acitvate Deactivate Sponsorer>
-- =============================================
CREATE PROCEDURE [dbo].[uspActiveDeactivateSponsorer]
	@SponsorerId bigint,
	@IsActive bit,
	@RowCount int OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       UPDATE Mas_Sponsorer SET 
	   IsActive=@IsActive
	   WHERE SponsorerId=@SponsorerId
	   SET @RowCount= @@RowCount
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount=0
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspActiveDeactivateStandard]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Standard Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspActiveDeactivateStandard]
	@StandardId bigint,
	@IsActive bit,
	@RowCount int OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       UPDATE Mas_Standard SET 
	   IsActive=@IsActive
	   WHERE StandardId=@StandardId
	   SET @RowCount= @@RowCount
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount=0
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspActiveDeactivateStandardSection]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Active/Deactive medium Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspActiveDeactivateStandardSection]
	@SectionId bigint,
	@IsStandardSection bit,
	@RowCount int OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       UPDATE Mas_Section SET 
	   IsStandardSection=@IsStandardSection
	   WHERE SectionId=@SectionId
	   SET @RowCount=@@ROWCOUNT;
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount=0;
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspActiveDeactivateSubject]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Active/Deactive medium Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspActiveDeactivateSubject]
	@SubjectId bigint,
	@IsActive bit,
	@RowCount int OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       UPDATE Mas_Subject SET 
	   IsActive=@IsActive
	   WHERE SubjectId=@SubjectId
	   SET @RowCount=@@ROWCOUNT;
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount=0;
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetAllChapters]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAllChapters] 
	@ChapterId bigint=null,
	@IsActive bit=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
	SELECT MC.ChapterId, MC.ChapterName ,
	MC.StandardId, MS.StandardName, 
	MC.MediumId , MM.MediumName,
	MC.SubjectId ,MSS.SubjectName,
	MC.ChapterThumbnail AS ChapterBytes, MC.Description,
	MC.IsActive
	FROM Mas_Chapter MC LEFT JOIN Mas_Standard MS
	ON MC.StandardId =MS.StandardId LEFT JOIN Mas_Medium MM
	ON MC.MediumId = MM.MediumId LEFT JOIN Mas_Subject MSS
	ON MC.SubjectId = MSS.SubjectId
	WHERE (ISNULL(@ChapterId,0)=0 OR MC.ChapterId=@ChapterId) AND (ISNULL(@IsActive,0)=0 OR MC.IsActive=@IsActive)
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetAllCoupons]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAllCoupons] 
	@CouponId bigint=null,
	@IsActive bit=null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
	SELECT MC.CouponId,MC.CouponCode,MC.IsActive,MC.CreatedBy,MC.CreatedDate,MC.ModifiedBy,MC.ModifiedDate FROM Mas_Coupon MC
	WHERE (ISNULL(@CouponId,0)=0 OR MC.CouponId=@CouponId) AND (ISNULL(@IsActive,0)=0 OR MC.IsActive=@IsActive)
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetAllLnkCouponDetails]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <07-03-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAllLnkCouponDetails] 
	@CouponId bigint=null,
	@SponsorerId bigint=null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
	SELECT LCD.CouponDetailsId,LCD.CouponId,LCD.SponsorerId FROM Lnk_CouponDetails LCD
	WHERE (ISNULL(@SponsorerId,0)=0 OR LCD.SponsorerId=@SponsorerId) AND (ISNULL(@CouponId,0)=0 OR LCD.CouponId=@CouponId)
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetAllMediums]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Medium Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAllMediums] 
	@MediumId bigint=null,
	@IsActive bit=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	

    SELECT MediumId,MediumName,Description,IsActive
	FROM Mas_Medium WHERE 
	(ISNULL(@MediumId,0)=0 OR MediumId=@MediumId) AND (ISNULL(@IsActive,0)=0 OR IsActive=@IsActive)
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetAllMediumSectionConfig]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Standard Medium Configuration>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAllMediumSectionConfig] 
@MediumSectionId bigint=null 
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
        SELECT MS.SectionId,MS.SectionName,MS.Description AS SectionDescription,
		MM.MediumId,MM.MediumName,MM.Description AS MediumDescription,
		LMS.MediumSectionId, LMS.IsActive
		FROM Mas_Section MS CROSS JOIN Mas_Medium MM
		LEFT JOIN Lnk_MediumSections LMS ON 
		MS.SectionId=LMS.SectionId AND MM.MediumId=LMS.MediumId
		WHERE (LMS.MediumSectionId=@MediumSectionId OR ISNULL(@MediumSectionId,0)=0)
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetAllSections]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Standard Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAllSections] 
	@SectionId bigint=null,
	@IsActive bit=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    SELECT SectionId,SectionName,Description,IsStandardSection,IsChapterSection,IsActive
	FROM Mas_Section WHERE 
	(ISNULL(@SectionId,0)=0 OR SectionId=@SectionId) AND (ISNULL(@IsActive,0)=0 OR IsActive=@IsActive)
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetAllSponsorer]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <07-03-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAllSponsorer] 
	@SponsorerId bigint=null,
	@IsActive bit=null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
	SELECT MSP.SponsorerId,MSP.SponsorerName,MSP.ThumbnailImage AS ThumbnailImageByteData ,MSP.LandingImage AS LandingImageByteData,MSP.IsActive,MSP.CreatedDate,MSP.CreatedBy FROM Mas_Sponsorer MSP
	WHERE (ISNULL(@SponsorerId,0)=0 OR MSP.SponsorerId=@SponsorerId) AND (ISNULL(@IsActive,0)=0 OR MSP.IsActive=@IsActive)
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetAllStandardMediumConfig]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Standard Medium Configuration>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAllStandardMediumConfig] 
@StandardMediumId bigint=null 
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
        SELECT MS.StandardId,MS.StandardName,MS.Description AS StandardDescription,
		MM.MediumId,MM.MediumName,MM.Description AS MediumDescription,
		LSM.StandardMediumId, LSM.IsActive
		FROM Mas_Standard MS CROSS JOIN Mas_Medium MM
		LEFT JOIN Lnk_StandardMedium LSM ON 
		MS.StandardId=LSM.StandardId AND MM.MediumId=LSM.MediumId
		WHERE (LSM.StandardMediumId=@StandardMediumId OR ISNULL(@StandardMediumId,0)=0)
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetAllStandards]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Standard Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAllStandards] 
	@StandardId bigint=null,
	@IsActive bit=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    SELECT StandardId,StandardName,Description,IsActive
	FROM Mas_Standard WHERE 
	(ISNULL(@StandardId,0)=0 OR StandardId=@StandardId) AND (ISNULL(@IsActive,0)=0 OR IsActive=@IsActive)
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetAllSubject]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAllSubject] 
	@SubjectId bigint=null,
	@IsActive bit=null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
	SELECT LSD.SubjectId,MS.SubjectName,MS.Description,MS.IsActive AS IsSubjectActive,SubjectThumbnail AS SubjectThumbnailPath,
	MM.MediumId,MM.MediumName,LSD.IsActive AS IsSubjectMediumActive,
	MSD.StandardId,MSD.StandardName
	FROM Lnk_SubjectDetails LSD  LEFT JOIN Mas_Subject MS
	ON LSD.SubjectId =MS.SubjectId
	LEFT JOIN Mas_Medium MM
	ON  LSD.MediumId=MM.MediumId LEFT JOIN Mas_Standard MSD
	ON LSD.StandardId=MSD.StandardId
	WHERE (ISNULL(@SubjectId,0)=0 OR LSD.SubjectId=@SubjectId) AND (ISNULL(@IsActive,0)=0 OR LSD.IsActive=@IsActive)
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetChapterContent]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[uspGetChapterContent] 
  @StandardId as BigInt=null,
  @MediumId as BigInt=null,
  @ChapterId as BigInt =null,
  @SectionId as Bigint=null,
  @SubjectId as BigInt =null,
  @IsSingleRecord as bit=1
AS
BEGIN
	SET NOCOUNT ON
	if(@IsSingleRecord<>1)
	BEGIN
		SELECT
		MCC.ChapterContentId,

		MC.StandardId,
		MSD.StandardName,

		MC.MediumId,
		MM.MediumName,

		MC.SubjectId,
		MSJ.SubjectName,

		MCC.ChapterId, 
		MC.ChapterName,

		MCC.SectionId,
		MS.SectionName,

		MCC.ContentType,
		MCC.ChapterTextContents,
		MCC.ChapterImageContents,
		MCC.ChapterVideoContents
		
		FROM Mas_ChapterContent MCC LEFT JOIN Mas_Chapter MC
		ON MCC.ChapterId=MC.ChapterId LEFT JOIN Mas_Section MS
		ON MCC.SectionId =MS.SectionId LEFT JOIN Mas_Standard MSD
		ON MC.StandardId=MSD.StandardId LEFT JOIN Mas_Medium MM 
		ON MC.MediumId=MM.MediumId LEFT JOIN Mas_Subject MSJ
		ON MC.SubjectId=MSJ.SubjectId
		WHERE 
		(ISNULL(@StandardId,0)=0 OR MC.StandardId=@StandardId) 
		AND (ISNULL(@MediumId,0)=0 OR MC.MediumId=@MediumId) 
		AND (ISNULL(@ChapterId,0)=0 OR MC.ChapterId=@ChapterId) 
		AND (ISNULL(@SectionId,0)=0 OR MCC.SectionId=@SectionId) 
		AND (ISNULL(@SubjectId,0)=0 OR MC.SubjectId=@SubjectId)
	END
	ELSE
	BEGIN
		 
		SELECT * FROM 
		(
		SELECT
		ROW_NUMBER() OVER(PARTITION BY MCC.ChapterId,MCC.SectionId ORDER BY MCC.ChapterId,MCC.SectionId) as rowno,
		MCC.ChapterContentId,

		MC.StandardId,
		MSD.StandardName,

		MC.MediumId,
		MM.MediumName,

		MC.SubjectId,
		MSJ.SubjectName,

		MCC.ChapterId, 
		MC.ChapterName,

		MCC.SectionId,
		MS.SectionName,

		MCC.ContentType,
		MCC.ChapterTextContents,
		MCC.ChapterImageContents,
		MCC.ChapterVideoContents
		
		FROM Mas_ChapterContent MCC LEFT JOIN Mas_Chapter MC
		ON MCC.ChapterId=MC.ChapterId LEFT JOIN Mas_Section MS
		ON MCC.SectionId =MS.SectionId LEFT JOIN Mas_Standard MSD
		ON MC.StandardId=MSD.StandardId LEFT JOIN Mas_Medium MM 
		ON MC.MediumId=MM.MediumId LEFT JOIN Mas_Subject MSJ
		ON MC.SubjectId=MSJ.SubjectId
		WHERE 
		(ISNULL(@StandardId,0)=0 OR MC.StandardId=@StandardId) 
		AND (ISNULL(@MediumId,0)=0 OR MC.MediumId=@MediumId) 
		AND (ISNULL(@ChapterId,0)=0 OR MC.ChapterId=@ChapterId) 
		AND (ISNULL(@SectionId,0)=0 OR MCC.SectionId=@SectionId) 
		AND (ISNULL(@SubjectId,0)=0 OR MC.SubjectId=@SubjectId)
		) as tblChapter
		WHERE tblChapter.rowno=1
	END
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetChapterListBySubject]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[uspGetChapterListBySubject]
  @SubjectId as BigInt,
  @StandardId as BigInt,
  @MediumId as BigInt
AS
BEGIN
	
	SET NOCOUNT ON;
	IF EXISTS (SELECT  MC.ChapterId  FROM Mas_Chapter MC 
		WHERE MC.StandardId=@StandardId AND MC.SubjectId=@SubjectId AND MC.MediumId=@MediumId)
	BEGIN
		SELECT  MC.ChapterId,MC.ChapterName FROM Mas_Chapter MC 
		WHERE MC.StandardId=@StandardId AND MC.SubjectId=@SubjectId AND MC.MediumId=@MediumId
		AND MC.IsActive=1
	END
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetConfiguredStandardMedium]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Standard Medium Configuration>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetConfiguredStandardMedium] 
	@StandardId bigint
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
        SELECT MS.StandardId,MS.StandardName,MS.Description AS StandardDescription,
		MM.MediumId,MM.MediumName,MM.Description AS MediumDescription,
		LSM.StandardMediumId, LSM.IsActive
		FROM Lnk_StandardMedium LSM LEFT JOIN Mas_Standard MS ON LSM.StandardId=MS.StandardId
		LEFT JOIN Mas_Medium MM ON LSM.MediumId=MM.MediumId
		WHERE LSM.IsActive=1 AND LSM.StandardId= @StandardId
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetConfiguredSubjectList]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Standard Medium Configuration>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetConfiguredSubjectList] 
	@StandardId bigint,
	@MediumId bigint
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
        SELECT * FROM Mas_Subject MS LEFT JOIN Lnk_SubjectDetails LSD
		ON MS.SubjectId=LSD.SubjectId 
		WHERE LSD.StandardId=@StandardId AND LSD.MediumId=@MediumId
		AND LSD.IsActive=1
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetSectionsByChapter]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[uspGetSectionsByChapter]
  @ChapterId as BigInt,
  @MediumId as BigInt
AS
BEGIN
	
	SET NOCOUNT ON;
	IF EXISTS (SELECT  MS.SectionId  FROM Mas_Section MS LEFT JOIN Lnk_MediumSections LMS
			ON MS.SectionId=LMS.SectionId
			WHERE LMS.MediumId=@MediumId)
	BEGIN
		SELECT MS.SectionId,MS.SectionName FROM Mas_Section MS LEFT JOIN Lnk_MediumSections LMS
		ON MS.SectionId=LMS.SectionId LEFT JOIN Mas_Chapter MC 
		ON MC.MediumId=LMS.MediumId 
		WHERE MC.ChapterId=@ChapterId AND MC.MediumId=@MediumId
		AND MS.IsChapterSection=1
	END
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetSubjectContent]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[uspGetSubjectContent] 
  @StandardId as BigInt=null,
  @MediumId as BigInt=null,
  @SubjectId as BigInt=null,
  @SectionId as Bigint=null
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT 
	MCC.StandardId,
	MS.StandardName,
	MCC.MediumId,
	MM.MediumName,
	MCC.SubjectId,
	MSS.SubjectName,
	MCC.SectionId,
	MSC.SectionName,
	MCC.SubjectTextContents

	FROM Mas_SubjectContent MCC LEFT JOIN Mas_Standard MS
	ON MCC.StandardId=MS.StandardId LEFT JOIN Mas_Section MSC
	ON MCC.SectionId=MSC.SectionId LEFT JOIN Mas_Medium MM
	ON MCC.MediumId=MM.MediumId LEFT JOIN Mas_Subject MSS
	ON MCC.SubjectId=MSS.SubjectId
	WHERE 
	(ISNULL(@StandardId,0)=0 OR MCC.StandardId=@StandardId) 
	AND (ISNULL(@MediumId,0)=0 OR MCC.StandardId=@MediumId) 
	AND (ISNULL(@SubjectId,0)=0 OR MCC.SubjectId=@SubjectId) 
	AND  (ISNULL(@SectionId,0)=0 OR MCC.SectionId=@SectionId)
	AND MCC.IsActive=1
END

GO
/****** Object:  StoredProcedure [dbo].[uspInsertChapter]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspInsertChapter]
	@StandardId bigint,
	@SubjectId bigint,
	@MediumId bigint,
	@ChapterName nvarchar(150),
	@ChapterThumbnail varbinary(MAX)=null,
	@Description nvarchar(100),
	@ChapterId bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       INSERT INTO  Mas_Chapter(StandardId,SubjectId,MediumId,ChapterName,ChapterThumbnail,Description,IsActive) 
	   VALUES (@StandardId,@SubjectId,@MediumId,@ChapterName,@ChapterThumbnail,@Description,1)
	   
	   SET @ChapterId =SCOPE_IDENTITY()
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @ChapterId =0
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspInsertChapterContent]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspInsertChapterContent]
	@ChapterId bigint,
	@SectionId bigint,
	@ContentType int,
	@ChapterTextContents nvarchar(MAX)=null,
	@ChapterVideoContents nvarchar(500)=null,
	@ChapterImageContents nvarchar(500)=null,
	@ChapterContentId bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY

	   IF(@ContentType=1) -- Text Content
	   BEGIN
			IF EXISTS(SELECT ChapterContentId FROM Mas_ChapterContent WHERE ChapterId=@ChapterId AND SectionId=@SectionId)
			BEGIN
				UPDATE Mas_ChapterContent SET ChapterTextContents=@ChapterTextContents,IsActive=1 WHERE 
				ChapterId=@ChapterId AND SectionId=@SectionId

				SET @ChapterContentId =@@ROWCOUNT
			END
			ELSE 
			BEGIN
				INSERT INTO  Mas_ChapterContent(ChapterId,SectionId,ChapterTextContents,ChapterImageContents,ChapterVideoContents,ContentType,IsActive) 
				   VALUES (@ChapterId,@SectionId,@ChapterTextContents,@ChapterImageContents,@ChapterVideoContents,@ContentType,1)
	   
				   SET @ChapterContentId =SCOPE_IDENTITY()
			END
	   END
	   ELSE IF(@ContentType=3) -- Video Content
	   BEGIN
			IF EXISTS(SELECT ChapterContentId FROM Mas_ChapterContent WHERE ChapterId=@ChapterId AND SectionId=@SectionId)
			BEGIN
				UPDATE Mas_ChapterContent SET ChapterVideoContents=@ChapterVideoContents,IsActive=1 WHERE 
				ChapterId=@ChapterId AND SectionId=@SectionId

				SET @ChapterContentId =@@ROWCOUNT
			END
			ELSE 
			BEGIN
				INSERT INTO  Mas_ChapterContent(ChapterId,SectionId,ChapterTextContents,ChapterImageContents,ChapterVideoContents,ContentType,IsActive) 
				   VALUES (@ChapterId,@SectionId,@ChapterTextContents,@ChapterImageContents,@ChapterVideoContents,@ContentType,1)
	   
				   SET @ChapterContentId =SCOPE_IDENTITY()
			END
	   END
	   ELSE -- Image Content
	   BEGIN
		   INSERT INTO  Mas_ChapterContent(ChapterId,SectionId,ChapterTextContents,ChapterImageContents,ChapterVideoContents,ContentType,IsActive) 
		   VALUES (@ChapterId,@SectionId,@ChapterTextContents,@ChapterImageContents,@ChapterVideoContents,@ContentType,1)
	   
		   SET @ChapterContentId =SCOPE_IDENTITY()
	   END
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @ChapterContentId =0
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspInsertLinkSponsorerCoupon]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspInsertLinkSponsorerCoupon]
	@FromCouponId bigint,
	@ToCouponId bigint,
	@SponsorerId bigint,
	@RowCount bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       INSERT INTO  Lnk_CouponDetails(CouponId,SponsorerId) 
	   SELECT mc.CouponId,@SponsorerId FROM Mas_Coupon mc WHERE mc.CouponId BETWEEN @FromCouponId and @ToCouponId
	   SET @RowCount =SCOPE_IDENTITY()
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount =0
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspInsertMedium]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Insert Medium Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspInsertMedium]
	@MediumName nvarchar(100),
	@Description nvarchar(100),
	@MediumId bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       INSERT INTO Mas_Medium(MediumName,Description,IsActive)
	   VALUES(@MediumName,@Description,1)

	   SET @MediumId =SCOPE_IDENTITY()
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @MediumId =0
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspInsertSection]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Insert Medium Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspInsertSection]
	@SectionName nvarchar(100),
	@Description nvarchar(100),
	@IsStandardSection bit=0,
	@IsChapterSection bit =0,
	@SectionId bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       INSERT INTO Mas_Section(SectionName,Description,IsStandardSection,IsChapterSection,IsActive)
	   VALUES(@SectionName,@Description,@IsStandardSection,@IsChapterSection,1)

	   SET @SectionId =SCOPE_IDENTITY()
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @SectionId =0
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspInsertSponsorer]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <07-03-2020>
-- Description:	<Insert Sponsorer Details>
-- =============================================
CREATE PROCEDURE [dbo].[uspInsertSponsorer]
	@SponsorerId bigint,
	@SponsorerName nvarchar(200),
	@ThumbnailImage varbinary(MAX)=null,
	@LandingImage varbinary(MAX)=null,
	@IsActive bit = null,
	@CreatedBy bigint = null,
	@ModifiedBy bigint = null,
	@ReturnId bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       INSERT INTO  Mas_Sponsorer(SponsorerName,ThumbnailImage,LandingImage,IsActive,CreatedBy,ModifiedBy) 
	   VALUES (@SponsorerName,@ThumbnailImage,@LandingImage,@IsActive,@CreatedBy,@ModifiedBy)
	   
	   SET @ReturnId =SCOPE_IDENTITY()
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @ReturnId =0
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspInsertStandard]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Standard Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspInsertStandard]
	@StandardName nvarchar(100),
	@Description nvarchar(100),
	@StandardId bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       INSERT INTO Mas_Standard(StandardName,Description,IsActive)
	   VALUES(@StandardName,@Description,1)
	   SET @StandardId= SCOPE_IDENTITY()
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @StandardId=0;
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspInsertSubject]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspInsertSubject]
	@SubjectName nvarchar(100),
	@SubjectThumbnail nvarchar(500)=null,
	@Description nvarchar(100),
	@SubjectId bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       INSERT INTO Mas_Subject(SubjectName,SubjectThumbnail,Description,IsActive)
	   VALUES(@SubjectName,ISNULL(@SubjectThumbnail,NULL),@Description,1)
	   SET @SubjectId= SCOPE_IDENTITY()
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @SubjectId=0;
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspInsertSubjectContent]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspInsertSubjectContent]
	@StandardId bigint,
	@SubjectId bigint,
	@MediumId bigint,
	@SectionId bigint,
	@SubjectTextContents nvarchar(MAX)=null,
	@SubjectContentId bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY

	   
		IF EXISTS(SELECT SubjectContentId FROM Mas_SubjectContent WHERE StandardId=@StandardId AND SectionId=@SectionId AND SubjectId=@SubjectId AND MediumId=@MediumId)
		BEGIN
			UPDATE Mas_SubjectContent SET SubjectTextContents=@SubjectTextContents,IsActive=1 WHERE 
			StandardId=@StandardId AND SectionId=@SectionId AND SubjectId=@SubjectId AND MediumId=@MediumId

			SET @SubjectContentId =@@ROWCOUNT
		END
		ELSE 
		BEGIN
			INSERT INTO  Mas_SubjectContent(StandardId,SubjectId,MediumId,SectionId,SubjectTextContents,IsActive) 
				VALUES (@StandardId,@SubjectId,@MediumId,@SectionId,@SubjectTextContents,1) 
	   
				SET @SubjectContentId =SCOPE_IDENTITY()
		END
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @SubjectContentId =0
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspInsertSubjectDetails]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspInsertSubjectDetails]
	@StandardId bigint,
	@MediumId bigint,
	@SubjectId bigint,
	@IsActive bit,
	@SubjectDetailsId bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY

	   SELECT @SubjectDetailsId=SubjectDetailsId FROM Lnk_SubjectDetails WHERE  
	   StandardId=@StandardId AND MediumId=@MediumId AND SubjectId=@SubjectId
	   IF(@SubjectDetailsId>0)
	   BEGIN
		   UPDATE Lnk_SubjectDetails SET IsActive=@IsActive WHERE StandardId=@StandardId AND MediumId=@MediumId AND SubjectId=@SubjectId
	   END
	   ELSE
	   BEGIN
		   INSERT INTO Lnk_SubjectDetails(StandardId,MediumId,SubjectId,IsActive)
		   VALUES(@StandardId,@MediumId,@SubjectId,1)
		   SET @SubjectDetailsId= SCOPE_IDENTITY()
	   END
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @SubjectDetailsId=0;
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspInsertUpdateMediumSectionConfig]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<INSERT/UPDATE all Standard Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspInsertUpdateMediumSectionConfig]
	@SectionId bigint,
	@MediumId bigint,
	@IsActive bit,
	@MediumSectionId bigint=0 OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
	   SELECT @MediumSectionId=MediumSectionId FROM Lnk_MediumSections WHERE SectionId=@SectionId AND MediumId=@MediumId

	   IF (@MediumSectionId>0)
	   BEGIN
		UPDATE Lnk_MediumSections SET IsActive=@IsActive WHERE SectionId=@SectionId AND MediumId=@MediumId
	   END
	   ELSE 
	   BEGIN
		INSERT INTO Lnk_MediumSections(SectionId,MediumId,IsActive)
		VALUES(@SectionId,@MediumId,1)
		SET @MediumSectionId= SCOPE_IDENTITY()
	   END
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @MediumSectionId=0;
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspInsertUpdateStandardMediumConfig]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<INSERT/UPDATE all Standard Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspInsertUpdateStandardMediumConfig]
	@StandardId bigint,
	@MediumId bigint,
	@IsActive bit,
	@StandardMediumId bigint=0 OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
	   SELECT @StandardMediumId=StandardMediumId FROM Lnk_StandardMedium WHERE StandardId=@StandardId AND MediumId=@MediumId

	   IF (@StandardMediumId>0)
	   BEGIN
		UPDATE Lnk_StandardMedium SET IsActive=@IsActive WHERE StandardId=@StandardId AND MediumId=@MediumId
	   END
	   ELSE 
	   BEGIN
		INSERT INTO Lnk_StandardMedium(StandardId,MediumId,IsActive)
		VALUES(@StandardId,@MediumId,1)
		SET @StandardMediumId= SCOPE_IDENTITY()
	   END
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @StandardMediumId=0;
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspUpdateChapter]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspUpdateChapter]
	@StandardId bigint,
	@SubjectId bigint,
	@MediumId bigint,
	@ChapterName nvarchar(150),
	@ChapterThumbnail varbinary(MAX)=null,
	@Description nvarchar(100),
	@ChapterId bigint,
	@RowCount int OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       UPDATE Mas_Chapter SET 
	   StandardId=@StandardId,
	   SubjectId=@SubjectId,
	   MediumId=@MediumId,
	   ChapterName=@ChapterName,
	   ChapterThumbnail=@ChapterThumbnail,
	   Description=@Description
	   WHERE 
	   ChapterId=@ChapterId

	   INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
	   VALUES('Inserted',12,'Inserted')

	   SET @RowCount =@@ROWCOUNT
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount =0
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspUpdateMedium]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<UPDATE Medium Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspUpdateMedium]
	@MediumId bigint,
	@MediumName nvarchar(100),
	@Description nvarchar(100),
	@IsActive bit,
	@RowCount int OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       UPDATE Mas_Medium SET 
	   MediumName=@MediumName,
	   Description=@Description,
	   IsActive=@IsActive
	   WHERE MediumId=@MediumId

	   SET @RowCount = @@ROWCOUNT;
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount = 0;
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspUpdateSection]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<UPDATE Medium Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspUpdateSection]
	@SectionId bigint,
	@SectionName nvarchar(100),
	@Description nvarchar(100),
	@IsStandardSection bit=0,
	@IsChapterSection bit =0,
	@IsActive bit,
	@RowCount int OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       UPDATE Mas_Section SET 
	   SectionName=@SectionName,
	   Description=@Description,
	   IsStandardSection=@IsStandardSection,
	   IsChapterSection=@IsChapterSection,
	   IsActive=@IsActive
	   WHERE SectionId=@SectionId

	   SET @RowCount = @@ROWCOUNT;
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount = 0;
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspUpdateSponsorer]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <11-02-2020>
-- Description:	<Update  Sponsorer Details>
-- =============================================
CREATE PROCEDURE [dbo].[uspUpdateSponsorer]
	@SponsorerId bigint,
	@SponsorerName nvarchar(200),
	@ThumbnailImage varbinary(MAX)=null,
	@LandingImage varbinary(MAX)=null,
	@IsActive bit = null,
	@CreatedBy bigint = null,
	@ModifiedBy bigint = null,
	@ReturnId bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       UPDATE Mas_Sponsorer SET 
	   SponsorerName=@SponsorerName,
	   ThumbnailImage=@ThumbnailImage,
	   LandingImage=@LandingImage,
	   IsActive=@IsActive,
	   CreatedBy=@CreatedBy,
	   ModifiedBy=@ModifiedBy,
	   ModifiedDate = GETDATE()
	   WHERE SponsorerId=@SponsorerId

	   SET @ReturnId =@@ROWCOUNT
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @ReturnId =0
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspUpdateStandard]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Standard Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspUpdateStandard]
	@StandardId bigint,
	@StandardName nvarchar(100),
	@Description nvarchar(100),
	@IsActive bit,
	@RowCount int OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       UPDATE Mas_Standard SET 
	   StandardName=@StandardName,
	   Description=@Description,
	   IsActive=@IsActive
	   WHERE StandardId=@StandardId

	   SET @RowCount =@@ROWCOUNT
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount =0
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[uspUpdateSubject]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Update subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspUpdateSubject]
	@SubjectId bigint,
	@SubjectName nvarchar(100),
	@SubjectThumbnail nvarchar(500)=null,
	@Description nvarchar(100),
	@RowCount int OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
	   UPDATE Lnk_SubjectDetails SET IsActive=0 WHERE SubjectId=@SubjectId 
	
       UPDATE Mas_Subject SET 
	   SubjectName=@SubjectName,
	   SubjectThumbnail=ISNULL(@SubjectThumbnail,NULL),
	   Description=@Description
	   WHERE SubjectId=@SubjectId

	   SET @RowCount =@@ROWCOUNT
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount =0
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [Student].[uspChapterList]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [Student].[uspChapterList]
  @SubjectId as BigInt,
  @StandardId as BigInt,
  @MediumId as BigInt
AS
BEGIN
	
	SET NOCOUNT ON;
	IF EXISTS (SELECT  MC.ChapterId  FROM Mas_Chapter MC 
		WHERE MC.StandardId=@StandardId AND MC.SubjectId=@SubjectId AND MC.MediumId=@MediumId)
	BEGIN
		SELECT  MC.ChapterId,MC.ChapterName,MC.ChapterThumbnail AS ChapterBytes,
		MS.SubjectId,MS.SubjectName 
		FROM Mas_Chapter MC LEFT JOIN Mas_Subject MS
		ON MC.SubjectId=MS.SubjectId
		WHERE MC.StandardId=@StandardId AND MC.SubjectId=@SubjectId AND MC.MediumId=@MediumId
	END
END
GO
/****** Object:  StoredProcedure [Student].[uspGetChapterSection]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [Student].[uspGetChapterSection]
  @IsChapterSection as bit,
  @MediumId as BigInt,
  @SubjectId as Bigint,
  @ChapterId as Bigint,
  @IsActive as bit
AS
BEGIN
	
	SET NOCOUNT ON;
	IF EXISTS (SELECT MS.SectionId FROM Mas_Section MS LEFT JOIN Lnk_MediumSections LMS 
				ON MS.SectionId=LMS.SectionId
				WHERE MS.IsChapterSection=@IsChapterSection AND LMS.MediumId=@MediumId AND MS.IsActive=@IsActive)
	BEGIN
		DECLARE @SubjectName AS nvarchar(500)=''
		SELECT @SubjectName=SubjectName FROM Mas_Subject WHERE SubjectId=@SubjectId

		DECLARE @ChapterName AS nvarchar(500)=''
		SELECT @ChapterName=ChapterName FROM Mas_Chapter WHERE ChapterId=@ChapterId

		SELECT MS.SectionId,MS.SectionName,LMS.MediumId ,
		@SubjectName AS SubjectName ,
		@ChapterName AS ChapterName
		FROM Mas_Section MS LEFT JOIN Lnk_MediumSections LMS 
				ON MS.SectionId=LMS.SectionId
				WHERE MS.IsChapterSection=@IsChapterSection AND LMS.MediumId=@MediumId AND MS.IsActive=@IsActive
	END
END

GO
/****** Object:  StoredProcedure [Student].[uspGetmediumList]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [Student].[uspGetmediumList]
AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT MediumId,MediumName FROM Mas_Medium WHERE IsActive=1
END

GO
/****** Object:  StoredProcedure [Student].[uspGetSectionContents]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [Student].[uspGetSectionContents] --1,3
  @ChapterId as BigInt,
  @SectionId as Bigint
AS
BEGIN
	
	SET NOCOUNT ON;
	IF EXISTS (SELECT ChapterContentId FROm Mas_ChapterContent WHERE ChapterId=@ChapterId AND SectionId=@SectionId)
	BEGIN
		SELECT MCC.SectionId,
		MS.SectionName,
		MCC.ContentType,

		MC.ChapterId,
		MC.ChapterName,

		MJ.SubjectId,
		MJ.SubjectName,

		MCC.ChapterImageContents,
		MCC.ChapterVideoContents, 
		MCC.ChapterTextContents,

		MSS.StandardId,
		MSS.StandardName,

		MM.MediumId,
		MM.MediumName

		FROM Mas_ChapterContent MCC LEFT JOIN Mas_Chapter MC 
		ON MCC.ChapterId=MC.ChapterId LEFT JOIN Mas_Section MS 
		ON MCC.SectionId=MS.SectionId LEFT JOIN Mas_Standard MSS
		ON MC.StandardId=MSS.StandardId LEFT JOIN Mas_Medium MM 
		ON MC.MediumId=MM.MediumId LEFT JOIN Mas_Subject MJ
		ON MC.SubjectId=MJ.SubjectId
		WHERE MCC.ChapterId=@ChapterId AND MCC.SectionId=@SectionId 
		AND MCC.IsActive=1
	END
END

GO
/****** Object:  StoredProcedure [Student].[uspGetStandardSection]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [Student].[uspGetStandardSection]
  @IsStandardSection as bit,
  @MediumId as BigInt,
  @SubjectId as Bigint,
  @IsActive as bit
AS
BEGIN
	
	SET NOCOUNT ON;
	IF EXISTS (SELECT MS.SectionId FROM Mas_Section MS LEFT JOIN Lnk_MediumSections LMS 
				ON MS.SectionId=LMS.SectionId
				WHERE MS.IsStandardSection=@IsStandardSection AND LMS.MediumId=@MediumId AND MS.IsActive=@IsActive)
	BEGIN
		DECLARE @SubjectName AS nvarchar(500)=''
		SELECT @SubjectName=SubjectName FROM Mas_Subject WHERE SubjectId=@SubjectId

		SELECT MS.SectionId,MS.SectionName,
		LMS.MediumId,@SubjectName AS SubjectName
		FROM Mas_Section MS LEFT JOIN Lnk_MediumSections LMS 
		ON MS.SectionId=LMS.SectionId
		WHERE MS.IsStandardSection=@IsStandardSection AND LMS.MediumId=@MediumId AND MS.IsActive=@IsActive
	END
END

GO
/****** Object:  StoredProcedure [Student].[uspGetSubjectSectionContents]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [Student].[uspGetSubjectSectionContents] --1,3
  @SubjectId as BigInt,
  @SectionId as Bigint
AS
BEGIN
	
	SET NOCOUNT ON;
	IF EXISTS (SELECT SubjectContentId FROm Mas_SubjectContent WHERE SubjectId=@SubjectId AND SectionId=@SectionId)
	BEGIN
		SELECT MCC.SectionId,
		MS.SectionName,

		MJ.SubjectId,
		MJ.SubjectName,

		MCC.SubjectTextContents,

		MSS.StandardId,
		MSS.StandardName,

		MM.MediumId,
		MM.MediumName

		FROM Mas_SubjectContent MCC LEFT JOIN Mas_Section MS 
		ON MCC.SectionId=MS.SectionId LEFT JOIN Mas_Standard MSS
		ON MCC.StandardId=MSS.StandardId LEFT JOIN Mas_Medium MM 
		ON MCC.MediumId=MM.MediumId LEFT JOIN Mas_Subject MJ
		ON MCC.SubjectId=MJ.SubjectId
		WHERE MCC.SubjectId=@SubjectId AND MCC.SectionId=@SectionId 
		AND MCC.IsActive=1
	END
END

GO
/****** Object:  StoredProcedure [Student].[uspLoginUser]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Insert Medium Information>
-- =============================================
CREATE PROCEDURE [Student].[uspLoginUser] --'9763968153','JayUsha@81185'
	@PhoneNumber nvarchar(100),
	@Password nvarchar(100)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @LoginId AS BIGINT=0
	BEGIN TRY
       SELECT @LoginId=LoginId FROm Mas_Login WHERE PhoneNumber=@PhoneNumber AND Password=@Password

	   IF(@LoginId>0)
	   BEGIN
			SELECT MS.LoginId,ML.FirstName,ML.LastName,MS.StudentId,ML.ExpiryDate,
			MS.MediumId,MM.MediumName,
			MSD.StandardId,MSD.StandardName 
			FROM Mas_Student MS INNER JOIN Mas_Login ML ON MS.LoginId=ML.LoginId 
			LEFT JOIN Mas_Medium MM ON MS.MediumId = MM.MediumId
			LEFT JOIN Mas_Standard MSD ON MS.StandardId = MSD.StandardId
			WHERE ML.LoginId=@LoginId
	   END
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
    END CATCH
END

GO
/****** Object:  StoredProcedure [Student].[uspRegisterUser]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tushar.Harlikar>
-- Create date: <11-02-2020>
-- Description:	<Insert Medium Information>
-- =============================================
CREATE PROCEDURE [Student].[uspRegisterUser]
	@PhoneNumber nvarchar(100),
	@FirstName nvarchar(100),
	@LastName nvarchar(100),
	@Password nvarchar(100),
	@DateOfBirth DateTime,
	@StandardId bigint,
	@MediumId bigint,
	@CouponId bigint,
	@ExpiryDate DateTime
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @LoginId AS BIGINT=0
	DECLARE @StudentId AS BIGINT=0

	BEGIN TRANSACTION 
	BEGIN TRY
       INSERT INTO Mas_Login(PhoneNumber,FirstName,LastName,[Password],DateOfBirth,ExpiryDate)
	   VALUES(@PhoneNumber,@FirstName,@LastName,@Password,@DateOfBirth,@ExpiryDate)

	   SET @LoginId =SCOPE_IDENTITY()
	   IF(@LoginId>0)
	   BEGIN
		 INSERT INTO Mas_Student(LoginId,StandardId,MediumId)
		 VALUES(@LoginId,@StandardId,@MediumId)

		 SET @StudentId =SCOPE_IDENTITY()
	   END
	   COMMIT TRANSACTION ;

	   IF(@LoginId>0 AND @StudentId>0)
	   BEGIN
			
			UPDATE Mas_Coupon SET IsUsed=1 WHERE CouponId=@CouponId

			INSERT INTO Lnk_CouponUser(CouponId,LoginId)
			VALUES(@CouponId,@LoginId)

			SELECT MS.LoginId,ML.FirstName,ML.LastName,MS.StudentId,ML.ExpiryDate,
			MS.MediumId,MM.MediumName,
			MSD.StandardId,MSD.StandardName 
			FROM Mas_Student MS INNER JOIN Mas_Login ML ON MS.LoginId=ML.LoginId 
			LEFT JOIN Mas_Medium MM ON MS.MediumId = MM.MediumId
			LEFT JOIN Mas_Standard MSD ON MS.StandardId = MSD.StandardId
			WHERE ML.LoginId=@LoginId
	   END
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @MediumId =0
		ROLLBACK TRANSACTION ;
    END CATCH
END

GO
/****** Object:  StoredProcedure [Student].[uspSubjectList]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [Student].[uspSubjectList] --1
  @LoginId as BigInt
AS
BEGIN
	
	SET NOCOUNT ON;
	IF EXISTS (SELECT LoginId FROM Mas_Student WHERE LoginId=@LoginId)
	BEGIN
		SELECT  MSJ.SubjectId,MSJ.SubjectName,MSJ.SubjectThumbnail AS SubjectThumbnailPath FROM Mas_Login ML LEFT JOIN Mas_Student MS 
		ON ML.LoginId =MS.LoginId LEFT JOIN Mas_Standard MST
		ON MST.StandardId=MS.StandardId LEFT JOIN Mas_Medium MM
		ON MS.MediumId =MM.MediumId LEFT JOIN Lnk_SubjectDetails LSD
		ON MST.StandardId=LSD.StandardId AND MM.MediumId=LSD.MediumId LEFT JOIN Mas_Subject MSJ
		ON LSD.SubjectId=MSJ.SubjectId 
		WHERE ML.LoginId=@LoginId
	END
END

GO
/****** Object:  StoredProcedure [Student].[uspValidateCoupon]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [Student].[uspValidateCoupon]
  @CouponCode nvarchar(100)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT  CouponId,CouponCode FROM Mas_Coupon WHERE CouponCode=@CouponCode AND IsActive=1 AND IsUsed=0
END

GO
/****** Object:  StoredProcedure [Student].[uspValidateMobileNumber]    Script Date: 18-04-2020 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [Student].[uspValidateMobileNumber]
  @PhoneNumber nvarchar(100),
  @IsDuplicatePhoneNumber bit OUT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @DuplicateMobile AS nvarchar(100)=''
	SET @IsDuplicatePhoneNumber=0
	IF EXISTS(SELECT LoginId FROM Mas_Login WHERE PhoneNumber=@PhoneNumber)
	BEGIN
		SET @IsDuplicatePhoneNumber=1
	END
END

GO
USE [master]
GO
ALTER DATABASE [PrismSolution] SET  READ_WRITE 
GO
