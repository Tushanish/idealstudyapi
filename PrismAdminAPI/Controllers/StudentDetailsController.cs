﻿using PrismAdminAPI.Manager;
using PrismAdminAPI.Manager.StudentDetails;
using PrismAdminAPI.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Mvc;




namespace PrismAdminAPI.Controllers
{
    public class StudentDetailsController : Controller
    {

        private readonly IStudentDetailsManager _studentDetailsManager;
        private readonly IMediumManager _mediumManager;

        public StudentDetailsController(IStudentDetailsManager studentDetailsManager, IMediumManager mediumManager)
        {
            _studentDetailsManager = studentDetailsManager;
            _mediumManager = mediumManager;
        }

        /// <summary>
        /// get all student Details
        /// </summary>
        /// <returns></returns>
        [Route("studentDetails")]
        [Route("StudentDetails/ListStudentDetails")]
        public ActionResult ListStudentDetails()
        {
            //IEnumerable<StudentDetail> _studentDetails = _studentDetailsManager.GetStudentDetails(0,true).Take(1000);
            //IEnumerable<StudentDetail> _studentDetailsFiltered = _studentDetails;
            return View();
        }

        [HttpPost]
        public JsonResult FilterStudentDetails(bool used, long fromCouponId, long toCouponId)
        {
            return Json(GetStudentData(used,fromCouponId,toCouponId).Data, JsonRequestBehavior.AllowGet);
        }


        private JsonResult GetStudentData(bool usedCoupons,long fromCouponId, long toCouponId)
        {
            IEnumerable<StudentDetail> filteredData;
            long totalRecords = 0;
            
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Convert.ToInt32(Request["start"]);
                var length = Convert.ToInt32(Request["length"]);
                var searchvalue = Request["search[value]"];
                var sortcolumn = Request["columns[" + Request["order[0]column"] + "][name]"];
                string sortDirection = Request["order[0][dir]"];
                
              
                if (string.IsNullOrEmpty(searchvalue) && fromCouponId == 0)
                {
                    filteredData = _studentDetailsManager.GetStudentDetails(0, usedCoupons, out long totalrecords, start, length);

                    //{
                    //    filteredData = filteredData.Where(x => !string.IsNullOrEmpty(x.FirstName) && x.FirstName.ToLower().Contains(searchvalue.ToLower())
                    //                            || !string.IsNullOrEmpty(x.LastName) && x.LastName.ToLower().Contains(searchvalue.ToLower())
                    //                            || !string.IsNullOrEmpty(x.MediumName) && x.MediumName.ToLower().Contains(searchvalue.ToLower())
                    //                            || !string.IsNullOrEmpty(x.SponsorerName) && x.SponsorerName.ToLower().Contains(searchvalue.ToLower())
                    //                            || !string.IsNullOrEmpty(x.CouponCode) && x.CouponCode.ToLower().Contains(searchvalue.ToLower())
                    //                            || !string.IsNullOrEmpty(x.Password) && x.Password.ToLower().Contains(searchvalue.ToLower())
                    //                            || !string.IsNullOrEmpty(x.FatherName) && x.FatherName.ToLower().Contains(searchvalue.ToLower())
                    //                            || !string.IsNullOrEmpty(x.DateOfBirth) && x.DateOfBirth.ToLower().Contains(searchvalue.ToLower())
                    //                            || !string.IsNullOrEmpty(x.PhoneNumber) && x.PhoneNumber.ToLower().Contains(searchvalue.ToLower())).ToList<StudentDetail>();
                    //}
                    totalRecords = totalrecords;
                }
                else 
                {
                    filteredData = _studentDetailsManager.FilterStudentDetails(searchvalue, usedCoupons,fromCouponId,toCouponId);
                    totalRecords = (long)filteredData?.Count();

                }

                if (sortDirection == "asc")
                {
                    if (sortcolumn == "CouponCode")
                        filteredData = filteredData.OrderBy(x => x.CouponCode).ToList<StudentDetail>();
                    else if (sortcolumn == "FirstName")
                        filteredData = filteredData.OrderBy(x => x.FirstName).ToList<StudentDetail>();
                    else if (sortcolumn == "PhoneNumber")
                        filteredData = filteredData.OrderBy(x => x.PhoneNumber).ToList<StudentDetail>();
                    else if (sortcolumn == "LastName")
                        filteredData = filteredData.OrderBy(x => x.LastName).ToList<StudentDetail>();
                    else if (sortcolumn == "SponsorerName")
                        filteredData = filteredData.OrderBy(x => x.SponsorerName).ToList<StudentDetail>();
                }

                if (sortDirection == "desc")
                {
                    if (sortcolumn == "CouponCode")
                        filteredData = filteredData.OrderByDescending(x => x.CouponCode).ToList<StudentDetail>();
                    else if (sortcolumn == "FirstName")
                        filteredData = filteredData.OrderByDescending(x => x.FirstName).ToList<StudentDetail>();
                    else if (sortcolumn == "PhoneNumber")
                        filteredData = filteredData.OrderByDescending(x => x.PhoneNumber).ToList<StudentDetail>();
                    else if (sortcolumn == "LastName")
                        filteredData = filteredData.OrderByDescending(x => x.LastName).ToList<StudentDetail>();
                    else if (sortcolumn == "SponsorerName")
                        filteredData = filteredData.OrderByDescending(x => x.SponsorerName).ToList<StudentDetail>();
                }
                return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = filteredData });

        }

        [HttpGet]
        public JsonResult GetUnusedCoupons(bool used)
        {
            return Json(GetStudentData(used,0,0).Data, JsonRequestBehavior.AllowGet); 
        }

        //[HttpGet]
        //public JsonResult GetFromToCouponData(long fromCoupon, long toCoupon, bool usedCoupons)
        //{
        //    return Json(GetStudentData(usedCoupons, 0, 0).Data, JsonRequestBehavior.AllowGet);
        //}

        /// <summary>
        /// Update Password
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("editPassword/{loginId}")]
        [Route("StudentDetails/EditPassword/{loginId}")]
        public ActionResult EditPassword(StudentDetail detail)
        {
            if (detail.LoginId == 0)
            {
                return View(detail);
            }
            else
            {
                var studentDetail = _studentDetailsManager.GetStudentDetails(detail.LoginId, true, out long totalrecords, 0, 0).FirstOrDefault();
                studentDetail.MediumList = new List<SelectListItem> { };
                var mediumList = _mediumManager.GetMediumList(0, true);
                foreach (var medium in mediumList)
                {
                    studentDetail.MediumList.Add(new SelectListItem { Text = medium.MediumName, Value = medium.MediumId.ToString(), Selected = medium.MediumId == studentDetail.MediumId });
                }

                return View(studentDetail);
            }
        }

        [HttpPost]
        public ActionResult UpdatePassword(StudentDetail detail)
        {
            var studentDetail = _studentDetailsManager.UpdatePassword(detail.LoginId, detail.Password, detail.PhoneNumber, detail.FirstName, detail.LastName, int.Parse(detail.MediumId.ToString()));
            return RedirectToAction("ListStudentDetails");
        }


    }
}