﻿using PrismAdminAPI.Manager;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Controllers
{
    public class ChapterController : Controller
    {
        private readonly IChapterManager chapterManager;
        private readonly IStandardManager standardManager;
        private readonly IConfigurationManager configurationManager;
        public ChapterController(IChapterManager _chapterManager, 
            IStandardManager _standardManager,
            IConfigurationManager _configurationManager)
        {
            chapterManager = _chapterManager;
            standardManager = _standardManager;
            configurationManager = _configurationManager;
        }

        /// <summary>
        /// get all standard list 
        /// </summary>
        /// <returns></returns>
        [Route("chapterlist")]
        [Route("Chapter/ListChapter")]
        public ActionResult ListChapter()
        {
            IEnumerable<Chapter> _chapterList = chapterManager.GetChapterList();
            return View(_chapterList);
        }

        /// <summary>
        /// Add Standard
        /// </summary>
        /// <returns></returns>
        [Route("addchapter")]
        [Route("Chapter/AddChapter")]
        public ActionResult AddChapter()
        {
            Chapter _chapter = new Chapter();
            _chapter = GetChapterObjectDetails(_chapter);
            return View(_chapter);
        }

        [HttpPost]
        public ActionResult InsertChapter(Chapter _chapter)
        {
            long addResult = chapterManager.InsertChapter(_chapter);
            return RedirectToAction("ListChapter");
        }

        [Route("editchapter/{chapterId}")]
        [Route("Chapter/EditChapter/{chapterId}")]
        public ActionResult EditChapter(long chapterId)
        {
            Chapter _chapter = chapterManager.GetChapterList(chapterId).FirstOrDefault();
            if (_chapter== null)
            {
                return View("ListChapter");
            }
            else
            {
                _chapter = GetChapterObjectDetails(_chapter);
            }
            return View(_chapter);
        }

        [HttpPost]
        public ActionResult UpdateChapter(Chapter _chapter)
        {
            long updateResult = chapterManager.UpdateChapter(_chapter);
            return RedirectToAction("ListChapter");
        }

        [NonAction]
        public Chapter GetChapterObjectDetails(Chapter _chapter)
        {
            _chapter.StandardList = standardManager.GetStandardList(null, true).Select(x => new SelectListItem { Text = String.Format("{0} ({1})", x.StandardName, x.Description), Value = x.StandardId.ToString() });
            return _chapter;
        }

        [HttpGet]
        public JsonResult GetMediumsByStandardId(long standardId)
        {
            var _mediumList = configurationManager.GetConfiguredStandardMediamConfiguration(standardId)
                .Select(x => new SelectListItem { Text = x.MediumName, Value = x.MediumId.ToString() });
            
            return Json(_mediumList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSubjectList(long standardId, long mediumId)
        {
            var _mediumList = configurationManager.GetConfiguredSubjectList(standardId,mediumId)
                .Select(x => new SelectListItem { Text = x.SubjectName, Value = x.SubjectId.ToString() });

            return Json(_mediumList, JsonRequestBehavior.AllowGet);
        }
    }
}