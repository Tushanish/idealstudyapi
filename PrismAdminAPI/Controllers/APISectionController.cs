﻿using PrismAdminAPI.Manager;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PrismAdminAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("section")]
    public class APISectionController : ApiController
    {
        private IStudentAPIManager studentAPIManager = null;
        public APISectionController(IStudentAPIManager _studentAPIManager)
        {
            studentAPIManager = _studentAPIManager;
        }

        [HttpGet]
        [Route("getChapterSectionList/{isChapterSection}/{mediumId}/{isActive}/{subjectId}/{chapterId}")]
        [AllowAnonymous]
        public IEnumerable<SectionMediumViewModel> GetChapterSectionList(bool isChapterSection, long mediumId, bool isActive,long subjectId,long chapterId)
        {
            return studentAPIManager.GetChapterSection(isChapterSection, mediumId, isActive, subjectId, chapterId);
        }

        [HttpGet]
        [Route("getStandardSectionList/{isStandardSection}/{subjectId}/{mediumId}/{isActive}")]
        [AllowAnonymous]
        public IEnumerable<SectionMediumViewModel> GetStandardSectionList(bool isStandardSection,long subjectId, long mediumId, bool isActive)
        {
            return studentAPIManager.GetStandardSection(isStandardSection, subjectId, mediumId, isActive);
        }
    }
}
