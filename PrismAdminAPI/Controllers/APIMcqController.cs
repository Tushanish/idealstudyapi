﻿using PrismAdminAPI.Manager.McqAPIManager;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PrismAdminAPI.Controllers
{
    [RoutePrefix("apiMcq")]
    public class APIMcqController : ApiController
    {
        private readonly IMcqAPIManager _mcqAPIManager = null;
        public APIMcqController(IMcqAPIManager mcqAPIManager)
        {
            _mcqAPIManager = mcqAPIManager;
        }

        [HttpGet]
        [Route("getApiQuestions/{chapterId}")]
        [AllowAnonymous]
        public List<ApiQuestion> GetApiQuestions(int chapterId)
        {
            try
            {
                var apiQuestions = _mcqAPIManager.GetApiQuestions(chapterId);
                if (apiQuestions.Count() > 0)
                {
                    return apiQuestions;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpGet]
        [Route("getApiAnswers/{chapterId}")]
        [AllowAnonymous]
        public List<ApiAnswer> GetApiAnswers(int chapterId)
        {
            try
            {
                var apiAnswers = _mcqAPIManager.GetApiAnswer(chapterId);
                if (apiAnswers.Count() > 0)
                {
                    return  apiAnswers;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
