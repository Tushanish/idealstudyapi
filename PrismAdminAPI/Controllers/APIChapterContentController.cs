﻿using PrismAdminAPI.Manager;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PrismAdminAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("chaptercontent")]
    public class APIChapterContentController : ApiController
    {
        private IStudentAPIManager studentAPIManager = null;
        public APIChapterContentController(IStudentAPIManager _studentAPIManager)
        {
            studentAPIManager = _studentAPIManager;
        }

        [HttpGet]
        [Route("getchaptersectioncontent/{chapterId}/{sectionId}")]
        [AllowAnonymous]
        public IEnumerable<ChapterContentViewModel> GetChapterSectioncContent(long chapterId, long sectionId)
        {
            return studentAPIManager.GetChapterContentForSection(chapterId, sectionId);
        }
    }
}
