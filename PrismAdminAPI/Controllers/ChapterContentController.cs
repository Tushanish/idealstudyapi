﻿using PrismAdminAPI.BAL;
using PrismAdminAPI.Manager;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Controllers
{
    public class ChapterContentController : Controller
    {
        private readonly IChapterManager chapterManager;
        private readonly IStandardManager standardManager;
        private readonly IConfigurationManager configurationManager;
        private readonly ISectionManager sectionManager;
        private readonly IChapterContentManager chapterContentManager;
        public ChapterContentController(IChapterManager _chapterManager,
            IStandardManager _standardManager,
            IConfigurationManager _configurationManager,
            ISectionManager _sectionManager,
            IChapterContentManager _chapterContentManager)
        {
            chapterManager = _chapterManager;
            standardManager = _standardManager;
            configurationManager = _configurationManager;
            sectionManager = _sectionManager;
            chapterContentManager = _chapterContentManager;
        }

        [Route("getchaptercontent")]
        [Route("ChapterContent/ListChapterContent")]
        public ActionResult ListChapterContent()
        {
            ChapterContent chapterContent = new ChapterContent();
            chapterContent = GetChapterContentObjectDetails(chapterContent);
            return View(chapterContent);
        }

       

        [HttpPost]
        public ActionResult GetChapterContent(ChapterContent _chapterContent)
        {
            var _chapterContentList = chapterContentManager.GetChapterContent(_chapterContent.StandardId,
                _chapterContent.MediumId, _chapterContent.ChapterId, _chapterContent.SectionId, _chapterContent.SubjectId);
            return PartialView("_ListChapterContent", _chapterContentList);
        }

        [HttpGet]
        public ActionResult GetChapterContentOnSection(ChapterContent _chapterContent)
        {
            var _chapterContentList = chapterContentManager.GetChapterContent(_chapterContent.StandardId,
                _chapterContent.MediumId, _chapterContent.ChapterId, _chapterContent.SectionId, _chapterContent.SubjectId, false);
            return Json(_chapterContentList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetChapterContentOnSectionPrint(ChapterContent _chapterContent)
        {
            var _chapterContentList = chapterContentManager.GetChapterContent(_chapterContent.StandardId,
                _chapterContent.MediumId, _chapterContent.ChapterId, _chapterContent.SectionId, _chapterContent.SubjectId, false);
            return PartialView("_ViewChapterPrint", _chapterContentList.FirstOrDefault());
        }

        [HttpPost]
        public ActionResult DeleteChapterContent(long _chapterContentId)
        {
            bool isDeleted = chapterContentManager.DeleteChapterContent(_chapterContentId);
            return Json(isDeleted);
        }

        [Route("addchaptercontent")]
        [Route("ChapterContent/AddChapterContent")]
        public ActionResult AddChapterContent()
        {
            ChapterContent _chapterContent = new ChapterContent();
            _chapterContent = GetChapterContentObjectDetails(_chapterContent);
            return View(_chapterContent);
        }

        [NonAction]
        public ChapterContent GetChapterContentObjectDetails(ChapterContent _chapterContent)
        {
            _chapterContent.StandardList = standardManager.GetStandardList(null, true).Select(x => new SelectListItem { Text = String.Format("{0} ({1})", x.StandardName, x.Description), Value = x.StandardId.ToString() });
            return _chapterContent;
        }

        [HttpGet]
        public JsonResult GetMediumsByStandardId(long standardId)
        {
            var _mediumList = configurationManager.GetConfiguredStandardMediamConfiguration(standardId)
                .Select(x => new SelectListItem { Text = x.MediumName, Value = x.MediumId.ToString() });

            return Json(_mediumList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSubjectsByStandardAndMedium(long standardId, long mediumId)
        {
            var _mediumList = configurationManager.GetConfiguredSubjectList(standardId, mediumId)
                .Select(x => new SelectListItem { Text = x.SubjectName, Value = x.SubjectId.ToString() });

            return Json(_mediumList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetChaptersByStandardAndMedium(long standardId, long mediumId, long subjectId)
        {
            var _chapterList = chapterManager.GetChapterListByStandardId(standardId, mediumId, subjectId)
                .Select(x => new SelectListItem { Text = x.ChapterName, Value = x.ChapterId.ToString() });

            return Json(_chapterList, JsonRequestBehavior.AllowGet);
        }



        [HttpGet]
        public JsonResult GetSectionsByChapterId(long chapterId, long mediumId)
        {
            var _sectionList = sectionManager.GetSectionsByChapterId(chapterId, mediumId)
                .Select(x => new SelectListItem { Text = x.SectionName, Value = x.SectionId.ToString() });

            return Json(_sectionList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InsertChapterContent(ChapterContent _chapterContent)
        {
            if (ModelState.IsValid)
            {
                long result = chapterContentManager.InsertChapterContent(_chapterContent);

                 return RedirectToAction("ListChapterContent");
            }
            else
            {
                return View("AddChapterContent");
            }
        }

        [HttpPost]
        public ActionResult InsertChapterContentText(ChapterContentViewModel _ChapterContentViewModel)
        {
            if (ModelState.IsValid)
            {
                ChapterContent _chapterContent = new ChapterContent();
                _chapterContent.StandardId = _ChapterContentViewModel.StandardId;
                _chapterContent.MediumId = _ChapterContentViewModel.MediumId;
                _chapterContent.SubjectId = _ChapterContentViewModel.SubjectId;
                _chapterContent.ChapterId = _ChapterContentViewModel.ChapterId;
                _chapterContent.SectionId = _ChapterContentViewModel.SectionId;
                _chapterContent.ChapterTextContents = _ChapterContentViewModel.ChapterTextContents;
                _chapterContent.ContentType = ChapterContentType.Text;
                _chapterContent.ContentTypeString = ((int)ChapterContentType.Text).ToString();
                long result = chapterContentManager.InsertChapterContent(_chapterContent);
                return RedirectToAction("ListChapterContent");
            }
            else
            {
                return View("AddChapterContent");
            }
        }


        [HttpPost]
        public ActionResult GetChapterContentListView(long standardId, long mediumId, long subjectId, long chapterId, long sectionId)
        {
            var chapterContentViewModels = chapterContentManager.
                                           GetChapterContent(standardId, mediumId, chapterId, sectionId, subjectId, false);

            return PartialView("_ListChapterContent", chapterContentViewModels);
        }

        [HttpPost]
        public ActionResult ViewChapterContent(ChapterContentViewModel model)
        {
            var _chapterContentList = chapterContentManager.GetChapterContent(model.StandardId,
                model.MediumId, model.ChapterId, model.SectionId, model.SubjectId, false).FirstOrDefault();
            return View(_chapterContentList);
        }

        [HttpPost]
        public JsonResult DisableSection(long chapterId, long sectionId, bool isDisabled)
        {
            var result = chapterContentManager.EnableDisableChapterSection(chapterId, sectionId, isDisabled);
            return Json(result);
        }
    }
}