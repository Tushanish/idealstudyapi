﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PrismAdminAPI.Models;
using PrismAdminAPI.Manager;
using System.Web.Http.Cors;

namespace PrismAdminAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("medium")]
    public class APIMediumController : ApiController
    {
        private IStudentAPIManager studentAPIManager=null;
        public APIMediumController(IStudentAPIManager _studentAPIManager)
        {
            studentAPIManager = _studentAPIManager;
        }

        [HttpGet]
        [Route("getmediumlist")]
        [AllowAnonymous]
        public IEnumerable<Medium> GetMediumList()
        {
            return studentAPIManager.GetMediumList();
        }

    }
}