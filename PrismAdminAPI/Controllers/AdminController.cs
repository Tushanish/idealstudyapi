﻿using PrismAdminAPI.BAL;
using PrismAdminAPI.Manager;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Controllers
{
    public class AdminController : Controller
    {

        private readonly IAdminManager _adminManager;

        public AdminController(IAdminManager adminManager)
        {
            _adminManager = adminManager;
        }

        /// <summary>
        /// Add new admin.
        /// </summary>
        /// <returns>Add new admin</returns>
        [Route("addAdmin")]
        [Route("Admin/AddAdmin")]
        public ActionResult AddAdmin()
        {
            LnkAdminRole lnkAdminRole = new LnkAdminRole
            {
                Admin = new Admin { },
                Roles = Helper.ToSelectListItems(_adminManager.GetRolesByRoleId(0).ToDictionary(x => x.RoleId, x => x.RoleName))
            };
            return View(lnkAdminRole);
        }

        /// <summary>
        /// Get all admin details list 
        /// </summary>
        /// <returns>List of admin details</returns>
        [Route("adminlist")]
        [Route("Admin/ListAdminDetails")]
        public ActionResult ListAdmin()
        {
            var adminList = _adminManager.GetAdminListByAdminId(0);
            return View(adminList);
        }

        [HttpPost]
        public ActionResult InsertAdmin(LnkAdminRole _admin)
        {
            var _adminDetails = _admin.Admin ?? new Admin();
            bool addResult = _adminManager.InsertAdmin(_adminDetails);
            return RedirectToAction("ListAdmin");
        }

        [Route("editadmin/{adminId}")]
        [Route("Admin/EditAdmin/{adminId}")]
        public ActionResult EditAdmin(long adminId)
        {
            Admin _admin = _adminManager.GetAdminListByAdminId(adminId).FirstOrDefault();
            if (_admin == null)
            {
                return View("ListAdmin");
            }

            return View(_admin);
        }

        [HttpPost]
        public ActionResult UpdateAdmin(Admin _admin)
        {
            bool updateResult = _adminManager.UpdateAdmin(_admin);
            return RedirectToAction("ListAdmin");
        }

        /// <summary>
        /// Activate or Deactivate the admin
        /// </summary>
        /// <param name="_admin">Admin instance</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ActiveDeactivateAdmin(Admin _admin)
        {
            bool _activateDeactivateResult = _adminManager.ActivateDeactivateAdmin(_admin.AdminId, _admin.IsActive);
            return Json(_activateDeactivateResult);
        }
    }
}