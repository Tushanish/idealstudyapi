﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PrismAdminAPI.Models;
using PrismAdminAPI.Manager;
using System.Web.Http.Cors;
using PrismAdminAPI.ViewModel;

namespace PrismAdminAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("subject")]
    public class APISubjectController : ApiController
    {
        private IStudentAPIManager studentAPIManager = null;
        public APISubjectController(IStudentAPIManager _studentAPIManager)
        {
            studentAPIManager = _studentAPIManager;
        }

        [HttpGet]
        [Route("getsubjectlist/{loginId}")]
        [AllowAnonymous]
        public HttpResponseMessage GetSubjectList(long loginId)
        {
            try
            {
                var subjectList = studentAPIManager.GetSubjectList(loginId);
                if (subjectList.Count() > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, subjectList);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.ToString());
            }
        }
    }
}