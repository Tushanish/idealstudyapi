﻿using PrismAdminAPI.Manager;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Controllers
{
    public class SubjectContentController : Controller
    {
        private readonly IStandardManager standardManager;
        private readonly IConfigurationManager configurationManager;
        private readonly ISectionManager sectionManager;
        private readonly ISubjectContentManager subjectContentManager;
        public SubjectContentController(
            IStandardManager _standardManager,
            IConfigurationManager _configurationManager,
            ISectionManager _sectionManager,
            ISubjectContentManager _subjectContentManager)
        {
            standardManager = _standardManager;
            configurationManager = _configurationManager;
            sectionManager = _sectionManager;
            subjectContentManager = _subjectContentManager;
        }

        // GET: StandardContent
        [Route("getsubjectcontent")]
        [Route("SubjectContent/ListSubjectContent")]
        public ActionResult ListSubjectContent()
        {
            SubjectContent _subjectContent = new SubjectContent();
            _subjectContent = GetSubjectContentObjectDetails(_subjectContent);
            return View(_subjectContent);
        }

        [HttpPost]
        public ActionResult GetSubjectContent(SubjectContent _subjectContent)
        {
            var _subjectContentList = subjectContentManager.GetSubjectContent(_subjectContent.StandardId,
                _subjectContent.MediumId, _subjectContent.SubjectId, _subjectContent.SectionId);
            return PartialView("_ListSubjectContent", _subjectContentList);
        }

        [Route("addsubjectcontent")]
        [Route("SubjectContent/AddSubjectContent")]
        public ActionResult AddSubjectContent()
        {
            SubjectContent _subjectContent = new SubjectContent();
            _subjectContent = GetSubjectContentObjectDetails(_subjectContent);
            return View(_subjectContent);
        }

        [NonAction]
        public SubjectContent GetSubjectContentObjectDetails(SubjectContent _standardContent)
        {
            _standardContent.StandardList = standardManager.GetStandardList(null, true).Select(x => new SelectListItem { Text = String.Format("{0} ({1})", x.StandardName, x.Description), Value = x.StandardId.ToString() });
            return _standardContent;
        }

        [HttpGet]
        public JsonResult GetMediumsByStandardId(long standardId)
        {
            var _mediumList = configurationManager.GetConfiguredStandardMediamConfiguration(standardId)
                .Select(x => new SelectListItem { Text = x.MediumName, Value = x.MediumId.ToString() });
            return Json(_mediumList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSubjectsByStandardAndMedium(long standardId, long mediumId)
        {
            var _mediumList = configurationManager.GetConfiguredSubjectList(standardId, mediumId)
                .Select(x => new SelectListItem { Text = x.SubjectName, Value = x.SubjectId.ToString() });

            return Json(_mediumList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetStandardSections()
        {
            var _sectionList=sectionManager.GetSectionList(null,true).Where(x=>x.IsStandardSection==true)
                        .Select(x => new SelectListItem { Text = String.Format("{0}", x.SectionName), Value = x.SectionId.ToString() });
            return Json(_sectionList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InsertSubjectContent(SubjectContent _subjectContent)
        {
            if (ModelState.IsValid)
            {
                long result = subjectContentManager.InsertSubjectContent(_subjectContent);
                return RedirectToAction("ListSubjectContent");
            }
            else
            {
                return View("AddSubjectContent");
            }
        }

        [HttpGet]
        public ActionResult GetSubjectContentOnSection(SubjectContent _subjectContent)
        {
            var _subjectContentList = subjectContentManager.GetSubjectContent(_subjectContent.StandardId,
                 _subjectContent.MediumId, _subjectContent.SubjectId, _subjectContent.SectionId).FirstOrDefault();
            return Json(_subjectContentList, JsonRequestBehavior.AllowGet);
        }
    }
}