﻿using PrismAdminAPI.BAL;
using PrismAdminAPI.Manager;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Controllers
{
    public class McqController : Controller
    {

        private readonly IStandardManager _standardManager;
        private readonly IMediumManager _mediumManager;
        private readonly ISubjectManager _subjectManager;
        private readonly IChapterManager _chapterManager;
        private readonly ISectionManager _sectionManager;
        private readonly IMcqManager _mcqManager;
        private readonly IConfigurationManager _configurationManager;


        public McqController(IStandardManager standardManager,
                             IMediumManager mediumManager,
                             ISubjectManager subjectManager,
                             IChapterManager chapterManager,
                             ISectionManager sectionManager,
                             IMcqManager mcqManager,
                             IConfigurationManager configurationManager)
        {
            _standardManager = standardManager;
            _mediumManager = mediumManager;
            _subjectManager = subjectManager;
            _chapterManager = chapterManager;
            _sectionManager = sectionManager;
            _mcqManager = mcqManager;
            _configurationManager = configurationManager;
        }

        /// <summary>
        /// List of MCQs
        /// </summary>
        /// <returns>List of Mcq.</returns>
        [Route("mcqlist")]
        [Route("MCQ/ListMcq")]
        public ActionResult ListMcq()
        {
            var lnkQuestions = GetMcq(0, 0, 0, 0, 0);
            return View();
        }


        /// <summary>
        /// Add MCQs..
        /// </summary>
        /// <returns>List of Mcq.</returns>
        [Route("addmcq")]
        [Route("MCQ/AddMcq")]
        public ActionResult AddMcq()
        {
            //var lnkQuestions = _mcqManager.GetFilteredMcq(0, 0, 0, 0, 0,false);
            var lnkQuestions = PopulateData(0,0,0,0,0);
            AddDropDown(lnkQuestions);
            return View(lnkQuestions);
        }

        /// <summary>
        /// Get MCQs..
        /// </summary>
        /// <returns>List of Mcq.</returns>
        [Route("getmcq")]
        [Route("MCQ/GetMcq")]
        [HttpGet]
        public ActionResult GetMcq(long? standardId, long? mediumId, long? subjectId, long? chapterId, long? sectionId)
        {
            if (chapterId != null && sectionId != null)
            {
                var lnkQuestions = _mcqManager.GetFilteredMcq(standardId, mediumId, subjectId, chapterId, sectionId, false);
                if (lnkQuestions.Questions != null && lnkQuestions.Questions.Count == 0)
                {
                    lnkQuestions = PopulateData();
                }
                return PartialView("_McqPartial", lnkQuestions);
            }
            return PartialView("_McqPartial", PopulateData());
        }

        [Route("MCQ/GetMcqAfterSubmit")]
        [HttpGet]
        public ActionResult GetMcqAfterSubmit(long? standardId, long? mediumId, long? subjectId, long? chapterId, long? sectionId)
        {
            var lnkQuestions = _mcqManager.GetFilteredMcq(standardId, mediumId, subjectId, chapterId, sectionId, true);
            return PartialView("_McqPartial", lnkQuestions);
        }

        [NonAction]
        private LnkQuestionChapterViewModel PopulateData(long? standardId, long? mediumId, long? subjectId, long? chapterId, long? sectionId)
        {
            return new LnkQuestionChapterViewModel
            {
                ChapterId = chapterId,
                MediumId = mediumId,
                SectionId = sectionId,
                SubjectId = subjectId,
                StandardId = standardId,
                Questions = new List<Question>
                {
                    new Question
                    {
                        QuestionId = 0,
                        Answers = new List<Answer>()
                                {
                                    new Answer { AnswerNo = 0,AnswerId = 0, AnswerName = "" , IsCorrect = false , QuestionNo = 0},
                                },
                        QuestionName = "Add Your Question here",
                        QuestionNo = 0
                    }
                }
            };
        }

        [NonAction]
        private LnkQuestionChapterViewModel PopulateData()
        {
            LnkQuestionChapterViewModel lnkQuestionChapter = new LnkQuestionChapterViewModel();

            var standardlist = _standardManager.GetStandardList().Select(x => new { x.StandardId, x.StandardName })
                                .Distinct().ToDictionary(x => x.StandardId, x => x.StandardName);

            var mediumlist = _mediumManager.GetMediumList().Select(x => new { x.MediumId, x.MediumName }).Distinct().ToDictionary(x => x.MediumId, x => x.MediumName);

            var subjectlist = _subjectManager.GetSubjectList().Select(x => new { x.SubjectId, x.SubjectName }).Distinct().ToDictionary(x => x.SubjectId, x => x.SubjectName);

            var chapterlist = _chapterManager.GetChapterList().Select(x => new { x.ChapterId, x.ChapterName }).Distinct().ToDictionary(x => x.ChapterId, x => x.ChapterName);

            var sectionlist = _sectionManager.GetSectionList().Select(x => new { x.SectionId, x.SectionName }).Distinct().ToDictionary(x => x.SectionId, x => x.SectionName);

            lnkQuestionChapter.StandardList = Helper.ToSelectListItems(standardlist);

            lnkQuestionChapter.MediumList = Helper.ToSelectListItems(mediumlist);

            lnkQuestionChapter.SubjectList = Helper.ToSelectListItems(subjectlist);

            lnkQuestionChapter.ChapterList = Helper.ToSelectListItems(chapterlist);

            lnkQuestionChapter.SectionList = Helper.ToSelectListItems(sectionlist);

            lnkQuestionChapter.Questions = new List<Question>()
            {
                new Question
                {
                            QuestionId = 1,
                            Answers = new List<Answer>()
                            {
                                new Answer { AnswerId = 1 ,AnswerNo = 1 , AnswerName = "abcd" , IsCorrect = true , QuestionNo = 1 },
                                new Answer { AnswerId = 2,AnswerNo = 2 , AnswerName = "213" , IsCorrect = false , QuestionNo = 2 },
                                new Answer { AnswerId = 3,AnswerNo = 3 , AnswerName = "###" , IsCorrect = false , QuestionNo = 3 },
                                new Answer { AnswerId = 4,AnswerNo = 4 , AnswerName = "+++" , IsCorrect = false , QuestionNo = 4 }

                            },
                            QuestionName = "Which of the followings are alphabets?",
                            QuestionNo = 1
                }
            };

            return lnkQuestionChapter;
        }

        private void AddDropDown(LnkQuestionChapterViewModel lnkQuestions)
        {

            var standardlist = _standardManager.GetStandardList().Select(x => new { x.StandardId, x.StandardName })
                                .Distinct().ToDictionary(x => x.StandardId, x => x.StandardName);

            //var mediumlist = _mediumManager.GetMediumList().Select(x => new { x.MediumId, x.MediumName }).Distinct().ToDictionary(x => x.MediumId, x => x.MediumName);

            //var subjectlist = _subjectManager.GetSubjectList().Select(x => new { x.SubjectId, x.SubjectName }).Distinct().ToDictionary(x => x.SubjectId, x => x.SubjectName);

            //var chapterlist = _chapterManager.GetChapterList().Select(x => new { x.ChapterId, x.ChapterName }).Distinct().ToDictionary(x => x.ChapterId, x => x.ChapterName);

            //var sectionlist = _sectionManager.GetSectionList().Select(x => new { x.SectionId, x.SectionName }).Distinct().ToDictionary(x => x.SectionId, x => x.SectionName);


            lnkQuestions.StandardList = Helper.ToSelectListItems(standardlist);
            //lnkQuestions.MediumList = Helper.ToSelectListItems(mediumlist);
            //lnkQuestions.SubjectList = Helper.ToSelectListItems(subjectlist);
            //lnkQuestions.ChapterList = Helper.ToSelectListItems(chapterlist);
            //lnkQuestions.SectionList = Helper.ToSelectListItems(sectionlist);
        }


        [HttpPost]
        [Route("savemcq")]
        [Route("MCQ/SaveMcq")]
        public ActionResult SaveMcq([System.Web.Http.FromBody] LnkQuestionChapterViewModel lnkQuestionChapter)
        {
            var retVal = false;
            try
            {
                var existingQuestions = _mcqManager.GetFilteredMcq(lnkQuestionChapter.StandardId,
                   lnkQuestionChapter.MediumId,lnkQuestionChapter.SubjectId,lnkQuestionChapter.ChapterId, lnkQuestionChapter.SectionId ,true).Questions;

                if ( lnkQuestionChapter.Questions.Count > existingQuestions.Count )
                {
                    retVal = _mcqManager.AddMcq(lnkQuestionChapter, existingQuestions);
                }
                else
                {
                    retVal = _mcqManager.UpdateMcq(lnkQuestionChapter,existingQuestions);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return Json(retVal);
        }

        [HttpGet]
        public JsonResult GetMediumsByStandardId(long standardId)
        {
            var _mediumList = Helper.ToSelectListItems(_mediumManager.GetMediumList().Select(x => new { x.MediumId, x.MediumName }).Distinct().ToDictionary(x => x.MediumId, x => x.MediumName)); 

            return Json(_mediumList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSubjectsByStandardAndMedium(long standardId, long mediumId)
        {
            var _mediumList = Helper.ToSelectListItems(_configurationManager.GetConfiguredSubjectList(standardId, mediumId).Select(x => new { x.SubjectId, x.SubjectName }).Distinct().ToDictionary(x => x.SubjectId, x => x.SubjectName));

            return Json(_mediumList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetChaptersByStandardAndMedium(long standardId, long mediumId, long subjectId)
        {
            var _chapterList = Helper.ToSelectListItems(_chapterManager.GetChapterListByStandardId(standardId, mediumId, subjectId).Select(x => new { x.ChapterId, x.ChapterName }).Distinct().ToDictionary(x => x.ChapterId, x => x.ChapterName));

            return Json(_chapterList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSectionsByChapterId(long chapterId, long mediumId)
        {
            var _sectionList = Helper.ToSelectListItems(_sectionManager.GetSectionsByChapterId(chapterId, mediumId).Select(x => new { x.SectionId, x.SectionName }).Distinct().ToDictionary(x => x.SectionId, x => x.SectionName));

            return Json(_sectionList, JsonRequestBehavior.AllowGet);
        }

    }
}