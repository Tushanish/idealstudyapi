﻿using PrismAdminAPI.Manager.SponsorerAdmin;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Controllers
{
    public class SponsorerAdminController : Controller
    {
        private readonly ISponsorerAdminManager _sponsorerAdminManager;

        public SponsorerAdminController(ISponsorerAdminManager sponsorerAdminManager)
        {
            _sponsorerAdminManager = sponsorerAdminManager;
            
        }

        /// <summary>
        /// get all students assigned for sponsorer
        /// </summary>
        /// <returns></returns>
        [Route("sponsorerStudentView")]
        [Route("SponsorerAdmin/SponsorerStudentList")]
        public ActionResult SponsorerStudentList()
        {
            long.TryParse(Request.Cookies.Get("SponsorerId").Value, out long _sponsorerId);
            IEnumerable<StudentDetail> sponsorerStudentDetails = _sponsorerAdminManager.GetStudentDetailsForSponsorer(true,_sponsorerId);
            return View(sponsorerStudentDetails);
        }


        /// <summary>
        /// get all students assigned for sponsorer
        /// </summary>
        /// <returns></returns>
        [Route("sponsorerCouponView")]
        [Route("SponsorerAdmin/SponsorerCouponList")]
        public ActionResult SponsorerCouponList()
        {
            long.TryParse(Request.Cookies.Get("SponsorerId").Value, out long _sponsorerId);
            IEnumerable<SponsorerCoupon> sponsorerCoupons = _sponsorerAdminManager.GetCouponsForSponsorer(_sponsorerId);
            return View(sponsorerCoupons);
        }

        [HttpGet]
        public JsonResult GetUnusedCoupons(bool unused)
        {
            long.TryParse(Request.Cookies.Get("SponsorerId").Value, out long _sponsorerId);
            IEnumerable<StudentDetail> _studentDetails = _sponsorerAdminManager.GetStudentDetailsForSponsorer(unused, _sponsorerId);

            var studentList = new List<StudentDetail> { };
            _studentDetails.ToList().ForEach(x => {
                var student = new StudentDetail
                {
                    CouponCode = x.CouponCode ?? string.Empty,
                    CouponId = x.CouponId,
                    DateOfBirth = x.DateOfBirth ?? string.Empty,
                    FatherName = x.FatherName ?? string.Empty,
                    FirstName = x.FirstName ?? string.Empty,
                    IsActive = x.IsActive,
                    IsUsed = x.IsUsed,
                    LastLoginDate = x.LastLoginDate ?? string.Empty,
                    LastName = x.LastName ?? string.Empty,
                    LoginDate = x.LoginDate ?? string.Empty,
                    LoginId = x.LoginId,
                    MediumId = x.MediumId,
                    MediumList = x.MediumList ?? new List<SelectListItem> { },
                    MediumName = x.MediumName ?? string.Empty,
                    Password = x.Password ?? string.Empty,
                    PhoneNumber = x.PhoneNumber ?? string.Empty,
                    SponsorerName = x.SponsorerName ?? string.Empty,
                };
                studentList.Add(student);
            });

            return Json(studentList, JsonRequestBehavior.AllowGet);
        }


    }
}