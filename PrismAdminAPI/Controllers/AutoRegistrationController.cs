﻿using ExcelDataReader;
using PrismAdminAPI.Manager;
using PrismAdminAPI.Manager.StudentDetails;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;





namespace PrismAdminAPI.Controllers
{
    public class AutoRegistrationController : Controller
    {
        private readonly IStudentAPIManager _studentAPIManager;
        private readonly ICouponManager _couponManager;
        public AutoRegistrationController(IStudentAPIManager studentAPIManager, ICouponManager couponManager)
        {
            _studentAPIManager = studentAPIManager;
            _couponManager = couponManager;
        }
     
        [HttpGet]
        [Route("autoregistration")]
        [Route("AutoRegistration/AutoRegistration")]
        public ActionResult AutoRegistration()
        {
            return View();
        }
        
        [HttpPost]
        [Route("autoregistration")]
        [Route("AutoRegistration/AutoRegistration")]
        public ActionResult AutoRegistration(IEnumerable<HttpPostedFileBase> UploadedFile)
        {
            try
            {
                bool isSuccess = true;
                //HttpPostedFileBase file = Request.Files["UploadedFile"];
                if ((UploadedFile != null) && (UploadedFile.First().ContentLength > 0) && !string.IsNullOrEmpty(UploadedFile.First().FileName))
                {
                    var data = ReadFileContent(UploadedFile.First());

                    foreach (var login in data)
                    {
                        Student student = _studentAPIManager.StudentRegistration(login,false);
                        if (student != null)
                            isSuccess = true;
                    }
                }
                return Json(isSuccess);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private IEnumerable<Login> ReadFileContent(HttpPostedFileBase file)
        {
            string fileName = file.FileName;
            string fileContentType = file.ContentType;
            byte[] fileBytes = new byte[file.ContentLength];
            Stream stream = file.InputStream;
            IExcelDataReader reader = null;
            reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            DataSet result = reader.AsDataSet(new ExcelDataSetConfiguration()
            {
                ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                {
                    UseHeaderRow = true
                }
            });
            var loginList = new List<Login>();
            var couponList = new List<string>();
            foreach (DataRow row in result.Tables[0].Rows)
            {
                couponList.Add("'" + row["CouponCode"].ToString() + "'");
            }
            IEnumerable<Coupon> coupons = _couponManager.GetCouponsForIds(couponList);
            foreach (DataRow row in result.Tables[0].Rows)
            {
                loginList.Add(CreateLoginModel(row, coupons));
            }
            return loginList;
        }

        private Login CreateLoginModel(DataRow row, IEnumerable<Coupon> coupons)
        {

            Login login = new Login
            {
                PhoneNumber = row["PhoneNumber"].ToString(),
                FirstName = string.IsNullOrEmpty(row["FirstName"].ToString()) ? string.Empty : row["FirstName"].ToString(),
                LastName = string.IsNullOrEmpty(row["LastName"].ToString()) ? string.Empty : row["LastName"].ToString(),
                Password = string.IsNullOrEmpty(row["Password"].ToString()) ? string.Empty : row["Password"].ToString(),
                DateOfBirth = string.IsNullOrEmpty(row["DateOfBirth"].ToString()) ? string.Empty :
                (Convert.ToDateTime(row["DateOfBirth"]).Month.ToString() + "/" + Convert.ToDateTime(row["DateOfBirth"]).Day.ToString() + "/" + Convert.ToDateTime(row["DateOfBirth"]).Year.ToString()),
                StandardId = Convert.ToInt16(row["StandardId"].ToString()),
                MediumId = Convert.ToInt16(row["MediumId"].ToString()),
                CouponId = coupons.First(x => x.CouponCode == row["CouponCode"].ToString()).CouponId.Value,
                EmailId = string.IsNullOrEmpty(row["EmailId"].ToString()) ? string.Empty : row["EmailId"].ToString(),
                FatherName = string.IsNullOrEmpty(row["FatherName"].ToString()) ? string.Empty : row["FatherName"].ToString()
            };
            return login;
        }

    }
}