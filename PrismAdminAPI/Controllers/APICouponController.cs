﻿using PrismAdminAPI.Manager;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PrismAdminAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("coupon")]
    public class APICouponController : ApiController
    {
        private IStudentAPIManager studentAPIManager = null;
        public APICouponController(IStudentAPIManager _studentAPIManager)
        {
            studentAPIManager = _studentAPIManager;
        }


        [Route("ValidateCoupon/{couponCode}")]
        [AllowAnonymous]
        [HttpGet]
        public Coupon ValidateCoupon(string couponCode)
        {
            return studentAPIManager.ValidateCoupon(couponCode);
        }
    }
}
