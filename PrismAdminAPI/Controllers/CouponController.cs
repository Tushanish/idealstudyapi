﻿using PrismAdminAPI.BAL;
using PrismAdminAPI.Manager;
using PrismAdminAPI.Models;
using PrismAdminAPI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Controllers
{
    public class CouponController : Controller
    {

        private readonly ICouponManager _couponManager;
        private readonly ISponsorerManager _sponsoreManager;
        private readonly List<CouponSponsorerViewModel> _couponSponsorerViewModel;

        public CouponController(ICouponManager couponManager, ISponsorerManager sponsorerManager)
        {
            _couponManager = couponManager;
            _sponsoreManager = sponsorerManager;
            //_couponSponsorerViewModel = GetCouponDetails(); //GetCouponSponsorerDetails();
        }


        [Route("addCoupon")]
        [Route("Coupon/AddCoupon")]
        public ActionResult AddCoupon()
        {
            return View();
        }

        /// <summary>
        /// create coupon
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("addCoupon")]
        [Route("Coupon/AddCoupon")]
        public ActionResult AddCoupon(CouponViewModel coupon)
        {
            var couponList = CreateCouponObjects(coupon.NoOfCoupons);
            var returnVal = _couponManager.SaveBulkCoupons(coupon.NoOfCoupons, "dbo.Mas_Coupon", couponList);
            if (returnVal)
            {
                TempData["Success"] = 1;
            }
            else
            {
                TempData["Success"] = 0;
            }
            return RedirectToAction("ListCoupon");
        }


        /// <summary>
        /// get all coupon details list 
        /// </summary>
        /// <returns></returns>
        [Route("couponlist")]
        [Route("CouponList/ListCoupon")]
        public ActionResult ListCoupon()
        {
            return View();
            //return View(_couponSponsorerViewModel);
        }


        [HttpPost]
        public JsonResult FilterCouponDetails(bool usedCoupons, long fromCouponId, long toCouponId)
        {
            return Json(GetCouponDetails(usedCoupons, fromCouponId, toCouponId).Data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCouponDetails(bool usedCoupons, long fromCouponId, long toCouponId)
        {
            IEnumerable<CouponDetails> filteredData = null;
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchvalue = Request["search[value]"];
            var sortcolumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").
                                FirstOrDefault() + "][name]").FirstOrDefault();
            
            string sortDirection = Request["order[0][dir]"];
            long totalRecords = 0;

            if (string.IsNullOrEmpty(searchvalue) && fromCouponId == 0)
            {
                filteredData = _couponManager.GetCouponDetails(string.Empty, usedCoupons, out totalRecords, fromCouponId, toCouponId,start,length).ToList()
                                                ?? Enumerable.Empty<CouponDetails>();
                
            }
            else
            {
                filteredData = _couponManager.GetCouponDetails(searchvalue, usedCoupons, out  totalRecords, fromCouponId, toCouponId, start, length).ToList();

                //filteredData = allCouponDetails.Where(a => a.CouponCode.ToLower().Contains(searchvalue.ToLower()));

                totalRecords = (long)filteredData?.Count();
            }

            if (sortDirection == "asc")
            
            {
                if (sortcolumn == "Coupon Code")
                    filteredData = filteredData.OrderBy(x => x.CouponCode).ToList<CouponDetails>();
                else if (sortcolumn == "Coupon Id")
                    filteredData = filteredData.OrderBy(x => x.CouponId).ToList<CouponDetails>();
            }

            if (sortDirection == "desc")
            {
                if (sortcolumn == "Coupon Code")
                    filteredData = filteredData.OrderByDescending(x => x.CouponCode).ToList<CouponDetails>();
                else if (sortcolumn == "Coupon Id")
                    filteredData = filteredData.OrderByDescending(x => x.CouponId).ToList<CouponDetails>();

            }
            return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = filteredData });

        }


        /// <summary>
        /// Activate or Deactivate the coupon
        /// </summary>
        /// <param name="_coupon">Coupon instance</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ActiveDeactivateCoupon(long fromCouponId, long toCouponId,bool isActive)
        {
            bool _activateDeactivateResult = _couponManager.ActivateDeactivateCoupon(fromCouponId, toCouponId, isActive);
            return Json(_activateDeactivateResult);
        }

        [NonAction]
        public List<CouponSponsorerViewModel> GetCouponSponsorerDetails()
        {
            try
            {
            List<CouponSponsorerViewModel> couponSponsorerViewModels = new List<CouponSponsorerViewModel>();
            var lnkCoupons = _couponManager.GetAllLnkCouponDetails();
                var couponIds = new List<string>(); 
                lnkCoupons.ToList().ForEach(x => {
                    couponIds.Add(x.CouponId.ToString());
                });

            var couponDetails = _couponManager.GetCouponsForIds(couponIds);
            var sponsorerDetails = _sponsoreManager.GetAll();
            foreach (var lnkCoupon in lnkCoupons)
            {
                var sponsorer = sponsorerDetails.FirstOrDefault(x => x.SponsorerId == lnkCoupon.SponsorerId);
                var coupon = couponDetails.FirstOrDefault(x => x.CouponId == lnkCoupon.CouponId);

                couponSponsorerViewModels.Add(new CouponSponsorerViewModel
                {
                    CouponCode = coupon.CouponCode,
                    CouponId = coupon.CouponId,
                    CreatedDate = coupon.CreatedDate,
                    IsActive = coupon.IsActive,
                    CreatedBy = coupon.CreatedBy,
                    ModifiedBy = coupon.ModifiedBy,
                    ModifiedDate = coupon.ModifiedDate,
                    SponsorerId = sponsorer.SponsorerId,
                    SponsorerName = sponsorer.SponsorerName
                });
            }
            foreach (var coupon in couponDetails)
            {
                if(!lnkCoupons.Any(x=>x.CouponId == coupon.CouponId))
                {
                    couponSponsorerViewModels.Add(new CouponSponsorerViewModel
                    {
                        CouponCode = coupon.CouponCode,
                        CouponId = coupon.CouponId,
                        CreatedDate = coupon.CreatedDate,
                        IsActive = coupon.IsActive,
                        CreatedBy = coupon.CreatedBy,
                        ModifiedBy = coupon.ModifiedBy,
                        ModifiedDate = coupon.ModifiedDate,
                    });
                }
            }
            return couponSponsorerViewModels;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [NonAction]
        public List<Coupon> CreateCouponObjects(int noOfCoupons)
        {
            var couponList = new List<Coupon>();
            for (int i = 0; i < noOfCoupons; i++)
            {
                couponList.Add(new Coupon { CouponCode = CouponGenerator.RandomString() });
            }
            return couponList;
        }

    }
}