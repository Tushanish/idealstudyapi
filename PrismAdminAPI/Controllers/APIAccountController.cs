﻿using PrismAdminAPI.Manager;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PrismAdminAPI.Controllers.APIController
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("account")]
    public class APIAccountController : ApiController
    {
        private IStudentAPIManager studentAPIManager = null;
        private ISingleSignInUserManager singleSignInUserManager = null;
        public APIAccountController(IStudentAPIManager _studentAPIManager, ISingleSignInUserManager _singleSignInUserManager)
        {
            studentAPIManager = _studentAPIManager;
            singleSignInUserManager = _singleSignInUserManager;
        }


        [HttpPost]
        [Route("registerstudent")]
        [AllowAnonymous]
        public Student StudentRegistration([FromBody]Login login)
        {
            return studentAPIManager.StudentRegistration(login,true);
        }

        [HttpGet]
        [Route("validateMobileNumber/{phoneNumber}")]
        [AllowAnonymous]
        public bool ValidateMobileNumber(string phoneNumber)
        {
            return studentAPIManager.ValidateMobileNumber(phoneNumber);
        }

        [HttpGet]
        [Route("updateAppActiveTime/{loginId}")]
        [AllowAnonymous]
        public bool UpdateAppActiveTime(long loginId)
        {
            return true; //studentAPIManager.UpdateAppActiveTime(loginId);
        }

        [HttpGet]   
        [Route("logoutStudent/{loginId}")]
        [AllowAnonymous]
        public bool LogoutStudent(string loginId)
        {
            if (long.TryParse(loginId, out long result))
            {
                return singleSignInUserManager.RemoveSingleSignInDetails(result, 0,null);
            }
            else
                return false;
        }
    }
}