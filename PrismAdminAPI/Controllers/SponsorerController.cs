﻿using PrismAdminAPI.Manager;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Controllers
{
    public class SponsorerController : Controller
    {
        private readonly ISponsorerManager sponsorerManager;

        public SponsorerController(ISponsorerManager _sponsererManager)
        {
            sponsorerManager = _sponsererManager;
        }

        /// <summary>
        /// get all standard list 
        /// </summary>
        /// <returns></returns>
        [Route("sponsorerlist")]
        [Route("Sponsorer/ListSponsorer")]
        public ActionResult ListSponsorer()
        {
            IEnumerable<Sponsorer> _sponsorers = sponsorerManager.GetAll();
            //_sponsorers.ToList().ForEach(sp =>
            //{
            //    try
            //    {
            //        sp.LandingImageByteData = System.IO.File.ReadAllBytes(HttpContext.Server.MapPath(@sp.LandingImagePath));
            //    }
            //    catch (Exception)
            //    {
            //        sp.LandingImageByteData = null;
            //    }
                
            //    try
            //    {
            //        sp.ThumbnailImageByteData = System.IO.File.ReadAllBytes(HttpContext.Server.MapPath(@sp.ThumbnailImagePath));
            //    }
            //    catch (Exception)
            //    {
            //        sp.ThumbnailImageByteData = null;
            //    }
               
            //});


            return View(_sponsorers);
        }

        /// <summary>
        /// get all standard list 
        /// </summary>
        /// <returns></returns>
        [Route("assignsponsorer")]
        [Route("Sponsorer/AssignSponsorer")]
        public ActionResult AssignSponsorer()
        {
            LnkCouponDetails _lnkcouponDetails = new  LnkCouponDetails();
            var sponsorers = sponsorerManager.GetAll();
            foreach (var sponsorer in sponsorers)
            {
                _lnkcouponDetails.SponsorersList.Add(new SelectListItem { Text = sponsorer.SponsorerName, Value = sponsorer.SponsorerId.ToString() });
            }
            return View(_lnkcouponDetails);
        }

        /// <summary>
        /// get all standard list 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("assignsponsorer")]
        [Route("Sponsorer/AssignSponsorerCoupon")]
        public ActionResult AssignSponsorerCoupon(LnkCouponDetails couponDetails)//AssignSponsorer sponsorer)
        {
            sponsorerManager.AssignSponsorerToCoupon(couponDetails.FromCouponId, couponDetails.ToCouponId, couponDetails.SponsorerId);
            return RedirectToAction("ListCoupon", "Coupon");
        }

        /// <summary>
        /// Add Sponsorerr
        /// </summary>
        /// <returns></returns>
        [Route("addsponsorer")]
        [Route("Sponsorer/AddSponsorer")]
        public ActionResult AddSponsorer()
        {
            Sponsorer sponsorer = new Sponsorer();
            return View(sponsorer);
        }

        /// <summary>
        /// Insert Sponsorer
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("addsponsorer")]
        [Route("Sponsorer/InsertSponsorer")]
        public ActionResult InsertSponsorer(Sponsorer sponsorer)
        {
            sponsorerManager.AddSponsorer(sponsorer);
            return RedirectToAction("ListSponsorer");
        }

        /// <summary>
        /// Update Standard
        /// </summary>
        /// <returns></returns>
        [Route("editsponsorer/{sponsorerId}")]
        [Route("Sponsorer/EditSponsorer/{sponsorerId}")]
        public ActionResult EditSponsorer(int sponsorerId)
        {
            Sponsorer _sponsorer = sponsorerManager.GetAll(sponsorerId).FirstOrDefault();
            _sponsorer.LandingImageByteData =  System.IO.File.ReadAllBytes(HttpContext.Server.MapPath(_sponsorer.LandingImagePath));
            _sponsorer.ThumbnailImageByteData =  System.IO.File.ReadAllBytes(HttpContext.Server.MapPath(_sponsorer.ThumbnailImagePath));

            if (_sponsorer == null)
            {
                return View("ListSponsorer");
            }
            return View(_sponsorer);
        }

        /// <summary>
        /// Update standard 
        /// </summary>
        /// <param name="_standard"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateSponsorer(Sponsorer _sponsorer)
        {
            bool _updateResult = sponsorerManager.UpdateSponsorer(_sponsorer);
            return RedirectToAction("ListSponsorer");
        }

        /// <summary>
        /// Activate or Deactivate the standard
        /// </summary>
        /// <param name="_standard"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ActiveDeactiveSponsorer(Sponsorer _sponsorer)
        {
            bool _activateDeactivateResult = sponsorerManager.AcitvateDeactivateSponsorer(_sponsorer.SponsorerId, _sponsorer.IsActive);
            return Json(_activateDeactivateResult);
        }
    }
}