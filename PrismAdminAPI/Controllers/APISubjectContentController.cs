﻿using PrismAdminAPI.Manager;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PrismAdminAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("subjectcontent")]
    public class APISubjectContentController : ApiController
    {
        private IStudentAPIManager studentAPIManager = null;
        public APISubjectContentController(IStudentAPIManager _studentAPIManager)
        {
            studentAPIManager = _studentAPIManager;
        }

        [HttpGet]
        [Route("getsubjectsectioncontent/{subjectId}/{sectionId}")]
        [AllowAnonymous]
        public SubjectContentViewModel GetSubjectSectionContent(long subjectId, long sectionId)
        {
            return studentAPIManager.GetSubjectContentForSection(subjectId, sectionId);
        }
    }
}
