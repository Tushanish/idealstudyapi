﻿using PrismAdminAPI.Manager;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace PrismAdminAPI.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAdminManager adminManager;
       
        public AccountController(IAdminManager _adminManager)
        {
            adminManager = _adminManager;
        }
        // GET: Account
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Login(UserLogin _userLogin)
        {
            if (ModelState.IsValid)
            {
                Admin _admin = adminManager.ValidateAdmin(_userLogin);

                if (_admin != null)
                {
                    Response.Cookies.Add(new HttpCookie("SponsorerId", _admin.SponsorerId.ToString()));

                    FormsAuthentication.SetAuthCookie(_userLogin.UserName, _userLogin.RememberMe);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Please enter valid user name and password");
                    return View();
                }
            }
            else
            {
                return View();
            }
        }
    }
}