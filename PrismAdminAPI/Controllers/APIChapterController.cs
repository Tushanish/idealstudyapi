﻿using PrismAdminAPI.Manager;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PrismAdminAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("chapter")]
    public class APIChapterController : ApiController
    {
        private IStudentAPIManager studentAPIManager = null;
        public APIChapterController(IStudentAPIManager _studentAPIManager)
        {
            studentAPIManager = _studentAPIManager;
        }

        [HttpGet]
        [Route("getchapterlist/{standardId}/{subjectId}/{mediumId}")]
        [AllowAnonymous]
        public IEnumerable<Chapter> GetChapterList(long standardId, long subjectId, long mediumId)
        {
            var chapterList = studentAPIManager.GetChapterList(standardId, subjectId, mediumId);
            return chapterList;
        }
    }
}