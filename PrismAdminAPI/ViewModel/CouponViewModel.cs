﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.ViewModel
{
    public class CouponViewModel
    {
        [Required(ErrorMessage = "Please enter no of coupons to generate.")]
        [Range(minimum:1,maximum:500,ErrorMessage = "At once only 500 copouns can be generated.")]
        public int NoOfCoupons { get; set; }
    }
}