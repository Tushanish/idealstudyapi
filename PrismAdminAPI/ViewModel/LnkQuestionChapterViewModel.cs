﻿using PrismAdminAPI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Models
{
    public class LnkQuestionChapterViewModel
    {
        public long? StandardId { get; set; }

        public long? SubjectId { get; set; }

        public long? ChapterId { get; set; }

        public long? MediumId { get; set; }

        public long? SectionId { get; set; }

        public List<Question> Questions { get; set; }

        public IEnumerable<SelectListItem> StandardList { get  ; set; }

        public IEnumerable<SelectListItem> ChapterList { get; set; } = new List<SelectListItem> { };
               
        public IEnumerable<SelectListItem> MediumList { get; set; } = new List<SelectListItem> { };

        public IEnumerable<SelectListItem> SubjectList { get; set; } = new List<SelectListItem> { };

        public IEnumerable<SelectListItem> SectionList { get; set; } = new List<SelectListItem> { };
    }
}