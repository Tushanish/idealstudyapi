﻿using System;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.OAuth;
using System.Web.Http;

[assembly: OwinStartup(typeof(PrismAdminAPI.Startup))]
namespace PrismAdminAPI
{
    public class Startup
    {
        public void ConfigureAuth(IAppBuilder app)
        {

            app.UseCors(CorsOptions.AllowAll);
            var OAuthOptions = new OAuthAuthorizationServerOptions
                {
                    AllowInsecureHttp = true,
                    TokenEndpointPath = new PathString("/account/authenticate"),
                    AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(20),
                    Provider = new ApplicationOAuthProvider(),
                    //RefreshTokenProvider = new RefreshTokenProvider()
                };

            app.UseOAuthBearerTokens(OAuthOptions);
            app.UseOAuthAuthorizationServer(OAuthOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);
        }

        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}